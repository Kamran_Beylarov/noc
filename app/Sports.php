<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sports extends Model
{
  protected $table = 'sports';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'sport_name',
    'sport_name_en',
    'sport_icon',
    'sport_slug',
    'sport_slug_en',
  ];
}
