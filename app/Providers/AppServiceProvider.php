<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Sponsors;
use App\Games;
use App\News;
use Session;
use Config;
use File;
use App;
use App\AthleteGameTurnament;
use App\Observers\GamesObserver;
use App\Observers\NewsObserver;
use App\Observers\AthleteGameTurnamentObserver;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('*', function($view)
     {
       $locale =Session::get('locale', Config::get('app.locale'));
       $sponsors_layout = Sponsors::orderBy('sponsor_order')->get();

       $path = App::langPath().'/'. $locale .'/month.json';
       $lang_json = File::get($path);
        $lang_object = json_decode($lang_json);
        $months = (array) $lang_object;
        

       $view->with('sponsors_layout', $sponsors_layout);
       $view->with('locale', $locale);
       $view->with('months', $months);


     });

    //  Observers
      Games::observe(GamesObserver::class);
      News::observe(NewsObserver::class);
      AthleteGameTurnament::observe(AthleteGameTurnamentObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
