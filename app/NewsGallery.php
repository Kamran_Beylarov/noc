<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsGallery extends Model
{
  protected $table = 'news_gallery';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'news_id',
    'img_src',
  ];
  public function gallery_news(){
    return $this->belongsTo('App\News','news_id','id');
   }
}
