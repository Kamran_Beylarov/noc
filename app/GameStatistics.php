<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStatistics extends Model
{
  protected $table = 'game_statistics';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'male',
    'female',
    'sum',
    'link',
    'game_id',
    'sport_id',
  ];

  public function statistic_game()
  {
    return $this->belongsTo('App\Games','game_id','id');
  }
  public function statistic_sport()
  {
    return $this->belongsTo('App\Sports','sport_id','id');
  }
}
