<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoGallery extends Model
{
  protected $table = 'video_gallery';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'video_name',
    'video_name_en',
    'video_iframe',
    'video_date',
    'video_name_slug',
    'video_name_en_slug',
    'video_thumbnail',
  ];
}
