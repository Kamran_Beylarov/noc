<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Federations extends Model
{
  protected $table = 'federations';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'federation_type',
    'federation_name',
    'federation_name_en',
    'federation_icon',
    'drectory_name',
    'drectory_name_en',
    'telefon1',
    'telefon2',
    'fax',
    'web_site',
    'email',
    'address',
  ];

  public function members_federation(){
    return $this->hasMany('App\Members','federation_id','id');
  }
}
