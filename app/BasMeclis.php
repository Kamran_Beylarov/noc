<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasMeclis extends Model
{
  protected $table = 'bas_meclis';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'page_name',
    'page_name_en',
    'page_img',
    'page_description',
    'page_description_en',
    'page_slug',
    'page_slug_en',
  ];
  public function meclis_terkibi(){
  return $this->hasMany('App\MeclisTerkib','meclis_id');
}
}
