<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
  protected $table = 'members';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'member_name',
    'member_name_en',
    'member_post',
    'member_post_en',
    'federation_id',
    'institutions_id',
  ];

  public function members_federation(){
    return $this->hasOne('App\Federations','id', 'federation_id');
  }

}
