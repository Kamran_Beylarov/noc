<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Session;
use Config;
use Illuminate\Http\Request;
class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
    public function __construct(Request $request)
    {
      if ( Session::has('locale') ){
        $lang = Session::get('locale');
         App::setLocale($lang);
      }
    }
}
