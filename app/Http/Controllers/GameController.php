<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GameCategories;
use App\Games;
use App\News;
use App\GameStatistics;
use App\AthleteGameTurnament;
use Session;
use Config;
use Storage;
use Lang;
use App\Sports;
use App;
use File;
class GameController extends Controller
{
  public function game_categories()
  {
    $game_categories = GameCategories::orderBy('cate_order', 'ASC')->get();
    return view('front.game_categories',compact('game_categories'));
  }
  public function games($slug)
  {
    $locale = Session::get('locale', Config::get('app.locale'));
  
    if ($locale == 'az') {
      $game_cates = GameCategories::where('cate_slug', $slug)->first();
    }else {
      $game_cates = GameCategories::where('cate_slug_en', $slug)->first();
    }

    if ($game_cates != null) {
      $id = $game_cates->id;
      $games = Games::where('game_cate_id', $id)->orderBy('start_date','DESC')->get();

      return view('front.games',compact('games'));
    }else{
      return redirect('/');
    }

  }
  public function games_detalis($slug)
  {
    $locale = Session::get('locale', Config::get('app.locale'));
    
    if ($locale == 'az') {
      $game_detalis = Games::where('game_slug', $slug)->first();
    }else {
      $game_detalis = Games::where('game_slug_en', $slug)->first();
    }
      // dd($game_detalis);

    if($game_detalis != null){
      $id = $game_detalis->id;
      $game_statistics = GameStatistics::with('statistic_sport')->where('game_id', $id)->orderBy('sum')->get();
      $game_athlete_statistics = AthleteGameTurnament::with('athlete_game_tour','relation_statistics')->where('game_tournament_type', 1)->where('game_tournament_id', $id)->get()->sortBy(function($statistics , $key){
        
         return $statistics->relation_statistics[0]->medal;
      });

      $game_news = News::where('game_id', $id)->where('lang', $locale)->orderBy('news_date')->limit(4)->get();
      return view('front.game_detalis',compact('game_detalis','game_news','game_statistics','game_athlete_statistics'));
    }else{
      return redirect('/game-categories');
    }
  }


}
