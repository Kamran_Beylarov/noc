<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Athlete;
use Session;
use Config;
use App\Sports;
class AthleteController extends Controller
{



    public function index()
    {
      $locale = Session::get('locale', Config::get('app.locale'));
      if ($locale == 'az') {
          $athlete_name = 'athlete_name';
          $alpha = ['A','B','C','Ç','D','E','Ə','F','G','H','X','İ','J','K','Q','L','M','N','O','Ö','P','R','S','Ş','T','U','Ü','V','Y','Z'];
      }else{
          $athlete_name = 'athlete_name_en';
          $alpha = ['A','B','C','D','E','F','G','H','İ','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
      }
      $gender = null;
      $alphabet = null;
      $sports = null;
      $athletes = Athlete::orderBy('significance_status','DESC')->paginate(21);
      return view('front.athletes',compact('athletes','gender','alpha','alphabet','sports'));
    }


    public function detailis($slug)
    {
         $locale = Session::get('locale', Config::get('app.locale'));
          if($locale == 'az'){
           $athlete = Athlete::with('athlete_stat_relation','athlete_sport')->where('athlete_slug',$slug)->first();
         }else{
           $athlete = Athlete::with('athlete_stat_relation','athlete_sport')->where('athlete_slug_en',$slug)->first();
         }
         if($athlete != null){
           return view('front.athlete_detalis',compact('athlete'));
         }else {
           return redirect('/athletes');
         }
    }

    public function athlete_filter(Request $request)
    {
      $locale = Session::get('locale', Config::get('app.locale'));
      if ($locale == 'az') {
          $athlete_name = 'athlete_name';
          $alpha = ['A','B','C','Ç','D','E','Ə','F','G','H','X','İ','J','K','Q','L','M','N','O','Ö','P','R','S','Ş','T','U','Ü','V','Y','Z'];
      }else{
          $athlete_name = 'athlete_name_en';
          $alpha = ['A','B','C','D','E','F','G','H','İ','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
      }

      $data = Athlete::orderBy('significance_status','DESC');
    

      $gender = $request['gender'];
      if($gender != 'all' && $gender != null){
         $data->where('athlete_gender', $gender);
      }

      $alphabet = $request['alphabet'];

      if($alphabet != 'all' && $alphabet != null){
        $ath_data = Athlete::all();
        $ath_id = [];
        if($locale == 'az'){
          foreach ($ath_data as $key => $value) {
            if(substr($value->athlete_name, 0, 1) == $alphabet){
              $ath_id[] += $value->id;
            }
          }
          $data->whereIn('id', $ath_id);
        }else{
          foreach ($ath_data as $key => $value) {
            if(substr($value->athlete_name_en, 0, 1) == $alphabet){
              $ath_id[] += $value->id;
            }
          }
          $data->whereIn('id', $ath_id);
        }
      }


      $sports = $request['sports'];
      if($sports != 'all' && $sports != null){
          if ($locale == 'az') {
             $sport_id = Sports::where('sport_slug', $sports)->first()->id;
          }else{
            $sport_id = Sports::where('sport_slug_en', $sports)->first()->id;
          }
          $data->where('sport_id', $sport_id);
      }
      $athletes = $data->paginate(20);
      return view('front.athletes',compact('athletes','gender','alphabet','alpha','sports'));
    }

    public function sports()
    {
      $locale = Session::get('locale', Config::get('app.locale'));
      if ($locale == 'az') {
        $order = 'sport_name';
      }else{
        $order = 'sport_name_en';
      }
      //
      $sports = Sports::orderBy($order)->get();
      return view('front.sports',compact('sports'));
    }
}
