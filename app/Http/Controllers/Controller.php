<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Torann\LaravelMetaTags\Facades\MetaTag;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
        // Defaults
        MetaTag::set('description', 'Azərbaycan Olimpiya hərəkatını bizimlə izlə');
        MetaTag::set('image', asset('front/images/fb1.png'));
        MetaTag::set('title', 'Azərbaycan Respublikası Milli Olimpiya Komitəsi');
    }
}
