<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Publications;
use Session;
use Config;
use DB;
use Torann\LaravelMetaTags\Facades\MetaTag;
class NewsController extends Controller
{
    public function news()
    {

       $locale =Session::get('locale', Config::get('app.locale'));
      $news_page_title = News::where('news_type', 0)->where('lang', $locale)->orderBy('id' ,'DESC')->first();
      $news_page_sidebar = News::where('news_type', 0)->where('lang', $locale)->orderBy('id' ,'DESC')->skip(5)->take(10)->limit(12)->get();
      $news_container = News::where('news_type', 0)->where('lang', $locale)->orderBy('id' ,'DESC')->skip(1)->take(4)->limit(4)->get();
      return view('front.news',compact('news_page_title','news_page_sidebar','news_container'));
    }

    public function news_detalis($slug)
    {

      $locale =Session::get('locale', Config::get('app.locale'));
      $news_detalis = News::with('news_gallery')->where('news_slug', $slug)->first();

      $pieces = explode(" ", $news_detalis->news_description);
      $text = implode(" ", array_splice($pieces, 0, 10));

      MetaTag::set('title', $news_detalis->news_name);
      MetaTag::set('description', $text);
      MetaTag::set('image', asset('/news/'.$news_detalis->news_thumbnail));
      

      if($news_detalis != null){
        $view_count = $news_detalis->view_count;
        $is_page_refreshed = (isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] == 'max-age=0');
 
        if(!$is_page_refreshed ) {
         
           $news_detalis->update([
          'view_count'=> $view_count+1
        ]);
        }
        $game_id = $news_detalis->game_id;
        $news_benzer = News::where('game_id',$game_id)->where('news_slug', '!=', $slug)->where('news_type', 0)->where('lang', $locale)->orderBy('id','DESC')->limit(2)->get();
        $news_page_sidebar = News::where('news_slug', '!=', $slug)->where('news_type', 0)->where('lang', $locale)->orderBy('id','DESC')->limit(15)->get();
        return view('front.newsdetalis',compact('news_detalis','news_benzer','news_page_sidebar'));
      }else{
        return redirect('/articles');
      }
    }
    public function news_archive()
    {
      $locale =Session::get('locale', Config::get('app.locale'));
      $news_archive = News::where('news_type', 0)->where('lang', $locale)->orderBy('news_date','DESC')->get();

        return view('front.newsarchive',compact('news_archive'));

    }
    public function news_search(Request $request)
    {
      $locale =Session::get('locale', Config::get('app.locale'));
      $news_archive = News::where('news_name', 'like', '%'.$request['search_news'].'%')->orWhere('news_description', 'like', '%'.$request['search_news'].'%')->where('lang', $locale)->orderBy('id','DESC')->get();
      $search_data = $request['search_news'];

      return view('front.newsarchive',compact('news_archive','search_data'));
    }
    public function activity()
    {
      $locale =Session::get('locale', Config::get('app.locale'));
       $activitys = News::where('news_type', 1)->where('lang', $locale)->orderBy('news_date','DESC')->limit(9)->get();
       $programs = News::where('news_type', 2)->where('lang', $locale)->orderBy('news_date','DESC')->limit(2)->get();
       $publications = Publications::orderBy('id','DESC')->limit(6)->get();
       return view('front.activity',compact('activitys','programs','publications'));
    }
    public function program_detalis($slug)
    {
        $program = News::with('news_gallery')->where('news_slug',$slug)->first();
        if ($program != null) {
          return view('front.programdetalis',compact('program'));
        }else {
          return redirect('/');
        }
    }
    public function activity_detalis($slug)
    {
        $activity = News::with('news_gallery')->where('news_slug',$slug)->first();
        if ($activity != null) {
          return view('front.activitydetalis',compact('activity'));
        }else{
          return redirect('/');
        }
    }

    public function programs()
    {
      $locale =Session::get('locale', Config::get('app.locale'));
      $programs = News::where('news_type', 2)->where('lang', $locale)->orderBy('news_date','DESC')->get();
      return view('front.programs',compact('programs'));
    }

    public function activities()
    {
      $locale =Session::get('locale', Config::get('app.locale'));
      $activitys = News::where('news_type', 1)->where('lang', $locale)->orderBy('news_date','DESC')->get();
      return view('front.activities',compact('activitys'));
    }
}
