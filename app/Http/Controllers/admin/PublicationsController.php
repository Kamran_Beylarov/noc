<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Publications;
use Session;
use File;
class PublicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publications = Publications::all();

        return view('admin.publications.index',compact('publications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.publications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $thumbnail1 = $request->file('publication_thumbnail');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);

      $p_file1 = $request->file('publication_file');
      $p_file = time().'.'.$p_file1->getClientOriginalName();
      $p_file1->move(public_path('uploads'), $p_file);


        Publications::create([
            'publication_name'=>$request['publication_name'],
            'publication_name_en'=>$request['publication_name_en'],
            'author_name'=>$request['author_name'],
            'author_name_en'=>$request['author_name_en'],
            'publication_file'=>$p_file,
            'publication_thumbnail'=>$thumbnail,
        ]);
      Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/publications');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publication = Publications::find($id);

        return view('admin.publications.edit',compact('publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $publication = Publications::find($id);

      if($request->file('publication_thumbnail')){

         $del = $publication->publication_thumbnail;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('publication_thumbnail');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $publication->update([
          'publication_thumbnail'=>$cover,
        ]);
      }

      if($request->file('publication_file')){

         $del = $publication->publication_file;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $P_file1 = $request->file('publication_file');
        $P_file = time().'.'.$P_file1->getClientOriginalName();
        $P_file1->move(public_path('uploads'), $P_file);

        $publication->update([
          'publication_file'=>$P_file,
        ]);
      }

      $publication->update([
        'publication_name'=>$request['publication_name'],
        'publication_name_en'=>$request['publication_name_en'],
        'author_name'=>$request['author_name'],
        'author_name_en'=>$request['author_name_en'],
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/publications');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $publication = Publications::find($id);

      $del = $publication->publication_file;
       $image_path = public_path().'/uploads/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }

       $del1 = $publication->publication_file;
        $image_path1 = public_path().'/uploads/'.$del1;
        if(File::exists($image_path1)) {
          File::delete($image_path1);
        }

      $publication->delete();
      Session::flash('danger','Kontent silindi');
      return redirect('/admin/publications');
    }
}
