<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BasMeclis;
use App\MeclisTerkib;
use Session;
use File;
class BasMeclisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meclis =BasMeclis::all();
        return view('admin.meclis.index',compact('meclis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.meclis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('page_img');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);

      $meclis_id =  BasMeclis::create([
        'page_name'=>$request['page_name'],
        'page_name_en'=>$request['page_name_en'],
        'page_description'=>$request['page_description'],
        'page_description_en'=>$request['page_description_en'],
        'athlete_slug'=>str_slug($request['page_name']),
        'athlete_slug_en'=>str_slug($request['page_name_en']),
        'page_img'=>$thumbnail,
      ])->id;

      $terkib =$request['terkib'];
      $terkib_en =$request['terkib_en'];
      $yer =$request['yer'];
      $say =$request['say'];


      for ($i=0; $i <count($terkib) ; $i++) {
        MeclisTerkib::create([
          'terkib'=>$terkib[$i],
          'terkib_en'=>$terkib_en[$i],
          'yer'=>$yer[$i],
          'say'=>$say[$i],
          'meclis_id'=>$meclis_id,
        ]);
      }

      Session::flash('success','Kontent əlavə edildi');
     return redirect('/admin/bash_meclis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $terkib = MeclisTerkib::where('meclis_id', $id)->get();
      return view('admin.meclis.show',compact('terkib','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $meclis = BasMeclis::find($id);
      return view('admin.meclis.edit',compact('meclis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $meclis = BasMeclis::find($id);

      if($request->file('page_img')){

         $del = $meclis->page_img;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('page_img');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $meclis->update([
          'page_img'=>$cover,
        ]);
      }

      $meclis->update([
        'page_name'=>$request['page_name'],
        'page_name_en'=>$request['page_name_en'],
        'page_description'=>$request['page_description'],
        'page_description_en'=>$request['page_description_en'],
        'page_slug'=>str_slug($request['page_name']),
        'page_slug_en'=>str_slug($request['page_name_en']),
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/bash_meclis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $meclis = BasMeclis::find($id);

      $del = $meclis->page_img;
      $image_path = public_path().'/uploads/'.$del;
      if(File::exists($image_path)) {
        File::delete($image_path);
      }
      $meclis->delete();

    $terkib = MeclisTerkib::where('meclis_id', $id)->get();

      foreach ($terkib as $value) {
        $value->delete();
      }

      Session::flash('danger','Kontent silindi');
      return redirect('/admin/bash_meclis');
    }

    public function del($id)
    {
        $terkib = MeclisTerkib::find($id);
        $terkib->delete();
        return back();
    }
    public function terkib_edit($id)
    {
        $terkib = MeclisTerkib::find($id);
        return view('admin.meclis.terkibedit',compact('terkib'));
    }
    public function terkib_update(Request $request)
    {
        $terkib = MeclisTerkib::find($request['id']);

        $terkib->update([
          'terkib'=>$request['terkib'],
          'terkib_en'=>$request['terkib_en'],
          'yer'=>$request['yer'],
          'say'=>$request['say'],
        ]);

        return \Redirect::route('bash_meclis.show', $terkib->meclis_id);
    }

    public function creat_ter($id)
    {
        return view('admin.meclis.terkibcreate',compact('id'));
    }

    public function terkib_create(Request $request)
    {
      MeclisTerkib::create([
        'terkib'=>$request['terkib'],
        'terkib_en'=>$request['terkib_en'],
        'yer'=>$request['yer'],
        'say'=>$request['say'],
        'meclis_id'=>$request['meclis_id'],
      ]);

      return \Redirect::route('bash_meclis.show', $request['meclis_id']);
    }
}
