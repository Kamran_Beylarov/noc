<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GameCategories;
use App\News;
use App\Games;
use Session;
use File;
class GameCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $game_cates = GameCategories::orderBy('cate_order', 'ASC')->get();
        return view('admin.gamecates.index',compact('game_cates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gamecates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $thumbnail1 = $request->file('cate_img');
            $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
            $thumbnail1->move(public_path('uploads'), $thumbnail);

          GameCategories::create([
              'cate_name'=>$request['cate_name'],
              'cate_name_en'=>$request['cate_name_en'],
              'cate_order'=>$request['cate_order'],
              'cate_slug'=>str_slug($request['cate_name']),
              'cate_slug_en'=>str_slug($request['cate_name_en']),
              'cate_img'=>$thumbnail,
            ]);


             Session::flash('success','Kontent əlavə edildi');
            return redirect('/admin/game_categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $game_cate = GameCategories::find($id);
      return view('admin.gamecates.edit',compact('game_cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $game_cate = GameCategories::find($id);

      if($request->file('cate_img')){

         $del = $game_cate->cate_img;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('cate_img');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $game_cate->update([
          'cate_img'=>$cover,
        ]);
      }

      $game_cate->update([
        'cate_name'=>$request['cate_name'],
        'cate_name_en'=>$request['cate_name_en'],
        'cate_order'=>$request['cate_order'],
        'cate_slug'=>str_slug($request['cate_name']),
        'cate_slug_en'=>str_slug($request['cate_name_en']),
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/game_categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $game_cate = GameCategories::with('cate_games')->find($id);

       
         $del = $game_cate->cate_img;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

      $game_cate->delete();

      Session::flash('danger','Kontent silindi');
      return redirect('/admin/game_categories');
    }
}
