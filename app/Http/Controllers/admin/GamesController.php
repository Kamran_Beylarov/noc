<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GameCategories;
use App\Games;
use Session;
use File;
use Illuminate\Support\Facades\Input;
class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $games =  Games::orderBy('id', 'DESC')->get();
      return view('admin.games.index',compact('games'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cates =  GameCategories::all();
        return view('admin.games.create',compact('cates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('game_logo');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);


        Games::create([
          'game_name'=>$request['game_name'],
          'game_name_en'=>$request['game_name_en'],
          'game_slug'=>str_slug($request['game_name']),
          'game_slug_en'=>str_slug($request['game_name_en']),
          'game_logo'=>$thumbnail,
          'game_country'=>$request['game_country'],
          'game_country_en'=>$request['game_country_en'],
          'game_city'=>$request['game_city'],
          'game_city_en'=>$request['game_city_en'],
          'start_date'=>date_create_from_format('Y-m-d', $request['start_date']),
          'end_date'=>date_create_from_format('Y-m-d', $request['end_date']),
          'gold'=>$request['gold'],
          'bronz'=>$request['bronz'],
          'silver'=>$request['silver'],
          'short_description'=>$request['short_description'],
          'short_description_en'=>$request['short_description_en'],
          'description'=>$request['description'],
          'description_en'=>$request['description_en'],
          'game_cate_id'=>$request['game_cate_id'],
        ]);

        Session::flash('success','Kontent əlavə edildi');
       return redirect('/admin/games');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = Games::find($id);
        $cates =  GameCategories::all();
        return view('admin.games.edit',compact('cates','game'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $game = Games::find($id);

      if($request->file('game_logo')){

         $del = $game->game_logo;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('game_logo');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $game->update([
          'game_logo'=>$cover,
        ]);
      }

      $game->update([
        'game_name'=>$request['game_name'],
        'game_name_en'=>$request['game_name_en'],
        'game_slug'=>str_slug($request['game_name']),
        'game_slug_en'=>str_slug($request['game_name_en']),
        'game_country'=>$request['game_country'],
        'game_country_en'=>$request['game_country_en'],
        'game_city'=>$request['game_city'],
        'game_city_en'=>$request['game_city_en'],
        'start_date'=>date_create_from_format('Y-m-d', $request['start_date']),
        'end_date'=>date_create_from_format('Y-m-d', $request['end_date']),
        'gold'=>$request['gold'],
        'bronz'=>$request['bronz'],
        'silver'=>$request['silver'],
        'short_description'=>$request['short_description'],
        'short_description_en'=>$request['short_description_en'],
        'description'=>$request['description'],
        'description_en'=>$request['description_en'],
        'game_cate_id'=>$request['game_cate_id'],
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/games');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $game = Games::find($id);

        $del = $game->game_logo;
         $image_path = public_path().'/uploads/'.$del;
         if(File::exists($image_path)) {
           File::delete($image_path);
         }
      $game->delete();


      Session::flash('danger','Kontent silindi');
      return redirect('/admin/games');
    }

    public function game_status()
    {


      $id = Input::get('id');

      $games = Games::find($id);

      if($games->index_status == 0){

        $games->update([
          'index_status' => 1
        ]);
      }else{
        $games->update([
          'index_status' => 0
        ]);
      }
        return $games;
    }
}
