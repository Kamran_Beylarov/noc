<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sports;
use Session;
use File;
class SportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $sports = Sports::all();
      return view('admin.sports.index',compact('sports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('admin.sports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('sport_icon');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);

    Sports::create([
        'sport_name'=>$request['sport_name'],
        'sport_name_en'=>$request['sport_name_en'],
        'sport_slug'=>str_slug($request['sport_name']),
        'sport_slug_en'=>str_slug($request['sport_name']),
        'sport_icon'=>$thumbnail,
      ]);


       Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/sports');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sport = Sports::find($id);
      return view('admin.sports.edit',compact('sport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $sport = Sports::find($id);

      if($request->file('sport_icon')){

         $del = $sport->sport_icon;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('sport_icon');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $sport->update([
          'sport_icon'=>$cover,
        ]);
      }

      $sport->update([
        'sport_name'=>$request['sport_name'],
        'sport_name_en'=>$request['sport_name_en'],
        'sport_slug'=>str_slug($request['sport_name']),
        'sport_slug_en'=>str_slug($request['sport_name']),
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/sports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $sports = Sports::find($id);

         $del = $sports->sport_icons;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }
      $sports->delete();


      Session::flash('danger','Kontent silindi');
      return redirect('/admin/sports');
    }
}
