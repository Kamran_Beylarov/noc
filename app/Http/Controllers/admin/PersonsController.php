<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Persons;
use Session;
use File;
use Illuminate\Support\Facades\Input;
class PersonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $persons = Persons::orderBy('person_order')->get();
      return view('admin.persons.index',compact('persons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.persons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('avatar');
      $avatar = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $avatar);

      $p_file1 = $request->file('person_img');
      $p_file = time().'.'.$p_file1->getClientOriginalName();
      $p_file1->move(public_path('uploads'), $p_file);


        Persons::create([
            'name_surname'=>$request['name_surname'],
            'name_surname_en'=>$request['name_surname_en'],
            'person_post'=>$request['person_post'],
            'person_post_en'=>$request['person_post_en'],
            'person_order'=>$request['person_order'],
            'person_info'=>$request['person_info'],
            'person_info_en'=>$request['person_info_en'],
            'page_status'=>$request['page_status'],
            'person_slug'=>str_slug($request['name_surname']),
            'person_slug_en'=>str_slug($request['name_surname_en']),
            'person_img'=>$p_file,
            'avatar'=>$avatar,
        ]);
      Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/persons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $person = Persons::find($id);
        return view('admin.persons.edit',compact('person'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $person = Persons::find($id);

      if($request->file('avatar')){

         $del = $person->avatar;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('avatar');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $person->update([
          'avatar'=>$cover,
        ]);
      }

      if($request->file('person_img')){

         $del = $person->person_img;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $P_file1 = $request->file('person_img');
        $P_file = time().'.'.$P_file1->getClientOriginalName();
        $P_file1->move(public_path('uploads'), $P_file);

        $person->update([
          'person_img'=>$P_file,
        ]);
      }

      $person->update([
        'name_surname'=>$request['name_surname'],
        'name_surname_en'=>$request['name_surname_en'],
        'person_post'=>$request['person_post'],
        'person_post_en'=>$request['person_post_en'],
        'person_order'=>$request['person_order'],
        'person_info'=>$request['person_info'],
        'person_info_en'=>$request['person_info_en'],
        'page_status'=>$request['page_status'],
        'news_slug'=>str_slug($request['name_surname']),
        'news_slug_en'=>str_slug($request['name_surname_en']),
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/persons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $person = Persons::find($id);

      $del = $person->avatar;
       $image_path = public_path().'/uploads/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }

       $del1 = $person->person_img;
        $image_path1 = public_path().'/uploads/'.$del1;
        if(File::exists($image_path1)) {
          File::delete($image_path1);
        }

      $person->delete();
      Session::flash('danger','Kontent silindi');
      return redirect('/admin/persons');
    }

    public function index_status()
    {
      $id = Input::get('id');

      $person = Persons::find($id);

      if($person->aparat == 0){

        $person->update([
          'aparat' => 1
        ]);
      }else{
        $person->update([
          'aparat' => 0
        ]);
      }
        return $person;
    }

    public function icra_komite()
    {
      $id = Input::get('id');

      $person = Persons::find($id);

      if($person->komite == 0){

        $person->update([
          'komite' => 1
        ]);
      }else{
        $person->update([
          'komite' => 0
        ]);
      }
        return $person;
    }
}
