<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DocumentGroups;
class DocumentGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = DocumentGroups::orderBy('group_order')->get();
        return view('admin.document_groups.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.document_groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DocumentGroups::create([
          'group_name'=>$request['group_name'],
          'group_name_en'=>$request['group_name_en'],
          'group_order'=>$request['group_order'],
        ]);

        return redirect('/admin/document_groups');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $group = DocumentGroups::find($id);
      return view('admin.document_groups.edit',compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = DocumentGroups::find($id);
        $group->update([
        'group_name'=>$request['group_name'],
        'group_name_en'=>$request['group_name_en'],
        'group_order'=>$request['group_order'],
      ]);

      return redirect('/admin/document_groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = DocumentGroups::find($id);
        $group->delete();
          return redirect('/admin/document_groups');
    }
}
