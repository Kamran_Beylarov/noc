<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Institutions;
use App\Members;
use Session;
use File;
class InstitutionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cates = Institutions::orderBy('ins_order')->get();
        return view('admin.institutions.index',compact('cates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('admin.institutions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request['ins_order'] == null){
        $order = 0;
      }else{
        $order = $request['ins_order'];
      }
        Institutions::create([
          'title'=>$request['title'],
          'title_en'=>$request['title_en'],
          'ins_order'=>$order,
        ]);
        Session::flash('success','Kontent əlavə edildi');
        return redirect('/admin/institutions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $cate = Institutions::find($id);
          return view('admin.institutions.edit',compact('cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $cate = Institutions::find($id);

          if($request['ins_order'] == null){
            $order = 0;
          }else{
            $order = $request['ins_order'];
          }
            $cate->update([
              'title'=>$request['title'],
              'title_en'=>$request['title_en'],
              'ins_order'=>$order,
            ]);

            Session::flash('success','Kontent redaktə edildi');
            return redirect('/admin/institutions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cate = Institutions::find($id);

        $cate->delete();
        $members = Members::where('institutions_id', $id)->get();
        foreach ($members as $key => $value) {
          $value->delete();
        }
        Session::flash('danger','Kontent silindi');
        return redirect('/admin/institutions');
    }
}
