<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Icraye;
use Session;
class IcrayyeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $icraye = Icraye::all();
        return view('admin.icraye.index',compact('icraye'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.icraye.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Icraye::create([
          'title'=>$request['title'],
          'title_en'=>$request['title_en'],
          'description'=>$request['description'],
          'description_en'=>$request['description_en'],
        ]);

        Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/executive');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $icraye = Icraye::find($id);
        return view('admin.icraye.edit',compact('icraye'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $icraye = Icraye::find($id);

        $icraye->update([
          'title'=>$request['title'],
          'title_en'=>$request['title_en'],
          'description'=>$request['description'],
          'description_en'=>$request['description_en'],
        ]);
        Session::flash('success','Kontent redaktə edildi');
        return redirect('/admin/executive');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $icraye = Icraye::find($id);
      $icraye->delete();
      Session::flash('danger','Kontent silindi');
      return redirect('/admin/executive');
    }
}
