<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Turnir;
use Session;
class TurnirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tournaments = Turnir::all();
      return view('admin.tournaments.index',compact('tournaments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tournaments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      Turnir::create([
        'tournament_name'=>$request['tournament_name'],
        'tournament_name_en'=>$request['tournament_name_en'],
      ]);
      Session::flash('success','Kontent əlavə edildi');
     return redirect('/admin/tournaments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tournament = Turnir::find($id);
        return view('admin.tournaments.edit',compact('tournament'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $tournament = Turnir::find($id);
      $tournament->update([
        'tournament_name'=>$request['tournament_name'],
        'tournament_name_en'=>$request['tournament_name_en'],
      ]);
      Session::flash('success','Kontent redaktə edildi');
     return redirect('/admin/tournaments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $tournament = Turnir::find($id);
          $tournament->delete();
          Session::flash('success','Kontent silindi');
         return redirect('/admin/tournaments');
    }
}
