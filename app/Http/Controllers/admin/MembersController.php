<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Institutions;
use App\Federations;
use App\Members;
use Session;
use File;
class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Institutions::with('cate_members')->get();
        
          return view('admin.members.index',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $federations = Federations::orderBy('federation_name')->get();
      $cates = Institutions::orderBy('ins_order')->get();
        return view('admin.members.create',compact('federations','cates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Members::create([
          'member_name'=>$request['member_name'],
          'member_name_en'=>$request['member_name_en'],
          'member_post'=>$request['member_post'],
          'member_post_en'=>$request['member_post_en'],
          'federation_id'=>$request['federation_id'],
          'institutions_id'=>$request['institutions_id'],
        ]);

        Session::flash('success','Kontent əlavə edildi');
       return redirect('/admin/members');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Members::find($id);
        $federations = Federations::orderBy('federation_name')->get();
        $cates = Institutions::orderBy('ins_order')->get();
        return view('admin.members.edit',compact('federations','cates','member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $member = Members::find($id);

      $member->update([
        'member_name'=>$request['member_name'],
        'member_name_en'=>$request['member_name_en'],
        'member_post'=>$request['member_post'],
        'member_post_en'=>$request['member_post_en'],
        'federation_id'=>$request['federation_id'],
        'institutions_id'=>$request['institutions_id'],
      ]);

      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Members::find($id);
        $member->delete();
        Session::flash('success','Kontent silindi');
        return redirect('/admin/members');
    }
}
