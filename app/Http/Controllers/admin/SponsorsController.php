<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Sponsors;
use Session;
use File;
class SponsorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sponsors = Sponsors::orderBy('sponsor_order')->get();
        return view('admin.sponsors.index',compact('sponsors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('admin.sponsors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('logo');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);

      $logo_w1 = $request->file('logo_white');
      $logo_w = time().'.'.$logo_w1->getClientOriginalName();
      $logo_w1->move(public_path('uploads'), $logo_w);

      Sponsors::create([
          'sponsor_name'=>$request['sponsor_name'],
          'sponsor_detalis'=>$request['sponsor_detalis'],
          'sponsor_detalis_en'=>$request['sponsor_detalis_en'],
          'web_site'=>$request['web_site'],
          'ins'=>$request['ins'],
          'logo'=>$thumbnail,
          'logo_white'=>$logo_w,
          'fb'=>$request['fb'],
          'sponsor_order'=>$request['sponsor_order'],
      ]);
    Session::flash('success','Kontent əlavə edildi');
    return redirect('/admin/sponsors');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sponsor = Sponsors::find($id);
      return view('admin.sponsors.edit',compact('sponsor'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $sponsors = Sponsors::find($id);

      if($request->file('logo')){

         $del = $sponsors->logo;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('logo');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $sponsors->update([
          'logo'=>$cover,
        ]);
      }

      if($request->file('logo_white')){

         $del = $sponsors->logo_white;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

          $logo_w1 = $request->file('logo_white');
          $logo_w = time().'.'.$logo_w1->getClientOriginalName();
          $logo_w1->move(public_path('uploads'), $logo_w);

        $sponsors->update([
          'logo_white'=>$logo_w,
        ]);
      }

      $sponsors->update([
        'sponsor_name'=>$request['sponsor_name'],
        'sponsor_detalis'=>$request['sponsor_detalis'],
        'sponsor_detalis_en'=>$request['sponsor_detalis_en'],
        'web_site'=>$request['web_site'],
        'ins'=>$request['ins'],
        'fb'=>$request['fb'],
        'sponsor_order'=>$request['sponsor_order'],
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/sponsors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $sponsor = Sponsors::find($id);

         $del = $sponsor->logo;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }
         $del1 = $sponsor->logo_white;
          $image_path = public_path().'/uploads/'.$del1;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }
          $sponsor->delete();

          Session::flash('danger','Kontent silindi');
          return redirect('/admin/sponsors');
    }
}
