<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Documents;
use App\DocumentGroups;
use Session;
use File;
class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Documents::orderBy('document_name')->get();
        return view('admin.documents.index',compact('documents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $groups = DocumentGroups::all();
        return view('admin.documents.create',compact('groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if($request->file('document_file')){
        $doc1 = $request->file('document_file');
        $doc = time().'.'.$doc1->getClientOriginalName();
        $doc1->move(public_path('uploads'), $doc);
      }else{
        $doc = null;
      }

        Documents::create([
          'document_name'=>$request['document_name'],
          'document_file'=>$doc,
          'group_id'=>$request['group_id'],
          'lang'=>$request['lang'],
          'url'=>$request['url'],
        ]);

        Session::flash('success','Kontent əlavə edildi');

        return redirect('/admin/documents');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $document = Documents::find($id);
        $groups = DocumentGroups::all();
      return view('admin.documents.edit',compact('document','groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Documents::find($id);


        if($request->file('document_file')){

           $del = $document->document_file;
            $image_path = public_path().'/uploads/'.$del;
            if(File::exists($image_path)) {
              File::delete($image_path);
            }

          $cover1 = $request->file('document_file');
          $cover = time().'.'.$cover1->getClientOriginalName();
          $cover1->move(public_path('uploads'), $cover);

          $document->update([
            'document_file'=>$cover,
          ]);
        }

          $document->update([
            'document_name'=>$request['document_name'],
            'group_id'=>$request['group_id'],
            'lang'=>$request['lang'],
            'url'=>$request['url'],
         ]);
         Session::flash('success','Kontent redaktə edildi');
         return redirect('/admin/documents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $document = Documents::find($id);

      $del = $document->document_file;
       $image_path = public_path().'/uploads/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }

       $document->delete();

       Session::flash('success','Kontent silindi');
       return redirect('/admin/documents');
    }
}
