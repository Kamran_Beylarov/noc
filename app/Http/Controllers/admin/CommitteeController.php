<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Committee;
use Session;
class CommitteeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $committees =Committee::all();
        return view('admin.committee.index',compact('committees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.committee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      Committee::create([
        'menu_name'=>$request['menu_name'],
        'menu_name_en'=>$request['menu_name_en'],
        'title'=>$request['title'],
        'title_en'=>$request['title_en'],
        'description'=>$request['description'],
        'description_en'=>$request['description_en'],

      ]);
        Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/committees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $committee = Committee::find($id);

      return view('admin.committee.edit',compact('committee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $committee = Committee::find($id);

        $committee->update([
          'menu_name'=>$request['menu_name'],
          'menu_name_en'=>$request['menu_name_en'],
          'title'=>$request['title'],
          'title_en'=>$request['title_en'],
          'description'=>$request['description'],
          'description_en'=>$request['description_en'],
        ]);
        Session::flash('success','Kontent redaktə edildi');
        return redirect('/admin/committees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $committee = Committee::find($id);
        $committee->delete();
        Session::flash('danger','Kontent silindi');
        return redirect('/admin/committee');
  }
}
