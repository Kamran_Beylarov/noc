<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Federations;
use Session;
use File;
class FederationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         $federations =  Federations::orderBy('federation_name')->get();
         return view('admin.federations.index',compact('federations'));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         return view('admin.federations.create');
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
       if($request->file('federation_icon')){
         $thumbnail1 = $request->file('federation_icon');
         $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
         $thumbnail1->move(public_path('uploads'), $thumbnail);
       }else{
         $thumbnail = null;
       }

       Federations::create([
         'federation_name'=>$request['federation_name'],
         'federation_type'=>$request['federation_type'],
         'federation_name_en'=>$request['federation_name_en'],
         'federation_icon'=>$thumbnail,
         'drectory_name'=>$request['drectory_name'],
         'drectory_name_en'=>$request['drectory_name_en'],
         'telefon1'=>$request['telefon1'],
         'telefon2'=>$request['telefon2'],
         'fax'=>$request['fax'],
         'web_site'=>$request['web_site'],
         'email'=>$request['email'],
         'address'=>$request['address'],
       ]);

          Session::flash('success','Kontent əlavə edildi');

          return redirect('/admin/federations');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         $federation = Federations::find($id);
         return view('admin.federations.edit',compact('federation'));
     }


     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {

         $federation = Federations::find($id);

             if($request->file('federation_icon')){

                $del = $federation->federation_icon;
                 $image_path = public_path().'/uploads/'.$del;
                 if(File::exists($image_path)) {
                   File::delete($image_path);
                 }

               $cover1 = $request->file('federation_icon');
               $cover = time().'.'.$cover1->getClientOriginalName();
               $cover1->move(public_path('uploads'), $cover);

               $federation->update([
                 'federation_icon'=>$cover,
               ]);
             }

             $federation->update([
               'federation_name'=>$request['federation_name'],
               'federation_type'=>$request['federation_type'],
               'federation_name_en'=>$request['federation_name_en'],
               'drectory_name'=>$request['drectory_name'],
               'drectory_name_en'=>$request['drectory_name_en'],
               'telefon1'=>$request['telefon1'],
               'telefon2'=>$request['telefon2'],
               'fax'=>$request['fax'],
               'web_site'=>$request['web_site'],
               'email'=>$request['email'],
               'address'=>$request['address'],
             ]);
             Session::flash('success','Kontent redaktə edildi');
             return redirect('/admin/federations');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
           $federation = Federations::find($id);

           $del = $federation->federation_icon;
            $image_path = public_path().'/uploads/'.$del;
            if(File::exists($image_path)) {
              File::delete($image_path);
            }
            $federation->delete();

            Session::flash('success','Kontent silindi');
            return redirect('/admin/federations');

     }
}
