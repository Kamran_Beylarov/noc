<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\VideoGallery;
use Session;
use File;
class VideoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $videos = VideoGallery::orderBy('video_date')->get();
      return view('admin.videogallery.index',compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('admin.videogallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            $thumbnail1 = $request->file('video_thumbnail');
            $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
            $thumbnail1->move(public_path('uploads'), $thumbnail);

            VideoGallery::create([
              'video_name'=>$request['video_name'],
              'video_name_en'=>$request['video_name_en'],
              'video_date'=>date_create_from_format('Y-m-d', $request['video_date']),
              'video_iframe'=>$request['video_iframe'],
              'video_name_slug'=>str_slug($request['video_name']),
              'video_name_en_slug'=>str_slug($request['video_name_en']),
              'video_thumbnail'=>$thumbnail,
            ]);


            Session::flash('success','Kontent əlavə edildi');
            return redirect('/admin/videogallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = VideoGallery::find($id);

        return view('admin.videogallery.edit',compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $video = VideoGallery::find($id);

      if($request->file('video_thumbnail')){

         $del = $video->video_thubnail;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('video_thumbnail');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $video->update([
          'video_thumbnail'=>$cover,
        ]);
      }

      $video->update([
        'video_name'=>$request['video_name'],
        'video_name_en'=>$request['video_name_en'],
        'video_date'=>date_create_from_format('Y-m-d', $request['video_date']),
        'video_iframe'=>$request['video_iframe'],
        'video_name_slug'=>str_slug($request['video_name']),
        'video_name_en_slug'=>str_slug($request['video_name_en'])
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/videogallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $video = VideoGallery::find($id);

      $del = $video->video_thubnail;
      $image_path = public_path().'/uploads/'.$del;
      if(File::exists($image_path)) {
        File::delete($image_path);
      }
      $video->delete();

      Session::flash('danger','Kontent silindi');
      return redirect('/admin/videogallery');
    }
}
