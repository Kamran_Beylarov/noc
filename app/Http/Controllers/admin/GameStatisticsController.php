<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GameStatistics;
use App\Games;
use App\Sports;
use Session;
class GameStatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statistics = GameStatistics::with('statistic_sport','statistic_game')->orderBy('id', 'DESC')->get();
        return view('admin.game_statistics.index',compact('statistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $games = Games::orderBy('game_name')->get();
        $sports = Sports::orderBy('sport_name')->get();
        return view('admin.game_statistics.create',compact('games','sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $sum = $request['male'] + $request['female'];
        GameStatistics::create([
          'male'=>$request['male'],
          'female'=>$request['female'],
          'sum'=>$sum,
          'link'=>$request['link'],
          'game_id'=>$request['game_id'],
          'sport_id'=>$request['sport_id'],
        ]);
        Session::flash('success','Kontent əlavə edildi');
       return redirect('/admin/game_statistics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $games = Games::orderBy('game_name')->get();
      $sports = Sports::orderBy('sport_name')->get();
      $statistic = GameStatistics::find($id);
      return view('admin.game_statistics.edit',compact('statistic','sports','games'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $statistic = GameStatistics::find($id);
        $sum = $request['male'] + $request['female'];
        $statistic->update([
            'male'=>$request['male'],
            'female'=>$request['female'],
            'sum'=>$sum,
            'link'=>$request['link'],
            'game_id'=>$request['game_id'],
            'sport_id'=>$request['sport_id'],
          ]);

      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/game_statistics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $statistic = GameStatistics::find($id);
      $statistic->delete();
      Session::flash('danger','Kontent silindi');
      return redirect('/admin/game_statistics');
    }
}
