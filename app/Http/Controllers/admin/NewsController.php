<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\Games;
use App\NewsGallery;
use Session;
use File;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('news_date', 'DESC')->paginate(20);
        
        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $games = Games::all();
          return view('admin.news.create',compact('games'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('news_thumbnail');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('news'), $thumbnail);

      if($request['news_important'] == null){
        $news_important = 0;
      }else{
        $news_important = $request['news_important'];
      }
      if($request['intro_status'] == null){
        $intro_status = 0;
      }else{
        $intro_status = $request['intro_status'];
      }

      if($request->file('intro_img')){
        $cover1 = $request->file('intro_img');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('news'), $cover);
      }else{
        $cover = null;
      }

     $news_id = News::create([
        'news_name'=>$request['news_name'],
        'lang'=>$request['lang'],
        'news_description'=>$request['news_description'],
        'news_type'=>$request['news_type'],
        'news_important'=>$news_important,
        'news_date'=>date_create_from_format('Y-m-d', $request['news_date']),
        'news_slug'=>str_slug($request['news_name']),
        'news_thumbnail'=>$thumbnail,
        'intro_img'=>$cover,
        'intro_status'=>$intro_status,
        'game_id'=>$request['game_id'],
      ])->id;

      $imgsPort = $request['img_src'];

      if ($imgsPort != NULL) {
        foreach($imgsPort as $imgsPo){

            $img = time().$imgsPo->getClientOriginalName();
            $imgsPo->move(public_path('news'), $img);
              NewsGallery::create([
                'img_src'=>$img,
                'news_id'=>$news_id,
              ]);
        }
      }

      Session::flash('success','Kontent əlavə edildi');
     return redirect('/admin/news');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $news = News::with('news_gallery')->find($id);
      // dd($gallery);
      return view('admin.news.show',compact('news','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $news = News::find($id);
      $games = Games::all();
      return view('admin.news.edit',compact('news','games'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    $news = News::find($id);

      if($request->file('news_thumbnail')){

         $del = $news->news_thumbnail;
          $image_path = public_path().'/news/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $thumbnail1 = $request->file('news_thumbnail');
        $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
        $thumbnail1->move(public_path('news'), $thumbnail);

        $news->update([
          'news_thumbnail'=>$thumbnail,
        ]);
      }

      if($request['news_important'] == null){
        $news_important = 0;
      }else{
        $news_important = $request['news_important'];
      }
      if($request['intro_status'] == null){
        $intro_status = 0;
      }else{
        $intro_status = $request['intro_status'];
      }

      if($request->file('intro_img')){

        $del = $news->intro_img;
         $image_path = public_path().'/news/'.$del;
         if(File::exists($image_path)) {
           File::delete($image_path);
         }

        $cover1 = $request->file('intro_img');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('news'), $cover);

        $news->update([
          'intro_img'=>$cover,
        ]);

      }


      $news->update([
        'news_name'=>$request['news_name'],
        'lang'=>$request['lang'],
        'news_description'=>$request['news_description'],
        'news_type'=>$request['news_type'],
        'news_important'=>$news_important,
        'news_date'=>date_create_from_format('Y-m-d', $request['news_date']),
        'news_slug'=>str_slug($request['news_name']),
        'intro_status'=>$intro_status,
        'game_id'=>$request['game_id'],
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $news = News::find($id);

           $del = $news->news_thumbnail;
            $image_path = public_path().'/news/'.$del;
            if(File::exists($image_path)) {
              File::delete($image_path);
            }

     
      $news_imgs = NewsGallery::where('news_id',$id)->get();

      foreach ($news_imgs as $value) {
        $del3 = $value->img_src;
        $image_path3 = public_path().'/news/'.$del3;
        if(File::exists($image_path3)) {
          File::delete($image_path3);
        }
        $value->delete();
      }
      $news->delete();

      Session::flash('danger','Kontent silindi');
      return redirect('/admin/news');
    }

    public function del($id)
    {

      $portImgs = NewsGallery::find($id);

      $del = $portImgs->img_src;
       $image_path = public_path().'/news/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }
      $portImgs->delete();

        return back();
    }
    public function update_img(Request $request)
    {

      $id = $request['img_id'];


      $portImg = NewsGallery::find($id);

      $del = $portImg->img_src;
       $image_path = public_path().'/news/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }


      $img1 = $request->file('img_file');
      $img = time().'.'.$img1->getClientOriginalExtension();
      $img1->move(public_path('news'), $img);

      $portImg->update([
        'img_src'=>$img,
      ]);

        return back();
    }

    public function create_img(Request $request)
    {

      $id = $request['news_id'];

      $img1 = $request->file('img_src');
      $img = time().'.'.$img1->getClientOriginalExtension();
      $img1->move(public_path('news'), $img);

      NewsGallery::create([
        'img_src'=>$img,
        'news_id'=>$id,
      ]);

        return back();
    }
}
