<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AthleteStatistics;
use App\Athlete;
use App\Turnir;
use App\Games;
use App\AthleteGameTurnament;
use Session;
class AthleteStatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statistics = AthleteStatistics::with('statistics_relation')->orderBy('id', 'DESC')->get();
        return view('admin.athlete_statistics.index', compact('statistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $athlete =Athlete::orderBy('athlete_name')->get();
      return view('admin.athlete_statistics.create', compact('athlete'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $athlete_game_tournament_id = AthleteGameTurnament::where('game_tournament_id', $request['tournament_id'])->where('game_tournament_type', $request['tournament_type'])->where('athlete_id', $request['athlete_id'])->first();

      if($athlete_game_tournament_id == null){
      $id =  AthleteGameTurnament::create([
          'game_tournament_id'=>$request['tournament_id'],
          'game_tournament_type'=>$request['tournament_type'],
          'athlete_id'=>$request['athlete_id'],
        ])->id;
      }else{
        $id = $athlete_game_tournament_id->id;
      }
      if($request['is_team'] == null){
        $is_team = 0;
      }else{
        $is_team = 1;
      }


        AthleteStatistics::create([
          'year'=>$request['year'],
          'city'=>$request['city'],
          'city_en'=>$request['city_en'],
          'sport_type'=>$request['sport_type'],
          'sport_type_en'=>$request['sport_type_en'],
          'weight'=>$request['weight'],
          'medal'=>$request['medal'],
          'is_team'=>$is_team,
          'athlete_game_tournament_id'=>$id,
        ]);

        Session::flash('success','Kontent əlavə edildi');
       return redirect('/admin/athlete_statistics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $olyp_games =Games::all();
      $non_olyp_games =Turnir::all();
      $athlete =Athlete::orderBy('athlete_name')->get();
      $statistics = AthleteStatistics::with('statistics_relation')->find($id);
      return view('admin.athlete_statistics.edit', compact('statistics','olyp_games','non_olyp_games','athlete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


      $athlete_game_tournament_id = AthleteGameTurnament::where('game_tournament_id', $request['tournament_id'])->where('game_tournament_type', $request['tournament_type'])->where('athlete_id', $request['athlete_id'])->first();

      if($athlete_game_tournament_id == null){
      $id_relation =  AthleteGameTurnament::create([
          'game_tournament_id'=>$request['tournament_id'],
          'game_tournament_type'=>$request['tournament_type'],
          'athlete_id'=>$request['athlete_id'],
        ])->id;
      }else{
        $id_relation = $athlete_game_tournament_id->id;
      }

      if($request['is_team'] == null){
        $is_team = 0;
      }else{
        $is_team = 1;
      }
        $statistics = AthleteStatistics::find($id);

        $statistics->update([
          'year'=>$request['year'],
          'city'=>$request['city'],
          'city_en'=>$request['city_en'],
          'sport_type'=>$request['sport_type'],
          'sport_type_en'=>$request['sport_type_en'],
          'weight'=>$request['weight'],
          'medal'=>$request['medal'],
          'is_team'=>$is_team,
          'athlete_game_tournament_id'=>$id_relation,
        ]);

      Session::flash('success','Kontent redaktə edildi');
     return redirect('/admin/athlete_statistics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $statistics = AthleteStatistics::find($id);
      $statistics->delete();
      Session::flash('success','Kontent silindi');
     return redirect('/admin/athlete_statistics');
    }

    public function tournaments_type(Request $request)
    {
      if($request['type'] == 2){
        return Turnir::all();
      }else{
        return Games::all();
      }
    }
}
