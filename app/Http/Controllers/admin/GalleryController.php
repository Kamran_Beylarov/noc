<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use App\GalleryImages;
use Session;
use File;
class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallerys = Gallery::orderBy('id', 'DESC')->get();
        return view('admin.fotogallery.index',compact('gallerys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.fotogallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $thumbnail1 = $request->file('gallery_thubnail');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);

      $gallery_id =  Gallery::create([
        'gallery_name'=>$request['gallery_name'],
        'gallery_name_en'=>$request['gallery_name_en'],
        'gallery_slug'=>str_slug($request['gallery_name']),
        'gallery_slug_en'=>str_slug($request['gallery_name_en']),
        'gallery_thubnail'=>$thumbnail,
      ])->id;

      $imgsPort = $request['img_src'];

      if ($imgsPort != NULL) {
        foreach($imgsPort as $imgsPo){

            $img = time().$imgsPo->getClientOriginalName();
            $imgsPo->move(public_path('uploads'), $img);

              GalleryImages::create([
                'img_src'=>$img,
                'gallery_id'=>$gallery_id,
              ]);
        }
      }
       Session::flash('success','Kontent əlavə edildi');
      return redirect('/admin/gallery');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $gallery = Gallery::with('gallery_images')->find($id);
      // dd($gallery);
      return view('admin.fotogallery.show',compact('gallery','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $gallery = Gallery::find($id);
      return view('admin.fotogallery.edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $gallery = Gallery::find($id);

      if($request->file('gallery_thubnail')){

         $del = $gallery->gallery_thubnail;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }

        $cover1 = $request->file('gallery_thubnail');
        $cover = time().'.'.$cover1->getClientOriginalName();
        $cover1->move(public_path('uploads'), $cover);

        $gallery->update([
          'gallery_thubnail'=>$cover,
        ]);
      }

      $gallery->update([
        'gallery_name'=>$request['gallery_name'],
        'gallery_name_en'=>$request['gallery_name_en'],
        'gallery_slug'=>str_slug($request['gallery_name']),
        'gallery_slug_en'=>str_slug($request['gallery_name_en']),
      ]);
      Session::flash('success','Kontent redaktə edildi');
      return redirect('/admin/gallery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $gallery = Gallery::find($id);

          $del = $gallery->gallery_thubnail;
          $image_path = public_path().'/uploads/'.$del;
          if(File::exists($image_path)) {
            File::delete($image_path);
          }
          $gallery->delete();

          $gallery_imgs = GalleryImages::where('gallery_id',$id)->get();

          foreach ($gallery_imgs as $value) {
            $del3 = $value->img_src;
            $image_path3 = public_path().'/uploads/'.$del3;
            if(File::exists($image_path3)) {
              File::delete($image_path3);
            }
            $value->delete();
          }

          Session::flash('danger','Kontent silindi');
          return redirect('/admin/gallery');
    }

    public function del($id)
    {

      $portImgs = GalleryImages::find($id);

      $del = $portImg->img_src;
       $image_path = public_path().'/uploads/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }
      $portImgs->delete();

        return back();
    }
    public function update_img(Request $request)
    {

      $id = $request['img_id'];


      $portImg = GalleryImages::find($id);

      $del = $portImg->img_src;
       $image_path = public_path().'/uploads/'.$del;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }


      $img1 = $request->file('img_file');
      $img = time().'.'.$img1->getClientOriginalExtension();
      $img1->move(public_path('uploads'), $img);

      $portImg->update([
        'img_src'=>$img,
      ]);

        return back();
    }

    public function create_img(Request $request)
    {

      $id = $request['gallery_id'];

      $img1 = $request->file('img_src');
      $img = time().'.'.$img1->getClientOriginalExtension();
      $img1->move(public_path('uploads'), $img);

      GalleryImages::create([
        'img_src'=>$img,
        'gallery_id'=>$id,
      ]);

        return back();
    }
}
