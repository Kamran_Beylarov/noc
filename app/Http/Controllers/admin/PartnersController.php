<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partners;
use Session;
use File;
class PartnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $partners = Partners::orderBy('partner_order')->get();
      return view('admin.partners.index',compact('partners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('logo');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);


    Partners::create([
        'partner_name'=>$request['partner_name'],
        'partner_name_en'=>$request['partner_name_en'],
        'web_site'=>$request['web_site'],
        'short_description'=>$request['short_description'],
        'short_description_en'=>$request['short_description_en'],
        'description'=>$request['description'],
        'description_en'=>$request['description_en'],
        'partner_order'=>$request['partner_order'],
        'partner_slug'=>str_slug($request['partner_name']),
        'partner_slug_en'=>str_slug($request['partner_name_en']),
        'logo'=>$thumbnail
      ]);



      Session::flash('success','Kontent əlavə edildi');
     return redirect('/admin/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $partner = Partners::find($id);
      return view('admin.partners.edit',compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $partner = Partners::find($id);

        if($request->file('logo')){

           $del = $partner->logo;
            $image_path = public_path().'/uploads/'.$del;
            if(File::exists($image_path)) {
              File::delete($image_path);
            }

          $cover1 = $request->file('logo');
          $cover = time().'.'.$cover1->getClientOriginalName();
          $cover1->move(public_path('uploads'), $cover);

          $partner->update([
            'logo'=>$cover,
          ]);
        }

        $partner->update([
          'partner_name'=>$request['partner_name'],
          'partner_name_en'=>$request['partner_name_en'],
          'web_site'=>$request['web_site'],
          'short_description'=>$request['short_description'],
          'short_description_en'=>$request['short_description_en'],
          'description'=>$request['description'],
          'description_en'=>$request['description_en'],
          'partner_order'=>$request['partner_order'],
          'partner_slug'=>str_slug($request['partner_name']),
          'partner_slug_en'=>str_slug($request['partner_name_en']),
        ]);
        Session::flash('success','Kontent redaktə edildi');
        return redirect('/admin/partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $partner = Partners::find($id);
        $del = $partner->logo;
         $image_path = public_path().'/uploads/'.$del;
         if(File::exists($image_path)) {
           File::delete($image_path);
         }
   $partner->delete();
   Session::flash('danger','Kontent silindi');
   return redirect('/admin/partners');
    }
}
