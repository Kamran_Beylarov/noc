<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Athlete;
use App\Sports;
use Session;
use File;
use Illuminate\Support\Facades\Input;

class AthleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $athletes = Athlete::orderBy('id', 'DESC')->paginate(20);
      return view('admin.athlete.index',compact('athletes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $sports = Sports::orderBy('sport_name')->get();
      return view('admin.athlete.create',compact('sports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $thumbnail1 = $request->file('athlete_img');
      $thumbnail = time().'.'.$thumbnail1->getClientOriginalName();
      $thumbnail1->move(public_path('uploads'), $thumbnail);
//
      Athlete::create([
        'athlete_name'=>$request['athlete_name'],
        'athlete_name_en'=>$request['athlete_name_en'],
        'athlete_birthday'=>date_create_from_format('Y-m-d', $request['athlete_birthday']),
        'athlete_height'=>$request['athlete_height'],
        'athlete_weight'=>$request['athlete_weight'],
        'athlete_gender'=>$request['athlete_gender'],
        'personal_trainer'=>$request['personal_trainer'],
        'sport_id'=>$request['sport_id'],
        'athlete_slug'=>str_slug($request['athlete_name']),
        'athlete_slug_en'=>str_slug($request['athlete_name_en']),
        'athlete_img'=>$thumbnail,
      ]);
      Session::flash('success','Kontent əlavə edildi');
     return redirect('/admin/athlete');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sports = Sports::orderBy('sport_name')->get();
      $athlete = Athlete::find($id);
      return view('admin.athlete.edit',compact('sports','athlete'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $athlete = Athlete::find($id);
        if($request->file('athlete_img')){
          $del = $athlete->athlete_img;
           $image_path = public_path().'/uploads/'.$del;
           if(File::exists($image_path)) {
             File::delete($image_path);
           }

         $cover1 = $request->file('athlete_img');
         $cover = time().'.'.$cover1->getClientOriginalName();
         $cover1->move(public_path('uploads'), $cover);

         $athlete->update([
           'athlete_img'=>$cover,
         ]);
        }


     $athlete->update([
       'athlete_name'=>$request['athlete_name'],
       'athlete_name_en'=>$request['athlete_name_en'],
       'athlete_birthday'=>date_create_from_format('Y-m-d', $request['athlete_birthday']),
       'athlete_height'=>$request['athlete_height'],
       'athlete_weight'=>$request['athlete_weight'],
       'athlete_gender'=>$request['athlete_gender'],
       'personal_trainer'=>$request['personal_trainer'],
       'sport_id'=>$request['sport_id'],
       'athlete_slug'=>str_slug($request['athlete_name']),
       'athlete_slug_en'=>str_slug($request['athlete_name_en']),
     ]);
     Session::flash('success','Kontent redaktə edildi');
     return redirect('/admin/athlete');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $athlete = Athlete::find($id);

      $del = $athlete->athlete_img;
      $image_path = public_path().'/uploads/'.$del;
      if(File::exists($image_path)) {
        File::delete($image_path);
      }
      $athlete->delete();

      Session::flash('danger','Kontent silindi');
      return redirect('/admin/athlete');
    }


    public function kommisia()
    {
      $id = Input::get('id');

      $athlete = Athlete::find($id);

      if($athlete->kommisia == 0){

        $athlete->update([
          'kommisia' => 1
        ]);
      }else{
        $athlete->update([
          'kommisia' => 0
        ]);
      }
        return $athlete;
    } 
      public function significance_status()
    {
      $id = Input::get('id');

      $athlete = Athlete::find($id);

      if($athlete->significance_status == 0){

        $athlete->update([
          'significance_status' => 1
        ]);
      }else{
        $athlete->update([
          'significance_status' => 0
        ]);
      }
        return $athlete;
    }
}
