<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;
use App\Federations;
use App\Documents;
use App\Sponsors;
use App\Persons;
use App\Committee;
use App\BasMeclis;
use App\Institutions;
use App\DocumentGroups;
use App\Icraye;
use Session;
use Config;
// use MetaTag;
class AboutController extends Controller
{
  // MetaTag::set('description', $work->portfolio_name);
  // MetaTag::set('image', asset('/uploads/'.$work->intro_photo));
  // MetaTag::set('title', $work->port_client->client_name);

    public function about()
    {


      $locale = Session::get('locale', Config::get('app.locale'));

      if($locale == 'az'){
        $fed_list = 'federation_name';
        $kommis_list = 'menu_name';
        $meta_title = 'Azərbaycan Respublikası Milli Olimpiya Komitəsi';
        $meta_description = 'Milli Olimpiya Komitəsi';
      }else{
        $fed_list = 'federation_name_en';
        $kommis_list = 'menu_name_en';
        $meta_description = 'National Olympic Committee';
        $meta_title = 'National Olympic Committee of the Republic of Azerbaijan';
      }

      // MetaTag::set('description', $meta_description);
      // MetaTag::set('image', asset('/front/img/face.png'));
      // MetaTag::set('title', $meta_title);

      $history = History::orderBy('id')->first();
      $federations = Federations::orderBy($fed_list)->where('federation_type', 1)->get();

      $document_groups = DocumentGroups::with(['sort_doc' => function ($q) use ($locale) {

        $q->where('lang', $locale);

      }])->get();
      
      $sponsors = Sponsors::orderBy('sponsor_order')->get();
      $kommisias = Committee::orderBy($kommis_list)->get();
      $bas_meclis = BasMeclis::with('meclis_terkibi')->first();
      
      $terkib_hisse = Institutions::with('cate_members')->orderBy('ins_order')->get();
      // dd($terkib_hisse);

      $olimp_dunya = Persons::where('aparat', 1)->orderBy('person_order')->get();
      $icra_komite1 = Persons::where('komite', 1)->whereBetween('person_order', [1,2])->orderBy('person_order')->get();
      $icra_komite2 = Persons::where('komite', 1)->where('person_order', '!=', 1)->where('person_order', '!=', 2)->orderBy('person_order')->get();
      $president = Persons::where('page_status', 'president')->orderBy('person_order')->first();

      $all_persons =Persons::all();

      $rehber = Persons::where('page_status', 'rehber')->orderBy('person_order')->first();

      $icraye_page = Icraye::orderBy('id')->first();
      // dd($terkib_hisse);
      return view('front.about',compact('document_groups','all_persons','icraye_page','history','federations','sponsors','olimp_dunya','icra_komite1','icra_komite2','president','rehber','kommisias','bas_meclis','terkib_hisse'));
    }
}
