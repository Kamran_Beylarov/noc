<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Games;
use App\News;
use App\Gallery;
use App\VideoGallery;
use Twitter;
use Session;
use Lang;
use Config;
use Mockery\Exception;

class IndexController extends Controller
{

    public function setLocale(Request $request)
    {
      $locale = $request['lang'];
      if (in_array($locale, ['az','en'])) {
        Session::put('locale',$locale);
        Lang::setLocale($locale);
      }
      return back();
    }

    public function index()
    {

        try
        {
         // $tweets = Twitter::getUserTimeline(array('screen_name' => 'nocazerbaijan', 'count' => 4, 'format' => 'object'));
         $tweets = null;
        }
        catch (Exception $e)
        {
          // dd(Twitter::error());
          $tweets = null;
        }
        
     
      $locale =Session::get('locale', Config::get('app.locale'));
      
      $games = Games::where('index_status', 1)->orderBy('start_date','DESC')->limit(6)->get();
      $news_index = News::where('intro_status', 1)->where('news_type', 0)->where('lang', $locale)->orderBy('id', 'DESC')->get();
      $news_desc = News::where('news_type', 0)->where('lang', $locale)->orderBy('id', 'DESC')->limit(4)->get();

      $gallerys = Gallery::orderBy('id', 'DESC')->limit(4)->get();
      return view('front.index',compact('tweets','games','news_index','gallerys','news_desc'));
    } 

}
