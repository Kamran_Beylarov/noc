<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use App\VideoGallery;
use Session;
use Config;
class GalleryController extends Controller
{
    public function galleries()
    {
      $photo_galleries = Gallery::orderBy('id', 'DESC')->paginate(12);
      $video_galleries = VideoGallery::orderBy('video_date')->limit(4)->get();

      return view('front.galleries',compact('photo_galleries','video_galleries'));
    }
    public function gallery_detalis($slug)
    {
      $locale =Session::get('locale', Config::get('app.locale'));
      if ($locale == 'az') {
        $photo_gallery = Gallery::with('gallery_images')->where('gallery_slug', $slug)->first();
      }else {
        $photo_gallery = Gallery::with('gallery_images')->Where('gallery_slug_en' , $slug)->first();
      }

      if ($photo_gallery) {
          return view('front.gallerydetalis',compact('photo_gallery'));
      }else {
        return redirect('/');
      }

    }
    // public function video_detalis($slug)
    // {
    //   $locale =Session::get('locale', Config::get('app.locale'));
    //   if ($locale == 'az') {
    //     $video_gallery = VideoGallery::where('video_name_slug', $slug)->first();
    //   }else {
    //     $video_gallery = VideoGallery::where('video_name_slug_en', $slug)->first();
    //   }

    //   if ($video_gallery) {
    //       return view('front.videodetalis',compact('video_gallery'));
    //   }else {
    //     return redirect('/');
    //   }
    // }
}
