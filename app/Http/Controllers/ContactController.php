<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Config;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class ContactController extends Controller
{
    public function subscribe(Request $request)
    {
        $locale = Session::get('locale',Config::get('app.locale'));
        if($locale == 'az'){
            $success = 'Təşəkkür edirik';
            $error = 'Yenidən yoxlayın';
        }else{
            $success = 'Thank you';
            $error = 'Please try again';
        }
        
        $mail = new PHPMailer(true);
        try {
          $mail->SMTPOptions = array(
           'ssl' => array(
               'verify_peer' => false,
               'verify_peer_name' => false,
               'allow_self_signed' => true
           )
        );
          //Server settings
          $mail->SMTPDebug = 0;                                 // Enable verbose debug output
          $mail->isSMTP();                                      // Set mailer to use SMTP
          $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
          $mail->SMTPAuth = true;                               // Enable SMTP authentication
          $mail->Username = 'endorphinbaku@gmail.com';                 // SMTP username
          $mail->Password = 'edfjcnltzzbdedvn';                           // SMTP password
          $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
          $mail->Port = 587;                                    // TCP port to connect to

          //Recipients
          $mail->setFrom('endorphinbaky@gmail.com', 'Noc Web Site');
          $mail->addAddress('beylarov747@outlook.com');               // Name is optional

         
          $mail->isHTML(true);                                  // Set email format to HTML
          $mail->Subject = "NOC Web Site Subscribe";
          $mail->Body    = '<p> <b><div> <img src="https://noc.endorphin.az/news/1622411226.4L6A0269-min.JPG"></div></b> :'.$request['email']."</p>";
         
          $mail->send();
     
          Session::flash('success', $success);
      } catch (Exception $e) {
          Session::flash('error', $error);
      }
        return redirect('/');
    }
}
