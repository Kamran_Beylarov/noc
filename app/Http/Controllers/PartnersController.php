<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partners;
use Session;
use Config;
class PartnersController extends Controller
{
    public function partners()
    {
      $partners = Partners::orderBy('partner_order')->get();
      return view('front.partners',compact('partners'));
    }

    public function partner_detalis($slug)
    {

      $locale = Session::get('locale', Config::get('app.locale'));
      if ($locale == 'az') {
        $partner = Partners::where('partner_slug' ,$slug)->first();
      }else{
        $partner = Partners::where('partner_slug_en' ,$slug)->first();
      }

      if ($partner != null) {
        return view('front.partner_detalis',compact('partner'));
      }else{
        return redirect('/');
      }
    }
}
