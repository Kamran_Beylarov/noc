<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turnir extends Model
{
  protected $table = 'tournaments';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'tournament_name',
    'tournament_name_en',
  ];

  public function tournament_statistics(){
  return $this->hasMany('App\AthleteStatistics','athlete_id');
}
}
