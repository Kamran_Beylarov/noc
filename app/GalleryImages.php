<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImages extends Model
{
  protected $table = 'gallery_imgs';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id','gallery_id','img_src'
  ];

  public function images_gallery(){
  return $this->belongsTo('App\Gallery','gallery_id','id');
}

}
