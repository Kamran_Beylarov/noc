<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publications extends Model
{
  protected $table = 'publications';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id','publication_name','publication_name_en','publication_file','publication_thumbnail','author_name','author_name_en'
  ];
}
