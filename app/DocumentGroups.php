<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentGroups extends Model
{
  protected $table = 'document_groups';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'group_name',
    'group_name_en',
    'group_order',
  ];

  public function sort_doc(){
    return $this->hasMany('App\Documents','group_id','id');
  }
}
