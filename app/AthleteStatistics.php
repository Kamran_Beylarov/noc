<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AthleteStatistics extends Model
{
  protected $table = 'athlete_statistics';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'year',
    'city',
    'city_en',
    'sport_type',
    'sport_type_en',
    'weight',
    'medal',
    'athlete_game_tournament_id',
    'is_team',
  ];


  public function statistics_relation()
  {
    return $this->belongsTo('App\AthleteGameTurnament','athlete_game_tournament_id', 'id');
  }

    public static function boot() {
      parent::boot();
      self::deleting(function($statistics) { 
        $all_this_statistics = AthleteStatistics::where('athlete_game_tournament_id', $statistics->athlete_game_tournament_id)->get();
        if(count($all_this_statistics) == 1){
          $statistics->statistics_relation()->delete();
        }
      });
    }

}
