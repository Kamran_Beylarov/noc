<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
  protected $table = 'partners';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'partner_name',
    'partner_name_en',
    'logo',
    'web_site',
    'short_description',
    'short_description_en',
    'description',
    'description_en',
    'partner_slug',
    'partner_slug_en',
    'partner_order',
  ];

}
