<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AthleteGameTurnament extends Model
{
  protected $table = 'athlete_game_tournament';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'athlete_id',
    'game_tournament_id',
    'game_tournament_type',
  ];


  public function athlete_game_tour()
  {
    return $this->belongsTo('App\Athlete','athlete_id', 'id');
  }
  public function relation_game(){
    return $this->belongsTo('App\Games','game_tournament_id','id');
  }
  public function relation_tour(){
    return $this->belongsTo('App\Turnir','game_tournament_id','id');
  }
  public function relation_statistics(){
    return $this->hasMany('App\AthleteStatistics','athlete_game_tournament_id');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($relation_statistics) { 
         $relation_statistics->relation_statistics()->each(function($statistic){
          $statistic->delete();
         });
    });
}
}