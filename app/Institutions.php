<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institutions extends Model
{
  protected $table = 'institutions';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'title',
    'title_en',
    'ins_order',
  ];

  public function cate_members(){
  return $this->hasMany('App\Members','institutions_id');
}
}
