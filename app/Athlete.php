<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Athlete extends Model
{
  protected $table = 'athlete';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'athlete_img',
    'athlete_name',
    'athlete_name_en',
    'athlete_birthday',
    'athlete_height',
    'athlete_weight',
    'athlete_slug',
    'athlete_slug_en',
    'athlete_gender',
    'personal_trainer',
    'kommisia',
    'sport_id',
    'significance_status'
  ];

  public function athlete_sport(){
  return $this->belongsTo('App\Sports','sport_id','id');
}
  public function athlete_stat_relation(){
   return $this->hasMany('App\AthleteGameTurnament','athlete_id','id');
 }
  public function athlete_games_statistics(){
   return $this->hasMany('App\AthleteStatistics','athlete_id','id');
 }

 public static function boot() {
  parent::boot();
  self::deleting(function($ath_game_statistic) { 
    
      $ath_game_statistic->athlete_stat_relation()->each(function($realtions) {
          $realtions->delete(); 
     });
  });
}

}
