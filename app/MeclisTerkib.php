<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeclisTerkib extends Model
{
  protected $table = 'meclis_terkib';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'terkib',
    'terkib_en',
    'yer',
    'say',
    'meclis_id',
  ];
}
