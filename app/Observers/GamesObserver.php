<?php

namespace App\Observers;

use App\Games;
use File;
class GamesObserver
{
    /**
     * Handle the games "created" event.
     *
     * @param  \App\Games  $games
     * @return void
     */
    public function created(Games $games)
    {
        //
    }

    /**
     * Handle the games "updated" event.
     *
     * @param  \App\Games  $games
     * @return void
     */
    public function updated(Games $games)
    {
        //
    }

    /**
     * Handle the games "deleted" event.
     *
     * @param  \App\Games  $games
     * @return void
     */
    public function deleted(Games $games)
    {
        // dd($games);
        $del = $games->game_logo;
        $image_path = public_path().'/uploads/'.$del;
        if(File::exists($image_path)) {
          File::delete($image_path);
        }
    }

    /**
     * Handle the games "restored" event.
     *
     * @param  \App\Games  $games
     * @return void
     */
    public function restored(Games $games)
    {
        //
    }

    /**
     * Handle the games "force deleted" event.
     *
     * @param  \App\Games  $games
     * @return void
     */
    public function forceDeleted(Games $games)
    {
        //
    }
}
