<?php

namespace App\Observers;

use App\AthleteGameTurnament;
use App\AthleteStatistics;
class AthleteGameTurnamentObserver
{
    /**
     * Handle the athlete game turnament "created" event.
     *
     * @param  \App\AthleteGameTurnament  $athleteGameTurnament
     * @return void
     */
    public function created(AthleteGameTurnament $athleteGameTurnament)
    {
        //
    }

    /**
     * Handle the athlete game turnament "updated" event.
     *
     * @param  \App\AthleteGameTurnament  $athleteGameTurnament
     * @return void
     */
    public function updated(AthleteGameTurnament $athleteGameTurnament)
    {
        //
    }

    /**
     * Handle the athlete game turnament "deleted" event.
     *
     * @param  \App\AthleteGameTurnament  $athleteGameTurnament
     * @return void
     */
    public function deleted(AthleteGameTurnament $athleteGameTurnament)
    {
        
       $id = $athleteGameTurnament->id;
       $statistics = AthleteStatistics::where('athlete_game_tournament_id', $id)->get();
       foreach($statistics as $statistic){
        $statistic->delete();
       }
    }

    /**
     * Handle the athlete game turnament "restored" event.
     *
     * @param  \App\AthleteGameTurnament  $athleteGameTurnament
     * @return void
     */
    public function restored(AthleteGameTurnament $athleteGameTurnament)
    {
        //
    }

    /**
     * Handle the athlete game turnament "force deleted" event.
     *
     * @param  \App\AthleteGameTurnament  $athleteGameTurnament
     * @return void
     */
    public function forceDeleted(AthleteGameTurnament $athleteGameTurnament)
    {
        //
    }
}
