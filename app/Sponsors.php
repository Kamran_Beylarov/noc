<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsors extends Model
{
  protected $table = 'sponsors';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id','logo','logo_white','sponsor_name','sponsor_detalis','sponsor_detalis_en','web_site','fb','ins','sponsor_order'
  ];
}
