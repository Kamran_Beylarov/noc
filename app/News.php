<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  protected $table = 'news';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'news_name',
    'news_description',
    'news_slug',
    'news_type',
    'news_date',
    'news_thumbnail',
    'news_important',
    'intro_img',
    'intro_status',
    'game_id',
    'lang',
    'view_count',
  ];
  public function news_gallery(){
  return $this->hasMany('App\NewsGallery','news_id');
 } 
 public function news_game(){
  return $this->belongsTo('App\Games','game_id','id');
 }

 public static function boot() {
  parent::boot();
  self::deleting(function($news) { // before delete() method call this
       $news->news_gallery()->each(function($gallery) {
          $gallery->delete(); // <-- direct deletion
       });
      //  $user->posts()->each(function($post) {
      //     $post->delete(); // <-- raise another deleting event on Post to delete comments
      //  });
       // do the rest of the cleanup...
  });
}
}