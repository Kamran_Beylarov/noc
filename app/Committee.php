<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{
  protected $table = 'committee';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'menu_name',
    'menu_name_en',
    'title',
    'title_en',
    'description',
    'description_en',
  ];
}
