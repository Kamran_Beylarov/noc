<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persons extends Model
{
  protected $table = 'persons';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'avatar',
    'name_surname',
    'name_surname_en',
    'person_post',
    'person_post_en',
    'person_order',
    'person_img',
    'person_info',
    'person_info_en',
    'aparat',
    'komite',
    'person_slug',
    'person_slug_en',
    'page_status',
  ];
}
