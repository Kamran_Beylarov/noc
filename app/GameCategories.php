<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameCategories extends Model
{
  protected $table = 'game_categories';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'cate_name',
    'cate_name_en',
    'cate_slug',
    'cate_slug_en',
    'cate_img',
    'cate_order',
  ];
  public function cate_games()
  {
    return $this->hasMany('App\Games','game_cate_id');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($game_cates) { 
         $game_cates->cate_games()->each(function($games) {
            $games->delete(); 
          });
      });
  }
}