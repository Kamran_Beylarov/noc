<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
  protected $table = 'documents';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'document_name',
    'document_file',
    'group_id',
    'lang',
    'url',
  ];
}
