<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
  protected $table = 'gallery';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id','gallery_thubnail','gallery_name','gallery_name_en','gallery_slug','gallery_slug_en'
  ];

  public function gallery_images(){
  return $this->hasMany('App\GalleryImages','gallery_id');
}
//   public function mekt_eu_mekt(){
//   return $this->belongsTo('App\Scools','other_scool_id','id');
// }

}
