<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
  protected $table = 'games';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'game_name',
    'game_name_en',
    'game_slug',
    'game_slug_en',
    'game_logo',
    'game_country',
    'game_country_en',
    'game_city',
    'game_city_en',
    'start_date',
    'end_date',
    'gold',
    'bronz',
    'silver',
    'short_description',
    'short_description_en',
    'description',
    'description_en',
    'game_cate_id',
    'index_status',
  ];

  public function game_statistics()
  {
    return $this->hasMany('App\GameStatistics','game_id');
  }
    public function game_news()
  {
    return $this->hasMany('App\News','game_id');
  }
  public function game_athlete_turnament()
  {
    return $this->hasOne('App\AthleteGameTurnament','game_tournament_id');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($game) { 
         $game->game_news()->each(function($news) {
            $news->delete(); 
         });
         $game->game_statistics()->each(function($statistics) {
            $statistics->delete(); 
         }); 
         $game->game_athlete_turnament()->each(function($g_t_relation) {
            $g_t_relation->where('game_tournament_type', 1)->delete();
         });
       
    });
}

}
