<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icraye extends Model
{
  protected $table = 'icraye';
  protected $primaryKey = 'id';
  public $timestamps = false;
  protected $fillable = [
    'id',
    'title',
    'title_en',
    'description',
    'description_en',
  ];
}
