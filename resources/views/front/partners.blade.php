@extends('layouts.dizayn')

@section('content')
<div id="partners">
    <section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
        <div class="container">
            <h1 class="title uppercase">{{ trans('lang.partners') }}</h1>
        </div>
    </section>
    <div class="container">
        <div class="partners_wrapper">

          @foreach($partners as $partner)
            <div class="partner-card">
                <div class="photo">
                    <img src="/uploads/{{$partner->logo}}" alt="">
                </div>
                <p class="name">@if($locale == 'az'){{$partner->partner_name}}@else{{$partner->partner_name_en}}@endif</p>
                <a href="/partner/@if($locale == 'az'){{$partner->partner_slug}}@else{{$partner->partner_slug_en}}@endif" class="btn_more">{{ trans('lang.detailis') }}</a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
