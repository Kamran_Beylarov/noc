@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.programs') }} </h1>
    </div>
</section>
<section id="program-single">
    <div class="container">
      
        <div class="open-content">
            <h1 class="head_title">{{$program->news_name}}</h1>
            <div class="details">
              <?php
                $activ_date= strtotime($program->news_date);
               ?>
                <p class="date">{{date('d',$activ_date)}} {{$months[date('m',$activ_date)]}} {{date('Y',$activ_date)}}</p>
                <div class="web-contact">
                    <div class="social-media">
                    <a href="https://facebook.com/sharer.php?u={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-facebook"></i></a>
                <a href="https://twitter.com/intent/tweet?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-twitter"></i></a>
                <a href="https://api.whatsapp.com/send?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-whatsapp"></i></a>
                    </div>
                    
                </div>
            </div>
            <div class="desc">
            {!! $program->news_description !!}
            </div>
          {{--  <div class="open-img">
                <img src="/uploads/{{$program->news_thumbnail}}" alt="">
            </div>--}}

            <div class="open-gallery">
              @foreach($program->news_gallery as $gallery)
                <div class="gallery-img">
                    <img src="/nwes/{{$gallery->img_src}}" alt="">
                </div>
              @endforeach
            </div>
        </div>
    </div>
</section>

@endsection
