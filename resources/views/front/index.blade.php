@extends('layouts.dizayn')

@section('content')
    <div id="index">
      <section id="home-slider">
          <div class="container">
              <div class="owl-carousel">
                @foreach($news_index as $news)
                  <div class="item">
                      <img src="/news/{{$news->intro_img}}" alt="">
                      <div class="overlay">
                          <div class="content_wrapper">
                              <!-- <p>Növbəti ilə keçirilmiş</p> -->
                              <a href="/news/{{$news->news_slug}}" class="title">
                                {{$news->news_name}}

                              </a>
                              <a href="/news/{{$news->news_slug}}" class="btn_more" style="text-transform:uppercase">{{trans('lang.detailis')}}</a>
                          </div>
                      </div>
                  </div>
                  @endforeach
              </div>
          </div>
      </section>      <section id="olympic-games">
          <div class="container">
              <div class="wrapper">
                @foreach($games as $game)
                  <a href="/game/@if($locale == 'az'){{$game->game_slug}}@else{{$game->game_slug_en}}@endif" class="game_link">
                      <img src="/uploads/{{$game->game_logo}}" alt="">
                  </a>
                @endforeach
              </div>
          </div>
      </section>      <section id="latest-news">
          <div class="container">
              <div class="header_link">
                  <h1 class="title double">{!! trans('lang.last_news') !!}</h1>
                  <a href="/articles" class="all_links">
                      {{ trans('lang.all_news') }}
                      <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                  </a>
              </div>
              <div class="wrapper">
                <style>
                  .c_text br{
                    display: none !important;
                  }
                </style>
                @foreach($news_desc as $n)
                  <a href="/news/{{$n->news_slug}}" class="news-card">
                      <div class="card_box">
                          <div class="c_img">
                              <img src="/news/{{$n->news_thumbnail}}" alt="">
                          </div>
                          @php
                            $news_date= strtotime($n->news_date);
                            // $pieces = explode(" ", $n->news_name);
                            // $news_name = implode(" ", array_splice($pieces, 0, 5));
                            $pieces1 = explode(" ", $n->news_description);
                            $text = implode(" ", array_splice($pieces1, 0, 10));
                          @endphp
                          <h2 class="c_title">{{$n->news_name}}</h2>
                          <div class="c_text">

                            {!! $text !!}... <span class="more">{{ trans('lang.detailis') }}</span>
                          </p>
                          </div>
                      </div>
                      <span class="c_date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</span>
                  </a>
                  @endforeach
              </div>
          </div>
      </section>
        <section id="gallery-section">
          <div class="container">
              <div class="header_link">
                  <h1 class="title">{{ trans('lang.gallery') }}</h1>
                  <a href="/galleries" class="all_links">
                    {{ trans('lang.all_galleries') }}
                      <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                  </a>
              </div>
              <div class="gallery_grid">
                  @foreach($gallerys as $gallery)
                  <a href="/gallery/@if($locale == 'az'){{$gallery->gallery_slug}}@else{{$gallery->gallery_slug_en}}@endif" class="box">
                      <img src="/uploads/{{$gallery->gallery_thubnail}}" alt="">
                       <div class="video-overlay">
                          <div class="date-icon">
                            <?php
                              $gallery_date= strtotime($gallery->gallery_date);
                             ?>
                              <p class="date"></p>
                          </div>
                          <h4 class="title">
                            @if($locale == 'az'){{$gallery->gallery_name}}@else{{$gallery->gallery_name_en}}@endif
                          </h4>
                      </div>
                  </a>
                  @endforeach
              </div>
          </div>
      </section>  
      <section id="latest-twits">
        {{--    <div class="container">
              <div class="header_link">
                  <h1 class="title double">Ən son<b>Tvitlər</b></h1>
                  <a href="https://twitter.com/nocazerbaijan" class="all_links"  target="_blank">
                    {{ trans('lang.follow_us_twit') }}
                      <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                  </a>
              </div>
              <div class="wrapper">
                  @foreach($tweets as $tweet)
                  <div  class="twit-card" target="_blank">
                   @php
                      $created_at = strtotime($tweet->created_at);
                   @endphp  
                     
                      <p class="t_date">{{date('d',$created_at)}} {{$months[date('m',$created_at)]}} </p>
                      <p class="t_content">{{$tweet->text}}</p>
                  </div>  
                  @endforeach
            </div>
          </div> --}}
      </section> 
       </div>
@endsection
