@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.events') }}
    </h1>
    </div>
</section>
<section id="program-single">
    <div class="container">
        <div class="open-content">
            <h1 class="head_title">{{$activity->news_name}}</h1>
            <div class="details">
              <?php
                $activ_date= strtotime($activity->news_date);
               ?>

                <p class="date">{{date('d',$activ_date)}} {{$months[date('m',$activ_date)]}} {{date('Y',$activ_date)}}</p>
                <div class="web-contact">
                    <div class="social-media">
                        <a href="https://facebook.com/sharer.php?u={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/intent/tweet?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-twitter"></i></a>
                        <a href="https://api.whatsapp.com/send?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-whatsapp"></i></a>
                    </div>
                    <!-- <a href="" class="link">
                        <img src="../assets/link.svg" alt="">
                        <span>Paylaşım linkini yarat</span>
                    </a> -->
                </div>
            </div>
            <div class="desc">
              <?php $texts = explode(" ", $activity->news_description); ?>
              <?php for ($i=0; $i <count($texts)/2 ; $i++) {  ?> {!! $texts[$i] !!} <?php } ?>
            </div>
            <div class="open-img">
                <img src="/news/{{$activity->news_thumbnail}}" alt="">
            </div>
            <div class="desc">
              <?php $texts = explode(" ", $activity->news_description); ?>
          <?php for ($i=count($texts)/2; $i <count($texts) ; $i++) {  ?> {!! $texts[$i] !!} <?php } ?>
            </div>
            <div class="open-gallery">
              @foreach($activity->news_gallery as $gallery)
                <div class="gallery-img">
                    <img src="/news/{{$gallery->img_src}}" alt="">
                </div>
              @endforeach
            </div>
        </div>
    </div>
</section>

@endsection
