@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.activities_u') }}</h1>
    </div>
</section>
<section id="programs">
    <div class="container">
        <div class="header_link">
            <h1 class="title">{{ trans('lang.programs') }}</h1>
             <a href="/programs" class="all_links">
                {{ trans('lang.all_programs') }}
                <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
            </a>
        </div>
        <div class="programs-content">
          @foreach($programs as $program)
            <a href="/programs/{{$program->news_slug}}" class="program-card">
                <div class="card-img">
                    <img src="/news/{{$program->news_thumbnail}}" alt="">
                </div>
                <h4 class="card-title">
                    {{$program->news_name}}
                </h4>
                <div class="card-info">
                    <?php
                        $pieces1 = explode(" ", $program->news_description);
                        $text = implode(" ", array_splice($pieces1, 0, 10));
                      ?>
                      {!! $text !!}
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <section id="latest-news">
        <div class="container">
            <div class="header_link">
                <h1 class="title">{{ trans('lang.events') }}</h1>
                 <a href="/activities" class="all_links" >
                   {{ trans('lang.all_events') }}
                    <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                </a> 
            </div>
            <div class="wrapper">
              @foreach($activitys as $activity)
                <a href="/activity/{{$activity->news_slug}}" class="news-card">
                    <div class="card_box">
                        <div class="c_img">
                            <img src="/news/{{$activity->news_thumbnail}}" alt="">
                        </div>
                        <h2 class="c_title">{{$activity->news_name}}</h2>
                        <div class="c_text">
                          <?php
                             $pieces1 = explode(" ", $activity->news_description);
                             $text = implode(" ", array_splice($pieces1, 0, 10)); 
                             ?>
                            {!! $text !!}
                        </div>
                    </div>

                      <?php
                        $activ_date= strtotime($activity->news_date);
                       ?>

                    <span class="c_date">{{date('d',$activ_date)}} {{$months[date('m',$activ_date)]}} {{date('Y',$activ_date)}}</span>
                </a>
              @endforeach

          </div>
        </div>
    </section>

    <div class="publication">
        <div class="container">
            <div class="header_link">
                <h1 class="title">{{ trans('lang.publications') }}</h1>
                <!-- <a href="" class="all_links">
                    Bütün nəşrlər
                    <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                </a> -->
            </div>

            <div class="publication-content">
                @foreach($publications as $publication)
                <div class="publication-card">
                    <div class="item-img">
                        <img src="/uploads/{{$publication->publication_thumbnail}}" alt="">
                    </div>
                    <span>@if($locale == 'az'){{$publication->author_name}}@else {{$publication->author_name_en}} @endif</span>
                    <p class="item-name">@if($locale == 'az'){{$publication->publication_name}}@else{{$publication->publication_name_en}}@endif</p>
                    <a href="/uploads/{{$publication->publication_file}}" download="/uploads/{{$publication->publication_file}}" class="btn_grey download">{{ trans('lang.download') }}</a>
                </div>
                 @endforeach
           </div>

        </div>
    </div>

</section>

@endsection
