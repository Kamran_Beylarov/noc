@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.games') }}</h1>
    </div>
</section>
<div class="container">
    <div id="games">
      @foreach($game_categories as $cate)
        <a href="/game-categories/@if($locale == 'az'){{$cate->cate_slug}}@else{{$cate->cate_slug_en}}@endif" class="game-card">
            <img src="/uploads/{{$cate->cate_img}}" alt="">
            <div class="game-title">
                <p>
                  @if($locale == 'az'){{$cate->cate_name}}@else{{$cate->cate_name_en}}@endif
                </p>
            </div>
        </a>
      @endforeach
    </div>
</div>
@endsection
