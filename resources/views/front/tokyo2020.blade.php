@extends('layouts.dizayn')

@section('content')
    <div id="partners">
        <section id="section-banner" style="background-image: url({{ url('front/assets/bg.png') }});">
            <div class="container">
                <h1 class="title uppercase">TOKYO 2020</h1>
            </div>
        </section>
        <section id="widgets">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 iframe_main">
                        {{-- &ignoreBannerIds=red,green,blue,purple --}}
                        <iframe
                            src='https://webocsitok.ovpobs.tv/olympic-family-iframe/?widget=schedule&locale=en-GB&featuredOrganisationCode=AZE&partnerName=AZE&domain_source=www.olympic.az'
                            id="otchannel05"></iframe>
                    </div>
                    <div class="col-md-6 iframe_main">
                        <iframe
                            src="https://webocsitok.ovpobs.tv/olympic-family-iframe/?widget=schedule&locale=en-GB&organisationCode=AZE&partnerName=AZE&domain_source=www.olympic.az&ignoreBannerIds=red,green,blue,purple"
                            id="otchannel04"></iframe>
                    </div>
                    <div class="col-md-6 iframe_main">
    
    
                        <iframe
                            src="https://webocsitok.ovpobs.tv/olympic-family-iframe/?widget=medals&locale=en-GB&organisationCode=AZE&partnerName=AZE&domain_source=www.olympic.az&ignoreBannerIds=red,green,blue,purple"
                            id="otchannel01"></iframe>
                    </div>
                    <div class="col-md-6 iframe_main">
                        <iframe
                            src="https://webocsitok.ovpobs.tv/olympic-family-iframe/?widget=medals&locale=en-GB&featuredOrganisationCode=AZE&partnerName=AZE&domain_source=www.olympic.az&ignoreBannerIds=red,green,blue,purple"
                            id="otchannel02"></iframe>
                    </div>
                    <div class="col-md-6 iframe_main">
                        <iframe
                            src="https://webocsitok.ovpobs.tv/olympic-family-iframe/?widget=results&locale=en-GB&partnerName=AZE&domain_source=www.olympic.az&ignoreBannerIds=red,green,blue,purple"
                           id="otchannel03"></iframe>
                    </div>
    
                </div>
            </div>
        </section>
    </div>
@endsection
