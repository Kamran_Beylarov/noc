@extends('layouts.dizayn')

@section('content')
<style media="screen">
  #gallery-inside .gallery_display_img .owl-prev, #gallery-inside .gallery_display_img .owl-next{
    display:none !important;
  }
  iframe{
    width: 100%;
    height: 100%;
  }
</style>
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">Qalereya</h1>
    </div>
</section>
<div id="gallery-inside">
    <div class="container">
      <div class="open-content">
        <div class="details ">

            <p class="category_title">Video Qalereya</p>
            <div class="web-contact">
                <div class="social-media">
                <a href="https://facebook.com/sharer.php?u={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-facebook"></i></a>
                <a href="https://twitter.com/intent/tweet?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-twitter"></i></a>
                <a href="https://api.whatsapp.com/send?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-whatsapp"></i></a>
                </div>
                <!-- <a href="" class="link">
                    <img src="../assets/link.svg" alt="">
                    <span>Paylaşım linkini yarat</span>
                </a> -->
            </div>
        </div>
      </div>
        <h1 class="section_title">
          @if($locale == 'az'){{$video_gallery->video_name}}@else{{$video_gallery->video_name_en}}@endif
          </h1>
        <div class="gallery_display_img ">
          {!!$video_gallery->video_iframe!!}
        </div>
    </div>
</div>

@endsection
