@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.gallery_u') }}</h1>
    </div>
</section>
<div id="gallery">
    <div class="container">
        <div class="gallery_img_wrapper">
          @foreach($photo_galleries as $photo_gallery)
            <a href="/gallery/@if($locale == 'az'){{$photo_gallery->gallery_slug}}@else{{$photo_gallery->gallery_slug_en}}@endif" class="photo">
                <img src="/uploads/{{$photo_gallery->gallery_thubnail}}" alt="">
                <div class="overlay">
                  <?php
                    $gallery_date= strtotime($photo_gallery->gallery_date);
                   ?>
                    <p class="date"></p>
                    <h4 class="title">
                      @if($locale == 'az'){{$photo_gallery->gallery_name}}@else{{$photo_gallery->gallery_name_en}}@endif
                      </h4>
                </div>
            </a>
            @endforeach
        </div>
       {{ $photo_galleries->render()}}
     </div>
</div>

@endsection
