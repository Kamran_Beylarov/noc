@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.games') }}</h1>
    </div>
</section>
<div class="container">
    <div id="game-category">
        <div class="game-list-wrapper">
      @foreach($games as $game)
        <a href="/game/@if($locale == 'az'){{$game->game_slug}}@else{{$game->game_slug_en}}@endif" class="box">
            <div class="imgHolder">
                <img src="/uploads/{{$game->game_logo}}" alt="">
            </div>
        </a>
      @endforeach
    </div>
    </div>
</div>
@endsection
