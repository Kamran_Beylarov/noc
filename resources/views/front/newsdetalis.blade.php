@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.news') }}</h1>
    </div>
</section>
<section id="news-page">
    <div class="container">
        <div class="news-wrapper">
            <div class="news-content">
                <div class="open-content">
                    <h1 class="head_title">{{$news_detalis->news_name}}</h1>
                    <div class="details">
                      <?php
                        
                        $news_date= strtotime($news_detalis->news_date);
                       ?>
                        <p class="date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}} <span> <i class="far fa-eye"></i> {{$news_detalis->view_count}} </span></p>
                        
                        <div class="web-contact">
                            <div class="social-media">
                            <a href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->full()}}" target="_blank" class="sm-icon" style="color:#0A66C2"><i class="fab fa-linkedin"></i></a>
                            <a href="https://facebook.com/sharer.php?u={{url()->full()}}" target="_blank" class="sm-icon" style="color:#2D88FF"><i class="fab fa-facebook"></i></a>
                <a href="https://twitter.com/intent/tweet?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-twitter"></i></a>
                <a href="https://api.whatsapp.com/send?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-whatsapp"></i></a>
                            </div>
                            <!-- <a href="" class="link">
                                <img src="../assets/link.svg" alt="">
                                <span>Paylaşım linkini yarat</span>
                            </a> -->
                        </div>
                    </div>

                    <style media="screen">
                        .desc img{
                          display:none !important;
                        }
                    </style>

                    <div class="open-img">
                        <img src="/news/{{$news_detalis->news_thumbnail}}" alt="">
                    </div>
                    <div class="text_wrapper">
                        <div class="desc">
                          {!! $news_detalis->news_description !!}
                        </div>

                    </div>
                  
                    @if(count($news_detalis->news_gallery) != 0)
                    <div class="news_gallery_main" >
                        <div id="news_gallery" class="owl-carousel">
                            @foreach($news_detalis->news_gallery as $imgs)
                            <div class="item"><a href="/news/{{$imgs->img_src}}" data-fancybox="gallery"><img src="/news/{{$imgs->img_src}}" alt=""></a></div>
                            @endforeach
                        </div>
                        <div class="prev"><i class="fas fa-chevron-left"></i></div>
                        <div class="next"><i class="fas fa-chevron-right"></i></div>
                    </div>
                    @endif
                </div>
                <div class="header_link">
                    <h2 class="title">{{ trans('lang.similar_news') }}</h2>
                </div>
                <style>
                    .c_text br{
                      display: none !important;
                    }
                  </style>
                <div class="news-container">
                  @foreach($news_benzer as $news_ben)
                    <a href="/news/{{$news_ben->news_slug}}" class="news-card">
                        <div class="card_box">
                            <div class="c_img">
                                <img src="/news/{{$news_ben->news_thumbnail}}" alt="">
                            </div>
                            @php
                                $news_date= strtotime($news_ben->news_date);
                                // $pieces = explode(" ", $news_ben->news_name);
                                // $news_name = implode(" ", array_splice($pieces, 0, 5));
                                $pieces1 = explode(" ", $news_ben->news_description);
                                $text_b = implode(" ", array_splice($pieces1, 0, 10));
                            @endphp
                            <h2 class="c_title">{{$news_ben->news_name}}</h2>
                            <div class="c_text " >
                              {!! $text_b !!} ... <span class="more">{{ trans('lang.detailis') }}</span>
                            </div>
                        </div>
                      
                        <span class="c_date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</span>
                    </a>
                    @endforeach
                 </div>
            </div>
            <div class="v-desktop">
                <div class="news-sidebar">
                  @foreach($news_page_sidebar as $news_page_s)
                    <a href="/news/{{$news_page_s->news_slug}}" class="news-block   @if($news_page_s->news_important == 1) official @endif @if($news_page_s->news_important == 2) urgent @endif">
                     
                        @php
                        $news_date= strtotime($news_page_s->news_date);
                        $pieces = explode(" ", $news_page_s->news_name);
                        $news_name = implode(" ", array_splice($pieces, 0, 8));
                        @endphp
                        <p class="news-date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</p>
                        <h4 class="news-title">{{$news_name}}</h4>
                    </a>
                    @endforeach
                    <div class="button_wrapper">
                        <a href="/news-archive" class="btn_archive v-desktop">
                            <img src="{{url('front/assets/folder.svg')}}" alt="">
                           
                            {{ trans('lang.news_archive') }}
                        </a>
                        <a href="/news-archive" class="all_links v-mob">
                            {{ trans('lang.all_news') }}
                            <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                        </a>
                    </div>
                </div>
          </div>
        </div>
    </div>

</section>
@endsection