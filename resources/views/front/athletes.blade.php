@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.athletes_u') }}</h1>
    </div>
</section>
<div id="athlete-category">
    <div class="container">
        <div class="athlete_bar">
          <div class="header_link sports_link">
            <a href="/sports">{{ trans('lang.sports') }}</a>
          </div>
          <div class="athlete_filters">
            <form class="" action="/athletes/filters" method="get" id="filter">
              <div class="filter_gender">
                <label class="@if($gender == 'all' || $gender == null) active @endif" for="gender_select">@if($locale == 'az') Hamısı @else All @endif
                  <input type="radio" @if($gender == 'all' || $gender == null) checked @endif name="gender" id="gender_select" value="all" class="filter_input">
                </label>
                <label class="@if($gender == 'male') active @endif" for="gender_select1">@if($locale == 'az') Kişi @else Male @endif
                  <input type="radio" @if($gender == 'male') checked @endif name="gender" id="gender_select1" value="male" class="filter_input">
                </label>
                <label class="@if($gender == 'female') active @endif" for="gender_select2">@if($locale == 'az') Qadın @else Female @endif
                  <input type="radio" @if($gender == 'female') checked @endif  name="gender" id="gender_select2" value="female" class="filter_input">
                </label>
              </div>
              <div class="filter_a_z">
                <button type="button" >{{ trans('lang.sorting') }}</button>
              </div>
              <div class="alphabet_main">
                <label class="@if($alphabet == 'all' || $alphabet == null) active @endif" for="alpha_select">@if($locale == 'az') Hamısı @else All @endif
                  <input type="radio" @if($alphabet == 'all' || $alphabet == null)  @endif name="alphabet" id="alpha_select" value="all" class="filter_input">
                </label>
                @for($i=0; $i<count($alpha); $i++)
                <label class="@if($alpha[$i] == $alphabet) active @endif" for="alpha{{$i}}">{{$alpha[$i]}}
                  <input type="radio" @if($alpha[$i] == $alphabet) checked @endif name="alphabet" id="alpha{{$i}}" value="{{$alpha[$i]}}" class="filter_input">
                </label>
                @endfor
              </div>
              <input type="hidden" name="sports" value="{{$sports}}">
            </form>
          </div>
        </div>
        <div class="athlete_list_wrapper">
          @foreach($athletes as $athlete)
            <a href="/athlete/@if($locale == 'az'){{$athlete->athlete_slug}}@else{{$athlete->athlete_slug_en}}@endif" class="athlete">
                <div class="photo"><img src="/uploads/{{$athlete->athlete_img}}" alt=""></div>
                <p class="name">
                  @if($locale == 'az'){{$athlete->athlete_name}}@else{{$athlete->athlete_name_en}}@endif
                </p>
            </a>
          @endforeach
        </div>
    </div>
    <div class="pagination"> 
      {{$athletes->links()}}
    </div>
  
</div>

@endsection
