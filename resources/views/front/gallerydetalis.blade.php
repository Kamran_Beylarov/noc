@extends('layouts.dizayn')

@section('content')

<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.gallery_u') }}</h1>
    </div>
</section>
<div id="gallery-inside">
    <div class="container">
      <div class="open-content">
        <div class="details ">

            <p class="category_title">{{ trans('lang.photo_gallery') }}</p>
            <div class="web-contact">
                {{-- <div class="social-media">
                <a href="https://facebook.com/sharer.php?u={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-facebook"></i></a>
                <a href="https://twitter.com/intent/tweet?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-twitter"></i></a>
                <a href="https://api.whatsapp.com/send?text={{url()->full()}}" target="_blank" class="sm-icon"><i class="fab fa-whatsapp"></i></a>
                </div> --}}
            </div>
        </div>
      </div>
        <h1 class="section_title">
          @if($locale == 'az'){{$photo_gallery->gallery_name}}@else{{$photo_gallery->gallery_name_en}}@endif
        </h1>
        <div class="gallery_display_img owl-carousel">
          @foreach($photo_gallery->gallery_images as $img)
            <div class="item">
                <img src="/uploads/{{$img->img_src}}" alt="">
            </div>
          @endforeach
        </div>
        <div class="gallery-menu">
          @foreach($photo_gallery->gallery_images as $key => $img)
            <div class="item" data-index="{{$key+1}}">
                 <img src="/uploads/{{$img->img_src}}" alt="">
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
