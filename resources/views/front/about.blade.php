@extends('layouts.dizayn')

@section('content')

<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.about_us') }}</h1>
    </div>
</section>   
     <section id="about">
    <div class="tab_links_section">
        <div class="container">
            <div class="tab_links">
                <a href="" class="tab_link active" data-target="about-structure">{{ trans('lang.structure') }}</a>
                <a href="" class="tab_link" data-target="about-history">{{ trans('lang.history') }}</a>
                <a href="" class="tab_link" data-target="about-federation">{{ trans('lang.federations') }}</a>
                <a href="" class="tab_link" data-target="about-docs">{{ trans('lang.documents') }}</a>
                <a href="" class="tab_link" data-target="about-sponsor">{{ trans('lang.sponsors') }}</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="tab_content_wrapper">
            <div class="tab_content" id="about-structure">
                <div class="wrapper">
                    <div class="structure_content_wrapper">
                        <section id="tab-assembly" class="structure_content">
                          @if(isset($bas_meclis))
                            <h1 class="section_title">@if($locale == 'az'){{$bas_meclis->page_name}}@else {{$bas_meclis->page_name_en}} @endif</h1>
                            <div class="description">
                                <div class="photo-box"><img src="/uploads/{{$bas_meclis->page_img}}" alt=""></div>
                                <div class="text">
                                  @if($locale == 'az'){!! $bas_meclis->page_description !!}@else {!! $bas_meclis->page_description_en !!} @endif
                                </div>
                            </div>
                            <div class="table_wrapper">
                                <table class="table-sm">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('lang.federations_organizations_and_members') }}</th>
                                            <th>{{ trans('lang.number') }}</th>
                                            <th>{{ trans('lang.places') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($bas_meclis->meclis_terkibi as $tekib)
                                        <tr>
                                            <td>@if($locale == 'az'){{$tekib->terkib}} @else {{$tekib->terkib_en}} @endif</td>
                                            <td>{{$tekib->say}}</td>
                                            <td>{{$tekib->yer}}</td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                                <p class="total">{{ trans('lang.total') }}: {{$bas_meclis->meclis_terkibi->sum('yer')}} {{ trans('lang.person') }}</p>
                            </div>
                            @endif
                            @foreach($terkib_hisse as $cate)
                            <div class="table_wrapper">

                                <h1 class="table_title">@if($locale == 'az') {{$cate->title}} @else {{$cate->title_en}} @endif</h1>
                                <div class="table_container">
                                    <table  class="table-lg" >
                                        <tbody>
                                          @foreach($cate->cate_members as $key => $members)
                                            <tr>
                                                <td>{{$key+1}}. @if($locale == 'az') {{$members->member_name}} @else {{$members->member_name_en}} @endif</td>
                                                <td>@if($locale == 'az') {{$members->member_post}} @else {{$members->member_post_en}} @endif</td>
                                                <td>
                                                  @if($members->members_federation != null)
                                                   @if($locale == 'az') {{$members->members_federation->federation_name}} @else {{$members->members_federation->federation_name_en}} @endif
                                                  @endif

                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <?php $federations_id = []; ?>
                                <?php for ($i=0; $i < count($cate->cate_members) ; $i++) { ?>
                                  <?php if ( $cate->cate_members[$i]->federation_id != null): ?>
                                    <?php $federations_id[] += $cate->cate_members[$i]->federation_id ?>
                                  <?php endif; ?>
                                <?php  } ?>
                                <?php $unique_id = array_unique($federations_id) ?>
                              <p class="total">{{ trans('lang.total') }} : {{count($cate->cate_members)}} {{ trans('lang.person') }} @if(count($unique_id) != 0) ({{  count($unique_id) }} <span class="lowercase">{{ trans('lang.federation') }}</span> ) @endif</p>
                            </div>
                            @endforeach
                        </section>

                         <section id="tab-committee" class="structure_content">
                           @if(isset($icraye_page))
                            <h1 class="section_title">
                              @if($locale == 'az') {{$icraye_page->title}} @else {{$icraye_page->title_en}} @endif
                            </h1>
                            <div class="text">
                                @if($locale == 'az') {!! $icraye_page->description !!} @else {!! $icraye_page->description_en !!} @endif
                            </div>
                            @endif
                            <div class="members_v">
                              @foreach($icra_komite1 as $icr)
                                <a  class="member-card tab_link"  data-target="tabs{{$icr->id}}" >
                                    <div class="imgHolder">
                                        <img src="/uploads/{{$icr->avatar}}" alt="">
                                    </div>
                                    <h5 class="name">@if($locale == 'az') {{$icr->name_surname}} @else {{$icr->name_surname_en}} @endif</h5>
                                    <p class="occupation">@if($locale == 'az') {{$icr->person_post}} @else {{$icr->person_post_en}} @endif</p>
                                </a>
                              @endforeach
                            </div>
                            <div class="members_h">
                              @foreach($icra_komite2 as $icr)
                              <a  class="member-card tab_link"  data-target="tabs{{$icr->id}}">
                                  <div class="imgHolder">
                                      <img src="/uploads/{{$icr->avatar}}" alt="">
                                  </div>
                                  <h5 class="name">@if($locale == 'az') {{$icr->name_surname}} @else {{$icr->name_surname_en}} @endif</h5>
                                  <p class="occupation">@if($locale == 'az') {{$icr->person_post}} @else {{$icr->person_post_en}} @endif</p>
                              </a>
                              @endforeach
                              </div>
                        </section>

                          <section id="tab-president" class="structure_content">
                            <div class="description">
                              @if(isset($president))
                                <h1 class="section_title uppercase">{{ trans('lang.president') }}</h1>
                                <div class="photo-box"><img src="/uploads/{{$president->person_img}}" alt=""></div>
                                <div class="overview">
                                    <h2 class="title">
                                      @if($locale == 'az') {{$president->name_surname}} {{$president->person_post}} @else {{$president->name_surname_en}} {{$president->person_post_en}} @endif
                                    </h2>
                                    <div class="text">
                                        @if($locale == 'az') {!! $president->person_info !!} @else {!! $president->person_info_en !!} @endif
                                    </div >
                                </div>
                                @endif
                            </div>
                        </section>
                        @foreach($all_persons as $all)
                          <section id="tabs{{$all->id}}" class="structure_content" >
                            <div class="description">
                                <h1 class="section_title">
                                  @if($locale == 'az') {{$all->person_post}}  @else {{$all->person_post_en}}  @endif
                                </h1>
                                <div class="photo-box"><img src="/uploads/{{$all->person_img}}" alt=""></div>
                                <div class="overview">
                                    <h2 class="title">
                                      @if($locale == 'az') {{$all->name_surname}}  @else {{$all->name_surname_en}}  @endif
                                    </h2>
                                    <div class="text">
                                        @if($locale == 'az') {!! $all->person_info !!} @else {!! $all->person_info_en !!} @endif
                                    </div >
                                </div>
                            </div>
                        </section>
                        @endforeach

                        @foreach($kommisias as $kommisia)
                         <section id="tab{{$kommisia->id}}" class="structure_content">
                            <h1 class="section_title">{{$kommisia->title}}</h1>
                            <div class="text">
                              {!! $kommisia->description !!}
                            </div>
                        </section>
                        @endforeach


                           <section id="tab-olympic" class="structure_content" >
                            <h1 class="section_title">{{ trans('lang.executive_office') }}</h1>
                            <div class="members_h">
                              @foreach($olimp_dunya as $val)
                                <a  class="member-card tab_link"  data-target="tabs{{$val->id}}">
                                    <div class="imgHolder">
                                        <img src="/uploads/{{$val->avatar}}" alt="">
                                    </div>
                                    <h5 class="name">
                                      {{$val->name_surname}}
                                    </h5>
                                    <p class="occupation">
                                      {{$val->person_post}}
                                    </p>
                                </a>
                              @endforeach
                              </div>
                        </section>
                 </div>
                    <ul class="sidebar_tabs">
                        <li><a href="" class="tab_link active" data-target="tab-assembly">{{ trans('lang.general_assembly') }}</a></li>
                        <li><a href="" class="tab_link" data-target="tab-committee">{{ trans('lang.executive_committee') }}</a></li>
                        <li><a href="" class="tab_link" data-target="tab-president" >{{ trans('lang.president_c') }}</a></li>
                        <li class="submenu">
                            <a href="" class="link_title">{{ trans('lang.commissions') }}</a>
                            <ul>
                              @foreach($kommisias as $kommisia)
                                <li><a href="" class="tab_link submenu_link" data-target="tab{{$kommisia->id}}">{{$kommisia->menu_name}}</a></li>
                              @endforeach
                            </ul>
                            </li>
                        <li><a href="" class="tab_link" data-target="tab-olympic">{{ trans('lang.executive_office') }}</a></li>
                    </ul>
                </div>
            </div>
            @if(isset($history))
            <style media="screen">
              #about-history img{
                max-width: 100% !important
              }
            </style>
              <div class="tab_content history_content" id="about-history">
                @if($locale == 'az')
                  {!! $history->history !!}
                @else
                  {!! $history->history_en !!}
                @endif
              </div>
               @endif

             <div class="tab_content" id="about-federation">
                <div class="federation_wrapper">
                  @foreach($federations as $federation)
                    <div class="federation-card">
                        <div class="c_title">
                            <div class="icon">
                                <img src="/uploads/{{$federation->federation_icon}}" alt="">
                            </div>
                            <h6 class="title">@if($locale == 'az'){{$federation->federation_name}} @else {{$federation->federation_name_en}} @endif</h6>
                        </div>
                        <div class="c_detail">
                            <ul>
                                <li>
                                    <p class="head">{{ trans('lang.president_c') }}</p>
                                    <div class="content">
                                        <p>
                                          @if($locale == 'az'){{$federation->drectory_name}} @else {{$federation->drectory_name_en}} @endif
                                        </p>
                                    </div>
                                </li>
                                <li class="no-wrap">
                                    <p class="head">{{ trans('lang.phone') }}</p>
                                    <div class="content">
                                        <p>{{$federation->telefon1}}</p>
                                        <p>{{$federation->telefon2}}</p>
                                    </div>
                                </li>
                                <li class="no-wrap">
                                    <p class="head">{{ trans('lang.fax') }}</p>
                                    <div class="content">
                                        <p>{{$federation->fax}}</p>
                                    </div>
                                </li>
                            </ul>
                            <ul class="tbl-email">
                                <li>
                                    <p class="head"></p>
                                    <div class="content">{{ trans('lang.web_and_email') }}
                                        <p><a href="{{$federation->web_site}}" target="_blank">{{$federation->web_site}}</a></p>
                                        <p><a href="mailto:{{$federation->email}}">{{$federation->email}}</a></p>
                                    </div>
                                </li>
                                <li>
                                    <p class="head">{{ trans('lang.address') }}</p>
                                    <div class="content">
                                        <p>  @if($locale == 'az'){{$federation->address}} @else {{$federation->address_en}} @endif</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                  @endforeach
            </div>
          </div>
            <div class="tab_content" id="about-docs">
              <ul class="tab_links docs_filter">
                @foreach($document_groups as $key=>$group)
                <a href="" class="tab_link @if($key == 0) active @endif" data-target="doc{{$group->id}}">@if($locale == 'az'){{$group->group_name}}@else{{$group->group_name_en}}@endif</a>
                @endforeach
            </ul>
                <div class="docs_wrapper">
               
                @foreach($document_groups as $groups)
                <div class="doc_content" id="doc{{$groups->id}}">
                  @foreach($groups->sort_doc as $document)
                  <a href="@if($document->document_file == null){{$document->url}}@else/uploads/{{$document->document_file}}@endif" class="doc-card" target="_blank">
                    <div class="icon">
                      @if($document->document_file != null)
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                            x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                            <g>
                                <g>
                                    <path d="M494.479,138.557L364.04,3.018C362.183,1.09,359.621,0,356.945,0h-194.41c-21.757,0-39.458,17.694-39.458,39.442v137.789
                      H44.29c-16.278,0-29.521,13.239-29.521,29.513v147.744C14.769,370.761,28.012,384,44.29,384h78.787v88.627
                      c0,21.71,17.701,39.373,39.458,39.373h295.238c21.757,0,39.458-17.653,39.458-39.351V145.385
                      C497.231,142.839,496.244,140.392,494.479,138.557z M359.385,26.581l107.079,111.265H359.385V26.581z M44.29,364.308
                      c-5.42,0-9.828-4.405-9.828-9.82V206.744c0-5.415,4.409-9.821,9.828-9.821h265.882c5.42,0,9.828,4.406,9.828,9.821v147.744
                      c0,5.415-4.409,9.82-9.828,9.82H44.29z M477.538,472.649c0,10.84-8.867,19.659-19.766,19.659H162.535
                      c-10.899,0-19.766-8.828-19.766-19.68V384h167.403c16.278,0,29.521-13.239,29.521-29.512V206.744
                      c0-16.274-13.243-29.513-29.521-29.513H142.769V39.442c0-10.891,8.867-19.75,19.766-19.75h177.157v128
                      c0,5.438,4.409,9.846,9.846,9.846h128V472.649z" />
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path
                                        d="M132.481,249.894c-3.269-4.25-7.327-7.01-12.173-8.279c-3.154-0.846-9.923-1.269-20.308-1.269H72.596v84.577h17.077
                      v-31.904h11.135c7.731,0,13.635-0.404,17.712-1.212c3-0.654,5.952-1.99,8.856-4.01c2.904-2.019,5.298-4.798,7.183-8.336
                      c1.885-3.538,2.827-7.904,2.827-13.096C137.385,259.634,135.75,254.144,132.481,249.894z M117.856,273.173
                      c-1.288,1.885-3.067,3.269-5.337,4.154s-6.769,1.327-13.5,1.327h-9.346v-24h8.25c6.154,0,10.25,0.192,12.288,0.577
                      c2.769,0.5,5.058,1.75,6.865,3.75c1.808,2,2.712,4.539,2.712,7.615C119.789,269.096,119.144,271.288,117.856,273.173z" />
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path d="M219.481,263.452c-1.846-5.404-4.539-9.971-8.077-13.702s-7.789-6.327-12.75-7.789c-3.692-1.077-9.058-1.615-16.096-1.615
                      h-31.212v84.577h32.135c6.308,0,11.346-0.596,15.115-1.789c5.039-1.615,9.039-3.865,12-6.75c3.923-3.808,6.942-8.788,9.058-14.942
                      c1.731-5.039,2.596-11.039,2.596-18C222.25,275.519,221.327,268.856,219.481,263.452z M202.865,298.183
                      c-1.154,3.789-2.644,6.51-4.471,8.163c-1.827,1.654-4.125,2.827-6.894,3.519c-2.115,0.539-5.558,0.808-10.327,0.808h-12.75v0
                      v-56.019h7.673c6.961,0,11.635,0.269,14.019,0.808c3.192,0.692,5.827,2.019,7.904,3.981c2.077,1.962,3.692,4.692,4.846,8.192
                      c1.154,3.5,1.731,8.519,1.731,15.058C204.596,289.231,204.019,294.394,202.865,298.183z" />
                                </g>
                            </g>
                            <g>
                                <g>
                                    <polygon points="294.827,254.654 294.827,240.346 236.846,240.346 236.846,324.923 253.923,324.923 253.923,288.981
                      289.231,288.981 289.231,274.673 253.923,274.673 253.923,254.654 		" />
                                </g>
                            </g>
                        </svg>
                        @else
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                  viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <style type="text/css">
                                  .st0{fill:#13007F;}
                                </style>
                                <g transform="translate(1 1)">
                                  <g>
                                    <g>
                                      <path class="st0" d="M211.26,389.24l-60.33,60.33c-25.01,25.01-65.52,25.01-90.51,0.01c-25-25-25-65.51-0.01-90.5L181.1,238.4
                                        c24.99-24.99,65.5-24.99,90.49,0c8.33,8.33,21.84,8.33,30.17,0c8.33-8.33,8.33-21.84,0-30.17c-41.65-41.65-109.18-41.65-150.83,0
                                        L30.25,328.91c-41.65,41.65-41.65,109.18,0,150.83c41.65,41.68,109.18,41.68,150.85,0l60.33-60.33c8.33-8.33,8.33-21.84,0-30.17
                                        S219.59,380.91,211.26,389.24z"/>
                                      <path class="st0" d="M479.75,30.24c-41.65-41.65-109.2-41.65-150.85,0l-72.38,72.38c-8.33,8.33-8.33,21.84,0,30.17
                                        c8.33,8.33,21.84,8.33,30.17,0l72.38-72.38c24.99-24.99,65.52-24.99,90.51,0c24.99,24.99,24.99,65.5,0,90.49L316.85,283.64
                                        c-24.99,24.99-65.5,24.99-90.49,0c-8.33-8.33-21.84-8.33-30.17,0s-8.33,21.84,0,30.17c41.65,41.65,109.18,41.65,150.83,0
                                        l132.74-132.74C521.41,139.42,521.41,71.89,479.75,30.24z"/>
                                    </g>
                                  </g>
                                </g>
                                </svg>
                        @endif
                    </div>
                    <p>
                      {{ $document->document_name }}
                    </p>
                </a>
                @endforeach
                </div>
              @endforeach      
                 </div>
            </div>

              <div class="tab_content" id="about-sponsor">
                @foreach($sponsors as $sponsor)
                <div class="sponsor-card">
                    <div class="logo_wrapper">
                        <div class="logo"><img src="/uploads/{{$sponsor->logo}}" alt=""></div>
                        <p class="name">
                          @if($locale == 'az')   {{ $sponsor->sponsor_name }} @else    {{ $sponsor->sponsor_name_en }} @endif
                        </p>
                        <div class="sm_links">
                            <a href="{{$sponsor->fb}}" class="sm" target="_blank"><span class="icon-social-icons-black-03"></span></a>
                            <a href="{{$sponsor->ins}}" class="sm" target="_blank"><span class="icon-social-icons-black-01"></span></a>
                            <a href="{{$sponsor->web_site}}" class="sm" target="_blank"><img src="{{url('front/assets/global.svg')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="text_wrapper">
                        <div class="text">
                          @if($locale == 'az')   {!! $sponsor->sponsor_detalis !!} @else   {!! $sponsor->sponsor_detalis_en !!} @endif
                       </div>
                    </div>
                </div>
                @endforeach
            </div>
         </div>
    </div>
</section>
@endsection
