@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.news') }}</h1>
    </div>
</section>
<section id="news-page">
    <div class="container">
        <div class="news-wrapper">
            <div class="news-content">
                <div class="news-intro">
                    @if(isset($news_page_title))
                    <a href="/news/{{$news_page_title->news_slug}}" class="news-card">
                        <div class="c_img">
                            <img src="/news/{{$news_page_title->news_thumbnail}}" alt="">
                        </div>
                        @php
                            // $pieces = explode(" ", $news_page_title->news_name);
                            // $news_name = implode(" ", array_splice($pieces, 0, 5));
                            $pieces1 = explode(" ", $news_page_title->news_description);
                            $text = implode(" ", array_splice($pieces1, 0, 15));
                            $news_date= strtotime($news_page_title->news_date);
                        @endphp
                        <h2 class="c_title">{{$news_page_title->news_name}}</h2>

                          <div class="c_text">
                              {!! $text !!}...  <span class="more">{{ trans('lang.detailis') }}</span>
                          </p>
                          </div>
                        <span class="c_date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</span>
                    </a>
                      @endif
                  </div>

                <div class="news-container">
                    @foreach($news_container as $news_con)
                      <a href="/news/{{$news_con->news_slug}}" class="news-card">
                          <div class="card_box">
                              <div class="c_img">
                                  <img src="/news/{{$news_con->news_thumbnail}}" alt="">
                              </div>
                              @php
                                $news_date= strtotime($news_con->news_date);
                                // $pieces = explode(" ", $news_con->news_name);
                                // $news_name = implode(" ", array_splice($pieces, 0, 5));
                                $pieces1 = explode(" ", $news_con->news_description);
                                $text = implode(" ", array_splice($pieces1, 0, 10));
                              @endphp
                              <h2 class="c_title">{{$news_con->news_name}}</h2>
                              <div class="c_text">
                                  {!! $text !!}... <span class="more">{{ trans('lang.detailis') }}</span>
                                </p>
                            </p>
                              </div>
                          </div>
                        
                          <span class="c_date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</span>
                      </a>
                      @endforeach
                </div>
                
                <div class="v-mob">
                    <div class="news-sidebar">

                        @foreach($news_page_sidebar as $news_page_s)
                          <a href="/news/{{$news_page_s->news_slug}}" class="news-block   @if($news_page_s->news_important == 1) official @endif @if($news_page_s->news_important == 2) urgent @endif">
                            <?php
                              $news_date= strtotime($news_page_s->news_date);
                             ?>
                              <p class="news-date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</p>
                            @php
                            //   $pieces = explode(" ", $news_page_s->news_name);
                            //   $news_name = implode(" ", array_splice($pieces, 0, 5));
                            @endphp

                              <h4 class="news-title">{{$news_page_s->news_name}}</h4>
                          </a>
                          @endforeach


                        <div class="button_wrapper">
                            <a href="/news-archive" class="btn_archive v-desktop">
                                <img src="{{url('front/assets/folder.svg')}}" alt="">
                              
                                {{ trans('lang.news_archive') }}
                            </a>
                            <a href="/news-archive" class="all_links v-mob">
                               
                                {{ trans('lang.all_news') }}
                                <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                            </a>
                        </div>
                    </div>
                 </div>
            </div>
            <div class="v-desktop">
                <div class="news-sidebar">
                  @foreach($news_page_sidebar as $news_page_s)
                    <a href="/news/{{$news_page_s->news_slug}}" class="news-block   @if($news_page_s->news_important == 1) official @endif @if($news_page_s->news_important == 2) urgent @endif">
                    
                    @php
                        $news_date= strtotime($news_page_s->news_date);
                        // $pieces = explode(" ", $news_page_s->news_name);
                        // $news_name = implode(" ", array_splice($pieces, 0, 8));
                    @endphp
                        <p class="news-date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</p>
                       
                        <h4 class="news-title">{{$news_page_s->news_name}}</h4>
                    </a>
                    @endforeach
                    <div class="button_wrapper">
                        <a href="/news-archive" class="btn_archive v-desktop">
                            <img src="{{url('front/assets/folder.svg')}}" alt="">
                            {{ trans('lang.news_archive') }}
                        </a>
                        <a href="/news-archive" class="all_links v-mob">
                            {{ trans('lang.all_news') }}
                            <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
                        </a>
                    </div>
                </div>                    </div>
        </div>
    </div>

</section>
</main>

@endsection
