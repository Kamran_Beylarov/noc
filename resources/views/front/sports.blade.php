@extends('layouts.dizayn')

@section('content')

  <section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
           <div class="container">
               <h1 class="title">{{ trans('lang.sports') }}</h1>
           </div>
       </section>
       <div class="container">
           <div id="athletes">
             @foreach($sports as $sport)
             <a class="box" href="/athletes/filters?sports=@if($locale == 'az'){{$sport->sport_slug}}@else{{$sport->sport_slug_en}}@endif">
               <div class="photo"><img src="/uploads/{{$sport->sport_icon}}" alt=""></div>
               <p class="name text-center">@if($locale == 'az'){{$sport->sport_name}}@else{{$sport->sport_name_en}}@endif</p>
             </a>
             @endforeach
           </div>
       </div>
@endsection
