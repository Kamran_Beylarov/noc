@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.news') }}</h1>
    </div>
</section>
<section id="news-archive">
    <div class="container">

        <form class="search-form" action="/news-search" method="get" >
            <input class="search-input" type="text" placeholder="{{ trans('lang.enter_the_keyword') }}" name="search_news" value="@if(request()->path() == 'news-search') {{$search_data}} @endif">
            <button type="submit" class="search-button"><span>{{ trans('lang.search') }}</span>  <i class="fa fa-search"></i></button>
        </form>

        <div class="archive-news">
          @foreach($news_archive as $news_page_s)
            <a href="/news/{{$news_page_s->news_slug}}" class="news-block   @if($news_page_s->news_important == 1) official @endif @if($news_page_s->news_important == 2) urgent @endif">
                    @php
                         $news_date= strtotime($news_page_s->news_date);
                        //  $pieces = explode(" ", $news_page_s->news_name);
                        //  $news_name = implode(" ", array_splice($pieces, 0, 8));
                    @endphp
                <p class="news-date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</p>
                <h4 class="news-title">{{$news_page_s->news_name}}</h4>
            </a>
            @endforeach

        </div>
    </div>
</section>
@endsection
