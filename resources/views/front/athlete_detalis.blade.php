@extends('layouts.dizayn')

@section('content')
<div id="athlete-single">
    <section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
        <div class="container">
            <h1 class="title">{{ trans('lang.athletes_u') }}</h1>
        </div>
    </section>
    <div class="container">
        <div class="content_wrapper">
            <h1 class="section_title">@if($locale == 'az'){{$athlete->athlete_name}}@else{{$athlete->athlete_name_en}}@endif</h1>
            <div class="athlete-content">
                <div class="athlete-info">
                    <div class="photo"><img src="/uploads/{{$athlete->athlete_img}}" alt=""></div>
                    <div class="info-box">
                        <div class="i_head">
                            <div class="content">
                                <h4 class="i_title">{{ trans('lang.sport') }}:</h4>
                                <p class="i_cnt">@if($locale == 'az'){{$athlete->athlete_sport->sport_name}}@else{{$athlete->athlete_sport->sport_name_en}}@endif</p>
                            </div>
                            <div class="icon"><img src="/uploads/{{$athlete->athlete_sport->sport_icon}}" alt=""></div>
                        </div>
                        <div class="i_content">
                            <div class="title">
                                <h4 class="i_title">{{ trans('lang.was_born') }}:</h4>
                                @if($athlete->athlete_height != null) <h4 class="i_title">{{ trans('lang.height') }}:</h4> @endif
                                @if($athlete->athlete_weight != null) <h4 class="i_title">{{ trans('lang.weight') }}:</h4> @endif
                                @if($athlete->personal_trainer != null) <h4 class="i_title">{{ trans('lang.personal_trainer') }}:</h4> @endif
                            </div>
                            <div class="cnt">
                              <?php
                                $activ_date= strtotime($athlete->athlete_birthday);
                               ?>

                                <p class="i_cnt">{{date('d',$activ_date)}} {{$months[date('m',$activ_date)]}} {{date('Y',$activ_date)}}</p>
                                @if($athlete->athlete_height != null)  <p class="i_cnt"> {{$athlete->athlete_height}} sm</p> @endif
                                @if($athlete->athlete_weight != null) <p class="i_cnt">{{$athlete->athlete_weight}} {{ trans('lang.kg') }} </p> @endif
                                @if($athlete->personal_trainer != null) <p class="i_cnt">{{$athlete->personal_trainer}}  </p> @endif
                            </div>

                        </div>
                    </div>
                </div>
                <div class="athlete-table-content">
                    <h3 class="table_header">{{ trans('lang.rewards') }}</h3>
                    @foreach($athlete->athlete_stat_relation as $tournaments)
                    <div class="table_container">
                        <h5 class="tbl_title">

                          @if($tournaments->game_tournament_type == 2)
                            @if($locale == 'az'){{$tournaments->relation_tour->tournament_name}}@else {{$tournaments->relation_tour->tournament_name_en}} @endif
                          @endif

                          @if($tournaments->game_tournament_type == 1)
                              @if($locale == 'az'){{$tournaments->relation_game->game_name}}@else{{$tournaments->relation_game->game_name_en}}@endif
                          @endif
                        </h5>
                        <div class="table_wrapper">
                            <table>
                                <tbody>
                                  @foreach($tournaments->relation_statistics as $tournament)
                                    <tr>
                                        <td>{{$tournament->year}} @if($locale == 'az'){{$tournament->city}}@else {{$tournament->city_en}} @endif </td>
                                        <td> @if($locale == 'az'){{$tournament->sport_type}}@else {{$tournament->sport_type_en}}  @endif @if($tournament->is_team == 0) @if($tournament->weight != null) — {{$tournament->weight}} {{ trans('lang.kg') }} @endif @else {{ trans('lang.team') }} @endif</td>
                                        <td>

                                          @if($tournament->medal == 1)
                                            Qızıl
                                          @elseif($tournament->medal == 2)
                                           Gümüş
                                          @elseif($tournament->medal == 3)
                                           Bürünc
                                           @else
                                            İştirakçı
                                          @endif

                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
