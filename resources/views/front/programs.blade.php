@extends('layouts.dizayn')
@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">{{ trans('lang.programs') }}</h1>
    </div>
</section>
<section id="programs">
    <div class="container">
        <div class="programs-content all_programs">
            @foreach($programs as $program)
            <a href="/programs/{{$program->news_slug}}" class="program-card">
                <div class="card-img">
                    <img src="/news/{{$program->news_thumbnail}}" alt="">
                </div>
                <h4 class="card-title">
                    {{$program->news_name}}
                </h4>
                <div class="card-info">
                    <?php
                        $pieces1 = explode(" ", $program->news_description);
                        $text = implode(" ", array_splice($pieces1, 0, 10));
                     ?>
                      {!! $text !!}
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>
@endsection
