@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
  <div class="container">
    <h1 class="title">
      @if($locale == 'az'){{$game_detalis->game_name}}@else{{$game_detalis->game_name_en}}@endif
    </h1>
  </div>
</section>
<div id="game-single">
  <div class="container">
    <div class="game-intro">
      <div class="g-col">
        <div class="photo"><img src="/uploads/{{$game_detalis->game_logo}}" alt=""></div>
        <div class="content">
          <h1 class="title">
            @if($locale == 'az'){{$game_detalis->game_city}} {{$game_detalis->game_country}}@else{{$game_detalis->game_city_en}} {{$game_detalis->game_country_en}}@endif
            </h1>
          <?php
            $start_date= strtotime($game_detalis->start_date);
            $end_date= strtotime($game_detalis->end_date);
           ?>
          <p class="date lowercase">{{date('d',$start_date)}} {{$months[date('m',$start_date)]}} - {{date('d',$end_date)}} {{$months[date('m',$end_date)]}}</p>
        </div>
      </div>
      <div class="g-col">
        <div class="content">
          <div class="text">
            @if($locale == 'az'){!!$game_detalis->short_description!!}@else{!!$game_detalis->short_description_en!!}@endif
          </div>
        </div>
        <div class="photo athlete_detalis_rewards">
       
          <div class="medal">
            <img src="{{url('front/assets/gold.svg')}}" alt="">
            <span>{{$game_detalis->gold}}</span>
          </div>
          <div class="medal">
            <img src="{{url('front/assets/silver.svg')}}" alt="">
            <span>{{$game_detalis->silver}}</span>
          </div>
          <div class="medal">
            <img src="{{url('front/assets/bronze.svg')}}" alt="">
            <span>{{$game_detalis->bronz}}</span>
          </div>

        </div>
      </div>
    </div>
    <div class="g_desc">
      <div >
        @if($locale == 'az'){!!$game_detalis->description!!}@else{!!$game_detalis->description_en!!}@endif
      </div>
    </div>

    <div class="game-news">
      @php
      $elements = [];
      $elements_medal = [];
        foreach($game_athlete_statistics as $statis){
          if($statis->relation_statistics[0]->medal != 4){
            $elements_medal[] =$statis->id;
          }
          if($statis->relation_statistics[0]->medal == 4){
              $elements[] =$statis->id;
           }
        }
        $count = count($elements);
        $count_m = count($elements_medal);
    @endphp
    @if($count_m > 0)
      <div class="g-col">
      <div class="header_link">
          <h1 class="title double">{{ trans('lang.medalists') }} </h1>
          <!-- <a href="" class="all_links">
            Bütün xəbərlər
            <div class="arrow"><img src="../assets/angle-blue.svg" alt=""></div>
          </a> -->
        </div>
        <div class="table_wrapper">
          <table>
            <tbody>
              @foreach($game_athlete_statistics as $statis)
              @if($statis->relation_statistics[0]->medal != 4) 
              <tr>
                <td>
                  <div class="winner">
                    <div class="reward @if($statis->relation_statistics[0]->medal == 1) gold @elseif($statis->relation_statistics[0]->medal == 2) silver @elseif($statis->relation_statistics[0]->medal == 3) bronze @else @endif">
                        <img src="{{url('front/assets/olymp-logo.png')}}" alt="">
                    </div>
                    <p class="name">
                      
                      <a href="/athlete/@if($locale == 'az'){{$statis->athlete_game_tour->athlete_slug}}@else{{$statis->athlete_game_tour->athlete_slug_en}}@endif">@if($locale == 'az') {{$statis->athlete_game_tour->athlete_name}} @else {{$statis->athlete_game_tour->athlete_name_en}} @endif</a>
                    </p>
                  </div>
                </td>
                <td class="sport">
                    @if($locale == 'az') {{$statis->athlete_game_tour->athlete_sport->sport_name}} @else {{$statis->athlete_game_tour->athlete_sport->sport_name_en}} @endif
                </td>
                <td class="feature">
                  @if($statis->athlete_game_tour->athlete_gender == 'male')
                    @if($locale == 'az') Kişi @else Man @endif
                  @else
                   @if($locale == 'az') Qadın @else Woman @endif
                  @endif
                   @if($locale == 'az'){{$statis->relation_statistics[0]->sport_type}} @else {{$statis->relation_statistics[0]->sport_type_en}} @endif
                  @if($statis->relation_statistics[0]->is_team == 0) @if($statis->relation_statistics[0]->weight != null){{$statis->relation_statistics[0]->weight}}{{ trans('lang.kg') }}. @endif  @else  {{ trans('lang.team') }}  @endif
                </td>
                <td class="date">{{$statis->relation_statistics[0]->year}}</td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div> 
      @endif

       @if( $count > 0) 
       <div class="g-col">
      <div class="header_link">
          <h1 class="title double">{{ trans('lang.participants') }}</h1>
          <!-- <a href="" class="all_links">
            Bütün xəbərlər
            <div class="arrow"><img src="../assets/angle-blue.svg" alt=""></div>
          </a> -->
        </div>
        <div class="table_wrapper">
          <table>
            <tbody>
              @foreach($game_athlete_statistics as $statis)
              @if($statis->relation_statistics[0]->medal == 4) 
              <tr>
                <td>
                  <div class="winner">
                    <div class="reward ">
                        <img src="{{url('front/assets/olymp-logo.png')}}" alt="">
                    </div>
                    <p class="name">
                      <a href="/athlete/@if($locale == 'az'){{$statis->athlete_game_tour->athlete_slug}}@else{{$statis->athlete_game_tour->athlete_slug_en}}@endif">@if($locale == 'az') {{$statis->athlete_game_tour->athlete_name}} @else {{$statis->athlete_game_tour->athlete_name_en}} @endif</a>
                    </p>
                  </div>
                </td>
                <td class="sport">
                    @if($locale == 'az') {{$statis->athlete_game_tour->athlete_sport->sport_name}} @else {{$statis->athlete_game_tour->athlete_sport->sport_name_en}} @endif
                </td>
                <td class="feature">
                  @if($statis->athlete_game_tour->athlete_gender == 'male')
                    @if($locale == 'az') Kişi @else Man @endif
                  @else
                   @if($locale == 'az') Qadın @else Woman @endif
                  @endif
                   @if($locale == 'az'){{$statis->relation_statistics[0]->sport_type}} @else {{$statis->relation_statistics[0]->sport_type_en}} @endif
                  @if($statis->relation_statistics[0]->weight != null){{$statis->relation_statistics[0]->weight}}{{ trans('lang.kg') }}. @endif
                </td>
                <td class="date">{{$statis->relation_statistics[0]->year}}</td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      @endif
    </div>
  @if(count($game_news)>0)
  <section id="latest-news">
    <div class="header_link">
      <h1 class="title double uppercase">{{ trans('lang.news') }}</h1>
      <a href="/articles" class="all_links">
        {{ trans('lang.all_news') }}
        <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
      </a>
    </div>
    <div class="wrapper">
      @foreach($game_news as $new)
      <a href="/news/{{$new->news_slug}}" class="news-card">
          <div class="card_box">
              <div class="c_img">
                  <img src="/news/{{$new->news_thumbnail}}" alt="">
              </div>
              <h2 class="c_title">{{$new->news_name}}</h2>
              <div class="c_text">
                <?php 
                      $pieces1 = explode(" ", $new->news_description);
                            $text = implode(" ", array_splice($pieces1, 0, 10));
                  ?>
                  {!! $text !!}<span class="more">{{ trans('lang.detailis') }}</span>
              </div>
          </div>
          <?php
            $news_date= strtotime($new->news_date);
           ?>
  
          <span class="c_date">{{date('d',$news_date)}} {{$months[date('m',$news_date)]}} {{date('Y',$news_date)}}</span>
      </a>
      @endforeach
  </div>
    <div class="v-mob">
      <a href="/news" class="all_links">
        {{ trans('lang.all_news') }}
        <div class="arrow"><img src="{{url('front/assets/angle-blue.svg')}}" alt=""></div>
      </a>
    </div>
  </section>
  @endif
    <div class="athlete-list">
        <div class="header_link">
            <h1 class="title double">{{ trans('lang.number_athletes') }} </h1>
            <!-- <a href="" class="all_links">
                Bütün xəbərlər
                <div class="arrow"><img src="../assets/angle-blue.svg" alt=""></div>
            </a> -->
        </div>

        <div class="list_wrapper">
          @foreach($game_statistics as $statistic)
            <div class="athlete-card">
                <div class="a_header">
                    <div class="icon"><img src="/uploads/{{$statistic->statistic_sport->sport_icon}}" alt=""></div>
                    <h5 class="title">

                      @if($locale == 'az'){{$statistic->statistic_sport->sport_name}} @else {{$statistic->statistic_sport->sport_name_en}} @endif
                    </h5>
                </div>
               <div class="result_list">
                   <li>
                       <span class="head">{{ trans('lang.males') }}</span>
                       <p class="content">{{$statistic->male}}</p>
                   </li>
                   <li>
                       <span class="head">{{ trans('lang.females') }}</span>
                       <p class="content">{{$statistic->female}}</p>
                   </li>
                   <li>
                       <span class="head">{{ trans('lang.sum') }}</span>
                       <p class="content">{{$statistic->sum}}</p>
                   </li>
               </div>

            </div>
            @endforeach
             </div>
    </div>
      </div>
</div>
@endsection



