@extends('layouts.dizayn')

@section('content')
<!-- butun video qallereyalar bu sehifede olacaq pagination ile -->
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title">Qalereya</h1>
    </div>
</section>
<div id="gallery">
    <div class="container">

        <div class="header_link">
            <h1 class="title double">Video Qalereya</h1>
        </div>

        <div class="gallery_vd_wrapper">
          @foreach($video_galleries as $video_gallery)
            <div class="video-box">
                <img src="/uploads/{{$video_gallery->video_thumbnail}}" alt="">
                <div class="video-overlay">
                    <div class="date-icon">
                        <p class="date">
                          <?php
                             setlocale(LC_TIME, $locale);
                            $gallery_date= strtotime($video_gallery->video_date);
                           ?>
                        </p>
                        <i class="fa fa-video-camera"></i>
                    </div>
                    <h4 class="title">
                      <a href="/videos/@if($locale == 'az'){{$video_gallery->video_name_slug}}@else{{$video_gallery->video_name_slug_en}}@endif" class="title">

                        @if($locale == 'az'){{$video_gallery->video_name}}@else{{$video_gallery->video_name_en}}@endif
                      </a>
                    </h4>
                </div>
            </div>
          @endforeach
        </div>

        <!-- <div class="pagination">
            <a href=""><img src="../assets/angle-navy.svg" alt=""></a>
            <a href="">1</a>
            <a href="">2</a>
            <a href="" class="active">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a href=""><img src="../assets/angle-navy.svg" alt=""></a>
        </div>    -->
     </div>
</div>

@endsection
