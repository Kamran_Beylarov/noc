@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png')}});">
    <div class="container">
        <h1 class="title uppercase">@if($locale == 'az'){{$partner->partner_name}}@else{{$partner->partner_name_en}} @endif</h1>
    </div>
</section>
<div id="partner-single">
    <div class="container">
        {{-- <h1 class="section_title partners_title">@if($locale == 'az'){{$partner->partner_name}}@else{{$partner->partner_name_en}} @endif</h1> --}}
        <div class="p_content_wrapper">
            <div class="content_about">
                <div class="photo">
                    <img src="/uploads/{{$partner->logo}}" alt="">
                </div>
                <div class="text_box">
                  @if($locale == 'az'){!! $partner->short_description !!}@else{!! $partner->short_description_en !!}@endif
                    <p class="partner_link">
    
                      <a href="{{$partner->web_site}}" class="sm" target="_blank"><img width="20" src="{{url('front/assets/global.svg')}}" alt=""> {{ trans('lang.web_link') }}</a>
                    </p>
                </div>
            </div>
            <!-- <p class="text text_b">Avropa Olimpiya Komitəsinin əsas məqsədi aşağıdakılardır:</p>
            <ul class="p_list">
                <li class="text">Olimpiya ideallarının Avropada yayılması və təbliğ olunması;</li>
                <li class="text">Olimpiya Hərəkatının inkişafına dəstək və yardımın göstərilməsi;</li>
                <li class="text">İdmanın köməyi ilə uşaqların və gənclərin dostluq, qardaşlıq, sülh və
                    əminamanlığın bərqərar olacağı dünya
                    qurulması, ətraf mühitin mühafizəsi ruhunda tərbiyə edilməsi;</li>
                <li class="text">Ümumi marağa xidmət edən elmi tədqiqatlar, təhsil, və qabaqcıl təcrübənin
                    mübadiləsi sahəsində Avropa
                    Milli Olimpiya Komitələri arasında sıx əməkdaşlığın yaradılması.</li>
                <li class="text">Avropa Olimpiya Komitəsi idman və Olimpiya Hərəkatı sahəsində müxtəlif problem
                    məsələlərin, o cümlədən
                    qadın və idman, dopinqə qarşı mübarizə, idmanın hüquqi aspektləri və sair kimi məsələlərin
                    həlli
                    məsələlərində geniş planda işlər aparılması.</li>
            </ul>
            <p class="text">Azərbaycanla Avropa Milli Olimpiya Komitəsi arasında əsas əməkdaşlıq əlaqələri
                1997-ci idən mövcuddur. 1998-ci il noyabrın 13-14-də Rusiyanın Sankt-Peterburq şəhərində Avropa
                Olimpiya Komitəsinin XXVII Baş Məclisi keçirilib. Məclisdə 48 ölkədən Milli Olimpiya
                Komitələrinin nümayəndələri və Beynəlxalq Olimpiya Komitəsinin 25 üzvü iştirak edib. Azərbaycanı
                bu məclisdə MOK-un vitse-prezidenti Çingiz Hüseynzadə və baş katib Ağacan Abiyev təmsil ediblər.
                MOK-un vitse-prezidenti Çingiz Hüseynzadə və baş katib Ağacan Abiyev müvafiq olaraq AOK-un
                "Olimpiya Oyunlarına hazırlıq”, "Texniki əməkdaşlıq” komissiyalarının üzvləridir.</p> -->
              <div class="">
                  @if($locale == 'az'){!! $partner->description !!}@else{!! $partner->description_en !!}@endif
              </div>
        </div>
    </div>
</div>
@endsection
