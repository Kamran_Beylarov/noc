@extends('layouts.dizayn')

@section('content')
<section id="section-banner" style="background-image: url({{url('front/assets/bg.png.jpg')}});">
    <div class="container">
        <h1 class="title">{{ trans('lang.events') }}</h1>
    </div>
</section>
<section id="programs">
    <section id="latest-news">
        <div class="container">
            <div class="wrapper">
              @foreach($activitys as $activity)
                <a href="/activity/{{$activity->news_slug}}" class="news-card">
                    <div class="card_box">
                        <div class="c_img">
                            <img src="/news/{{$activity->news_thumbnail}}" alt="">
                        </div>
                        <h2 class="c_title">{{$activity->news_name}}</h2>
                        <div class="c_text">
                          <?php
                              $pieces1 = explode(" ", $activity->news_description);
                             $text = implode(" ", array_splice($pieces1, 0, 10)); 
                             ?>
                            {!! $text !!}
                        </div>
                    </div>
                      <?php
                        $activ_date= strtotime($activity->news_date);
                       ?>
                    <span class="c_date">{{date('d',$activ_date)}} {{$months[date('m',$activ_date)]}} {{date('Y',$activ_date)}}</span>
                </a>
              @endforeach
          </div>
        </div>
    </section>
</section>
@endsection
