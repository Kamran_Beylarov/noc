@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('games.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Oyun adı </th>
                          <th>Ana səhifədə</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($games as $game)
                      <tr>

                          <td>{{ $game->game_name }}</td>
                          <td>
                            @if($game->index_status == 1)
                               <button type="button" class="btn btn-success index_status id{{$game->id}}" value="{{$game->id}}" >Görünsün</button>
                               @else
                               <button type="button" class="btn btn-danger index_status id{{$game->id}}" value="{{$game->id}}" >Görünməsin</button>
                               @endif
                          </td>
                          <td>
                              <form action="{{ route('games.destroy', $game->id) }}" method="post" style="display: initial;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('games.edit', $game->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>





            </div>
        </div>
    </div>
</div>
<script>
$('.index_status').on('click',function(e){
  var id = $(this).val();

  $.ajax({
    type:"GET",
    cache:false,
    url:"/admin/game_status",
    data:{id:id},
    success: function (html) {
      console.log(html);
      if (html.index_status == 1) {
        $('.id'+html.id).empty().html('Görünsün').removeClass('btn-danger').addClass('btn-success');
      }else{
        $('.id'+html.id).empty().html('Görünməsin').removeClass('btn-success').addClass('btn-danger');
      }
    }
  });
});
</script>
@endsection
