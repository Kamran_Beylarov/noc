@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('games.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                  <p><b>Oyun kateqoriyasl  </b><span style="color:red;">*</span></p>
                  <select class="form-control" name="game_cate_id" required>
                    <option value="">Seçin</option>
                    @foreach($cates as $cate)
                    <option value="{{$cate->id}}">{{$cate->cate_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <p><b>Oyun adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="game_name"  class="form-control"  required>
                </div>
                <div class="col-md-3">
                  <p><b>Oyun adı  (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="game_name_en"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Logo (Fayl ölçüsü max 1024kb olmalıdır (min:208x230))</b><span style="color:red;">* </span></p>
                  <input type="file" name="game_logo"  class="form-control"  id="logo" required>
                </div>
                <div class="col-md-3">
                  <p><b>Keçirildiyi ölkə</b><span style="color:red;">* </span></p>
                  <input type="text" name="game_country"  class="form-control" required >
                </div>
                <div class="col-md-3">
                  <p><b>Keçirildiyi ölkə (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="game_country_en"  class="form-control"  >
                </div>
                <div class="col-md-3">
                  <p><b>Şəhər</b><span style="color:red;">* </span></p>
                  <input type="text" name="game_city"  class="form-control"  required>
                </div>
                <div class="col-md-3">
                  <p><b>Şəhər (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="game_city_en"  class="form-control"  >
                </div>
                <div class="col-md-3">
                  <p><b>Başlama vaxtı </b><span style="color:red;">*</span></p>
                  <input type="text" name="start_date"  class="form-control" required id="date-fr">
                </div>
                <div class="col-md-3">
                  <p><b>Bitmə vaxtı </b><span style="color:red;">*</span></p>
                  <input type="text" name="end_date"  class="form-control" required id="date-frend">
                </div>
                <div class="col-md-2">
                  <p><b>Qızıl (say) </b><span style="color:red;">*</span></p>
                  <input type="number" name="gold"  class="form-control" required >
                </div>
                <div class="col-md-2">
                  <p><b>Gümüş (say) </b><span style="color:red;">*</span></p>
                  <input type="number" name="silver"  class="form-control" required >
                </div>
                <div class="col-md-2">
                  <p><b>Bürünc (say) </b><span style="color:red;">*</span></p>
                  <input type="number" name="bronz"  class="form-control" required >
                </div>

                <div class="col-md-6">
                  <p><b>Qısa məlumat </b><span style="color:red;">*</span></p>
                  <textarea name="short_description" rows="8" cols="80" id="article-ckeditor"></textarea>
                </div>
                <div class="col-md-6">
                  <p><b>Qısa məlumat (en)</b><span style="color:red;"></span></p>
                 <textarea name="short_description_en" rows="8" cols="80" id="article-ckeditor1"></textarea>
                </div>
                <div class="col-md-6">
                  <p><b>Geniş məlumat </b><span style="color:red;">*</span></p>
                 <textarea name="description" rows="8" cols="80" id="article-ckeditor2"></textarea>
                </div>
                <div class="col-md-6">
                  <p><b>Geniş məlumat (en)</b><span style="color:red;"></span></p>
                 <textarea name="description_en" rows="8" cols="80" id="article-ckeditor3"></textarea>
                </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );
  CKEDITOR.replace( 'article-ckeditor2' );
  CKEDITOR.replace( 'article-ckeditor3' );

  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var messageLength1 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength < 1 || messageLength2 < 1 ) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }
    //  if(messageLength >= 500 || messageLength2 >= 500){
    //   alert( 'Mətnlər max 500 karakter olmalıdır' );
    //   e.preventDefault();
    //  }

 });

$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#date-fr, #date-frend').bootstrapMaterialDatePicker({
  format : 'YYYY-MM-DD',
  time: false,
 });
</script>
@endsection
