@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('videogallery.store') }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="col-md-4">
                  <p><b>Qallereya  Başlığı </b><span style="color:red;">* </span></p>
                  <input type="text" name="video_name"  class="form-control"  required>
                </div>
                <div class="col-md-4">
                  <p><b>Qallereya  Başlığı (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="video_name_en"  class="form-control"  >
                </div>
                <div class="col-md-4">
                  <p><b>Video Linki (İframe)</b><span style="color:red;">* </span></p>
                  <input type="text" name="video_iframe"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>Tədbir tarixi <span style="color:red">*</span></b></p>
                  <input type="text" name="video_date" value="" class="form-control" required id="date-fr">
                </div>
                <div class="col-md-6">
                  <p><b>Video Ön şəkli (Fayl ölçüsü max 1024kb olmalıdır (min:580x240))</b><span style="color:red;">* </span></p>
                  <input type="file" name="video_thumbnail"  class="form-control"  required  id="logo">
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
 $('#date-fr').bootstrapMaterialDatePicker({
   format : 'YYYY-DD-MM',
   time: false,
  });
$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#imgs').bind('change', function(event) {
  var length = $(this).context.files.length
   for (var i = 0; i < length; i++) {
     if ($(this).context.files[i].size/ 1024 > 1024) {
       $(this).replaceWith($(this).val('').clone(true));
       alert('Fayl 1024kb dan böyükdür');
     }
   }
});
</script>
@endsection
