@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('videogallery.update', $video->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Qallereya  Başlığı </b><span style="color:red;">* </span></p>
                    <input type="text" name="video_name"  class="form-control"  required value="{{$video->video_name}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Qallereya  Başlığı (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="video_name_en"  class="form-control"   value="{{$video->video_name_en}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Tədbir tarixi <span style="color:red">*</span></b></p>
                    <input type="text" name="video_date" value="{{$video->video_date}}" class="form-control" required id="date-fr">
                  </div>
                  <div class="col-md-6">
                    <p><b>Video Linki (iframe) <span style="color:red">*</span></b></p>
                    <input type="text" name="video_iframe" value="{{$video->video_iframe}}" class="form-control" required id="date-fr">
                  </div>
                <div class="col-md-6" >
                  <p><b>Video Ön şəkli </b><br><span style="color:red;">(Fayl ölçüsü max 1024kb olmalıdır (min:580x240))</span></p>
                  <input type="file" name="video_thumbnail"  class="form-control" id="cover" style="display:n one;">
                  <div class="col-md-6" style="padding-top:40px;">
                    <label for="cover"><img src="/uploads/{{$video->video_thumbnail}}" class="img-responsive" style="cursor:pointer"/></label>
                    <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                    <input type="file" name="img_src[]" multiple class="form-control"> -->
                  </div>
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

     <script type="text/javascript">
     $('#date-fr').bootstrapMaterialDatePicker({
       format : 'YYYY-MM-DD',
       time: false,
      });
    $('#cover, #intro').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
