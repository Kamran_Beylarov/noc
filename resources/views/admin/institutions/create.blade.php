@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('institutions.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-4">
                  <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="title"  class="form-control"  required>
                </div>
                <div class="col-md-4">
                  <p><b>Kateqoriya adı  (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="title_en"  class="form-control"  >
                </div>
                <div class="col-md-4">
                  <p><b>Sıra nömrəsi</b><span style="color:red;"> </span></p>
                  <input type="text" name="ins_order"  class="form-control"  >
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
