@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('institutions.update', $cate->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="title"  class="form-control"  required value="{{$cate->title}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="title_en"  class="form-control" value="{{$cate->title_en}}" >
                  </div>
                  <div class="col-md-4">
                    <p><b>Sıra nömrəsi</b><span style="color:red;"> </span></p>
                    <input type="text" name="ins_order"  class="form-control"  value="{{$cate->ins_order}}">
                  </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

@endsection
