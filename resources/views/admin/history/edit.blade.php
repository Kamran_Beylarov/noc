@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('history.update', $history->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">

                  <div class="col-md-12">
                    <p><b>Tarix<span style="color:red">*</span></b></p>
                    <textarea name="history" rows="8" cols="80" required id="article-ckeditor2">{{$history->history}}</textarea>
                  </div>
                  <div class="col-md-12">
                    <p><b>Tarix (en)<span style="color:red"></span></b></p>
                    <textarea name="history_en" rows="8" cols="80"  id="article-ckeditor3">{{$history->history_en}}</textarea>
                  </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var CSRFToken = $('meta[name="csrf-token"]').attr('content');

var options = {
  filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
  filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
  filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
  filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='+CSRFToken
};
CKEDITOR.replace( 'article-ckeditor2', options );
CKEDITOR.replace( 'article-ckeditor3', options );

$("form").submit( function(e) {

   var messageLength2 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
   // var messageLength3 = CKEDITOR.instances['article-ckeditor3'].getData().replace(/<[^>]*>/gi, '').length;
   if( messageLength2 < 1) {
       alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
       e.preventDefault();
   }

});

     </script>
@endsection
