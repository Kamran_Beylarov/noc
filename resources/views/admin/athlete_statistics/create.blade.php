@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('athlete_statistics.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="col-md-3">
                  <p><b>İdmançı </b><span style="color:red;">*</span></p>
                <select class="form-control" name="athlete_id" required>
                  <option value="">Seçin</option>
                  @foreach($athlete as $athl)
                    <option value="{{$athl->id}}">{{$athl->athlete_name}}</option>
                  @endforeach
                </select>
                </div>
                <div class="col-md-3">
                  <p><b>Yarış tipi </b><span style="color:red;">*</span></p>
                <select class="form-control" name="tournament_type" required id="tournament_type">
                  <option value="">Seçin</option>
                  <option value="1">Olimpiya</option>
                  <option value="2">Qeyri Olimpiya</option>
                </select>
                </div>
                <div class="col-md-3">
                  <p><b>Yarışlar </b><span style="color:red;">*</span></p>
                <select class="form-control" name="tournament_id" required id="tournaments">
                  <option value="">Seçin</option>
                </select>
                </div>
                <div class="col-md-3">
                  <p><b>Medal </b><span style="color:red;">*</span></p>
                <select class="form-control" name="medal" required>
                  <option value="">Seçin</option>
                  <option value="1">Qızıl</option>
                  <option value="2">Gümüş</option>
                  <option value="3">Bürünc</option>
                  <option value="4">İştirakçı</option>
                </select>
                </div>

                <div class="col-md-2">
                  <p><b>İl</b><span style="color:red;">* </span></p>
                  <input type="number" name="year"  class="form-control"  required>
                </div>
                <div class="col-md-2">
                  <p><b>Şəhər adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="city"  class="form-control"  required>
                </div>
                <div class="col-md-2">
                  <p><b>Şəhər adı (en) </b><span style="color:red;"> </span></p>
                  <input type="text" name="city_en"  class="form-control"  >
                </div>
                <div class="col-md-4">
                  <p><b>Komanda</b><span style="color:red;"></span></p>
                  <div class="form-check form-switch">
                    <input class="form-control check" type="checkbox"  id="check" name="is_team" value="1" />
                    <label  for="check"></label>
                  </div>
                </div>
                <div class="col-md-2">
                  <p><b>Çəki</b><span style="color:red;"> </span></p>
                  <input type="number" name="weight"  class="form-control weight"  >
                </div>
                <div class="col-md-6">
                  <p><b>İdman novünün tipi</b><span style="color:red;"></span></p>
                  <input type="text" name="sport_type"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>İdman novünün tipi (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="sport_type_en"  class="form-control"  >
                </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('#tournament_type').on('change', function(){
    var type = $(this).val();
    $.ajax({
      type:"GET",
      cache:false,
      url:"/admin/tournaments_type/",
      data:{type:type},
      success: function (html) {
        $('#tournaments').empty();
        $('#tournaments').append('<option value="">Seçin</option>');
        $.each(html, function( index, el) {
          if(type == 2){
            $('#tournaments').append('<option value="'+el.id+'">'+el.tournament_name+'</option>');
          }else{
            $('#tournaments').append('<option value="'+el.id+'">'+el.game_name+'</option>');
          }


        });
        // if (html.aparat == 1) {
        //   $('.id'+html.id).empty().html('Var').removeClass('btn-danger').addClass('btn-success');
        // }else{
        //   $('.id'+html.id).empty().html('Yoxdur').removeClass('btn-success').addClass('btn-danger');
        // }
      }
    });
  })
  $('.check').on('change',function(){
    if($('input.check').is(':checked')){
      $('.weight').prop('disabled',true);
    }else{
      $('.weight').prop('disabled',false);
    }
    
  });
</script>
@endsection
