@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('athlete.update', $athlete->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">

                  <div class="col-md-12">
                    <p><b>İdman növü  </b><span style="color:red;">*</span></p>
                    <select class="form-control" name="sport_id" required>
                      <option value="">Seçin</option>
                      @foreach($sports as $sport)
                      <option value="{{$sport->id}}" @if($athlete->sport_id == $sport->id) selected @endif>{{$sport->sport_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-4">
                    <p><b>Ad Soyad  </b><span style="color:red;">* </span></p>
                    <input type="text" name="athlete_name"  class="form-control"  required value="{{$athlete->athlete_name}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Ad Soyad  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="athlete_name_en"  class="form-control"  value="{{$athlete->athlete_name_en}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Doğum tarixi </b><span style="color:red;">*</span></p>
                    <input type="text" name="athlete_birthday"  class="form-control" required id="date-frend" value="{{$athlete->athlete_birthday}}">
                  </div>

                  <div class="col-md-4">
                    <p><b>Boy (sm) </b><span style="color:red;"></span></p>
                    <input type="number" name="athlete_height"  class="form-control"  value="{{$athlete->athlete_height}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Çəki (kq) </b><span style="color:red;"></span></p>
                    <input type="number" name="athlete_weight"  class="form-control"  value="{{$athlete->athlete_weight}}" >
                  </div>
                  <div class="col-md-4">
                    <p><b>Cins </b><span style="color:red;">*</span></p>
                  <select class="form-control" name="athlete_gender" required>
                    <option value="">Seçin</option>
                    <option value="male"  @if($athlete->athlete_gender == 'male') selected @endif>Kişi</option>
                    <option value="female"  @if($athlete->athlete_gender == 'female') selected @endif>Qadın</option>
                  </select>
                  </div>
                  <div class="col-md-12">
                    <p><b>Şəxsi Məşqçisi </b><span style="color:red;"></span></p>
                    <input type="text" name="personal_trainer"  class="form-control" value="{{$athlete->personal_trainer}}" >
                  </div>
                <div class="col-md-12" >
                  <p><b>Profil fotosu (Fayl ölçüsü max 1024kb olmalıdır (min:358x400))</b><span style="color:red;"> </span></p>
                  <input type="file" name="athlete_img"  class="form-control" id="logo" style="display:n one;">
                  <div class="col-md-3" style="padding-top:40px;">
                    <label for="logo"><img src="/uploads/{{$athlete->athlete_img}}" class="img-responsive" style="cursor:pointer"/></label>
                    <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                    <input type="file" name="img_src[]" multiple class="form-control"> -->
                  </div>
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#date-fr, #date-frend').bootstrapMaterialDatePicker({
  format : 'YYYY-MM-DD',
  time: false,
 });
</script>
@endsection
