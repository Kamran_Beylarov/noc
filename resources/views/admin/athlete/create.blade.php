@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('athlete.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                  <p><b>İdman növü  </b><span style="color:red;">*</span></p>
                  <select class="form-control" name="sport_id" required>
                    <option value="">Seçin</option>
                    @foreach($sports as $sport)
                    <option value="{{$sport->id}}">{{$sport->sport_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <p><b>Ad Soyad  </b><span style="color:red;">* </span></p>
                  <input type="text" name="athlete_name"  class="form-control"  required>
                </div>
                <div class="col-md-3">
                  <p><b>Ad Soyad  (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="athlete_name_en"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Profil fotosu (Fayl ölçüsü max 1024kb olmalıdır (min:358x400))</b><span style="color:red;">* </span></p>
                  <input type="file" name="athlete_img"  class="form-control"  id="logo" required>
                </div>
                <div class="col-md-3">
                  <p><b>Doğum tarixi </b><span style="color:red;">*</span></p>
                  <input type="text" name="athlete_birthday"  class="form-control" required id="date-frend">
                </div>

                <div class="col-md-3">
                  <p><b>Boy (sm) </b><span style="color:red;"></span></p>
                  <input type="number" name="athlete_height"  class="form-control"  >
                </div>
                <div class="col-md-3">
                  <p><b>Çəki (kq) </b><span style="color:red;"></span></p>
                  <input type="number" name="athlete_weight"  class="form-control"  >
                </div>
              
                <div class="col-md-3">
                  <p><b>Cins </b><span style="color:red;">*</span></p>
                <select class="form-control" name="athlete_gender" required>
                  <option value="">Seçin</option>
                  <option value="male">Kişi</option>
                  <option value="female">Qadın</option>
                </select>
                </div>
                <div class="col-md-12">
                  <p><b>Şəxsi Məşqçisi </b><span style="color:red;"></span></p>
                  <input type="text" name="personal_trainer"  class="form-control"  >
                </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#date-fr, #date-frend').bootstrapMaterialDatePicker({
  format : 'YYYY-MM-DD',
  time: false,
 });
</script>
@endsection
