@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('success') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('athlete.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>İdmançı adı</th>
                          <th>Kommisiya</th>
                          <th>Önə çıxsın</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($athletes as $athlete)
                      <tr>
                          <td>
                            {{$athlete->athlete_name}}
                          </td>
                          <td>
                            @if($athlete->kommisia == 1)
                               <button type="button" class="btn btn-success index_status id{{$athlete->id}}" value="{{$athlete->id}}" >Var</button>
                               @else
                               <button type="button" class="btn btn-danger index_status id{{$athlete->id}}" value="{{$athlete->id}}" >Yoxdur</button>
                               @endif
                          </td>
                            <td>
                            @if($athlete->significance_status == 1)
                               <button type="button" class="btn btn-success significance_status idi{{$athlete->id}}" value="{{$athlete->id}}" >Hə</button>
                               @else
                               <button type="button" class="btn btn-danger significance_status idi{{$athlete->id}}" value="{{$athlete->id}}" >Yoxdur</button>
                               @endif
                          </td>
                          <td>
                            <button value="{{$athlete->id}}" class="btn btn-danger remove" >Sil</button>
                              <form action="{{ route('athlete.destroy', $athlete->id) }}" method="post" style="display: none;"  id="remove{{$athlete->id}}">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('athlete.edit', $athlete->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
    {{$athletes->links()}}
</div>
<script type="text/javascript">
$('.index_status').on('click',function(e){
  var id = $(this).val();
  $.ajax({
    type:"GET",
    cache:false,
    url:"/admin/athlete_kommisia/",
    data:{id:id},
    success: function (html) {
      console.log(html);
      if (html.kommisia == 1) {
        $('.id'+html.id).empty().html('Var').removeClass('btn-danger').addClass('btn-success');
      }else{
        $('.id'+html.id).empty().html('Yoxdur').removeClass('btn-success').addClass('btn-danger');
      }
    }
  });
});
$('.significance_status').on('click',function(e){
  var id = $(this).val();
  $.ajax({
    type:"GET",
    cache:false,
    url:"/admin/significance_status/",
    data:{id:id},
    success: function (html) {
      console.log(html);
      if (html.significance_status == 1) {
        $('.idi'+html.id).empty().html('Hə').removeClass('btn-danger').addClass('btn-success');
      }else{
        $('.idi'+html.id).empty().html('Yoxdur').removeClass('btn-success').addClass('btn-danger');
      }
    }
  });
});
$('.remove').on('click', function(){
        var val = $(this).val();
      
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
         },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Əminsiniz?',
      text:"idmançı və ona aid bütün nəticələr silinəcək !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Bəli, Sil!',
      cancelButtonText: 'Ləğv et!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        $('#remove'+val+'').submit();
       
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Ləğv edildi',
        )
      }
    })
    });
</script>
@endsection
