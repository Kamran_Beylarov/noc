@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('partners.update', $partner->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">

                  <div class="col-md-6">
                    <p><b>Tərəfdaş adı</b><span style="color:red;">* </span></p>
                    <input type="text" name="partner_name"  class="form-control"  required value="{{$partner->partner_name}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Tərəfdaş adı (en)</b><span style="color:red;"> </span></p>
                    <input type="text" name="partner_name_en"  class="form-control"   value="{{$partner->partner_name_en}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Veb sayt linki</b><span style="color:red;">* </span></p>
                    <input type="text" name="web_site"  class="form-control"  required value="{{$partner->web_site}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Sıra nömrəsi</b><span style="color:red;">* </span></p>
                    <input type="number" name="partner_order"  class="form-control"  required value="{{$partner->partner_order}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Qısa məlumat<span style="color:red">*</span></b></p>
                    <textarea name="short_description" rows="8" cols="80" required id="article-ckeditor">{{$partner->short_description}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Qısa məlumat (en)<span style="color:red"></span></b></p>
                    <textarea name="short_description_en" rows="8" cols="80"  id="article-ckeditor1">{{$partner->short_description_en}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Məlumat detalları<span style="color:red">*</span></b></p>
                    <textarea name="description" rows="8" cols="80" required id="article-ckeditor2">{{$partner->description}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Məlumat detalları (en)<span style="color:red"></span></b></p>
                    <textarea name="description_en" rows="8" cols="80"  id="article-ckeditor3">{{$partner->description_en}}</textarea>
                  </div>
                              <div class="col-md-12" >
                                <p><b>Logo </b><br><span style="color:red;"></span></p>
                                <input type="file" name="logo"  class="form-control" id="cover" style="display:n one;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="cover"><img src="/uploads/{{$partner->logo}}" class="img-responsive" style="cursor:pointer"/></label>
                                  <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                                  <input type="file" name="img_src[]" multiple class="form-control"> -->
                                </div>
                              </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace( 'article-ckeditor' );
CKEDITOR.replace( 'article-ckeditor1' );
CKEDITOR.replace( 'article-ckeditor2' );
CKEDITOR.replace( 'article-ckeditor3' );

$("form").submit( function(e) {
   var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
   // var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
   var messageLength2 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
   // var messageLength3 = CKEDITOR.instances['article-ckeditor3'].getData().replace(/<[^>]*>/gi, '').length;
   if( messageLength < 1  || messageLength2 < 1 ) {
       alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
       e.preventDefault();
   }

});

$('#date-fr').bootstrapMaterialDatePicker({
  format : 'YYYY-DD-MM',
  time: false,
 });
$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
    $('#cover, #intro').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
