@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('publications.update', $publication->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-6">
                    <p><b>Nəşrin adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="publication_name"  class="form-control"  required value={{$publication->publication_name}}>
                  </div>
                  <div class="col-md-6">
                    <p><b>Nəşrin adı  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="publication_name_en"  class="form-control"   value={{$publication->publication_name_en}}>
                  </div>
                  <div class="col-md-6">
                    <p><b>Müəllif</b><span style="color:red;">* </span></p>
                    <input type="text" name="author_name"  class="form-control"  required value={{$publication->author_name}}>
                  </div>
                  <div class="col-md-6">
                    <p><b>Müəllif (en) <span style="color:red"></span></b></p>
                    <input type="text" name="author_name_en"  class="form-control"  value={{$publication->author_name_en}}>
                  </div>
                <div class="col-md-6" >
                  <p><b>Thumbnail </b><br><span style="color:red;">(Fayl ölçüsü max 1024kb olmalıdır (min:150x190))</span></p>
                  <input type="file" name="publication_thumbnail"  class="form-control" id="cover" style="display:n one;">
                  <div class="col-md-6" style="padding-top:40px;">
                    <label for="cover"><img src="/uploads/{{$publication->publication_thumbnail}}" class="img-responsive" style="cursor:pointer"/></label>
                    <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                    <input type="file" name="img_src[]" multiple class="form-control"> -->
                  </div>
                </div>
                <div class="col-md-6" >
                  <p><b>Nəşrin elektron forması (PDF) </b></p>
                  <input type="file" name="publication_file"  class="form-control"  style="display:n one;">
                  <div class="col-md-6" style="padding-top:40px;">
                      <a target="_blank" href="/uploads/{{$publication->publication_file}}">{{$publication->publication_file}}</a>
                  </div>
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

     <script type="text/javascript">
    $('#cover, #intro').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
