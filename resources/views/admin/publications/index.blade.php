@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('publications.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Nəşrin adı </th>
                          <th>Müəllif</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($publications as $publication)
                      <tr>

                          <td>{{ $publication->publication_name }}</td>
                          <td>{{ $publication->author_name }}</td>

                          <td>
                            <button value="{{$publication->id}}" class="btn btn-danger remove">Sil</button>
                              <form id="remove{{$publication->id}}" action="{{ route('publications.destroy', $publication->id) }}" method="post" style="display: none;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('publications.edit', $publication->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<script>

    $('.remove').on('click', function(){
        var val = $(this).val();
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
         },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Əminsiniz?',
      text: "Nəşr silinəcək",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Bəli, Sil!',
      cancelButtonText: 'Ləğv et!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        $('#remove'+val+'').submit();
       
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Ləğv edildi',
        )
      }
    })
    });
    </script>
@endsection
