@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('members.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Üzvlər  </th>
                          <th>Kateqoriya </th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($members as $meber)
                        @foreach($meber->cate_members as $memm)
                        <tr>

                            <td>{{ $memm->member_name }}</td>
                            <td>{{ $meber->title }}</td>
                            <td>
                                <form action="{{ route('members.destroy', $memm->id) }}" method="post" style="display: initial;">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="submit" value="Sil" class="btn btn-danger">
                                </form>
                                <a href="{{ route('members.edit', $memm->id) }}" class="btn-success btn">Redaktə et</a>
                            </td>
                        </tr>
                        @endforeach
                      @endforeach
                  </tbody>
              </table>





            </div>
        </div>
    </div>
</div>
@endsection
