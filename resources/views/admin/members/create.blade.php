@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('members.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-6">
                  <p><b>Üzv adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="member_name"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>Üzv adı  (en)</b><span style="color:red;"></span></p>
                  <input type="text" name="member_name_en"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Vəzifəsi  </b><span style="color:red;">* </span></p>
                  <input type="text" name="member_post"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>Vəzifəsi  (en)</b><span style="color:red;"></span></p>
                  <input type="text" name="member_post_en"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Kateqoriya </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="institutions_id" required>
                    <option value="">Seçin</option>
                    @foreach($cates as $cate)
                    <option value="{{$cate->id}}">{{$cate->title}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                  <p><b>Federasiya </b><span style="color:red;"> </span></p>
                  <select class="form-control" name="federation_id" >
                    <option value="">Seçin</option>
                    @foreach($federations as $federation)
                    <option value="{{$federation->id}}">{{$federation->federation_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
@endsection
