@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('federations.update', $federation->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Federasiya növü  </b><span style="color:red;">* </span></p>
                    <select class="form-control" name="federation_type" required>
                      <option value="">Seçin</option>
                      <option value="1" @if($federation->federation_type == 1) selected @endif>Olimpiya</option>
                      <option value="0" @if($federation->federation_type == 0) selected @endif>Qeyri Olimpiya</option>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <p><b>Federasiya adı  </b><span style="color:red;"> </span></p>
                    <input type="text" name="federation_name"  class="form-control"   value="{{$federation->federation_name}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Federasiya adı (en) </b><span style="color:red;"> </span></p>
                    <input type="text" name="federation_name_en"  class="form-control"   value="{{$federation->federation_name_en}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Federasiya rəhbərinin adı</b><span style="color:red;"> </span></p>
                    <input type="text" name="drectory_name"  class="form-control"    value="{{$federation->drectory_name}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Federasiya rəhbərinin adı (en)</b><span style="color:red;"> </span></p>
                    <input type="text" name="drectory_name_en"  class="form-control"    value="{{$federation->drectory_name_en}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Telefon</b><span style="color:red;"> </span></p>
                    <input type="text" name="telefon1"  class="form-control"    value="{{$federation->telefon1}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Telefon (2)</b><span style="color:red;"> </span></p>
                    <input type="text" name="telefon2"  class="form-control"    value="{{$federation->telefon2}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Fax</b><span style="color:red;"> </span></p>
                    <input type="text" name="fax"  class="form-control"    value="{{$federation->fax}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Veb sayt linki</b><span style="color:red;"> </span></p>
                    <input type="text" name="web_site"  class="form-control"    value="{{$federation->web_site}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Elektron poçt</b><span style="color:red;"> </span></p>
                    <input type="email" name="email"  class="form-control"    value="{{$federation->email}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Ünvan</b><span style="color:red;"> </span></p>
                    <input type="text" name="address"  class="form-control" value="{{$federation->address}}"   >
                  </div>
                              <div class="col-md-12" >
                              <p><b>İcon (min:54x54)</b><span style="color:red;">* </span></p>
                                <input type="file" name="federation_icon"  class="form-control" id="logo" style="display:n one;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="cover"><img src="/uploads/{{$federation->federation_icon}}" class="img-responsive" style="cursor:pointer"/></label>
                                  <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                                  <input type="file" name="img_src[]" multiple class="form-control"> -->
                                </div>
                              </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});

     </script>
@endsection
