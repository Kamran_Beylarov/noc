@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('federations.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="col-md-4">
                  <p><b>Federasiya növü  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="federation_type" required>
                    <option value="">Seçin</option>
                    <option value="1">Olimpiya</option>
                    <option value="0">Qeyri Olimpiya</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <p><b>Federasiya adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="federation_name"  class="form-control"  required>
                </div>
                <div class="col-md-4">
                  <p><b>Federasiya adı (en) </b><span style="color:red;"> </span></p>
                  <input type="text" name="federation_name_en"  class="form-control"  >
                </div>
                <div class="col-md-4">
                  <p><b>İcon (min:54x54)</b><span style="color:red;"> </span></p>
                  <input type="file" name="federation_icon"  class="form-control"    id="logo">
                </div>
                <div class="col-md-4">
                  <p><b>Federasiya rəhbərinin adı</b><span style="color:red;"> </span></p>
                  <input type="text" name="drectory_name"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Federasiya rəhbərinin adı (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="drectory_name_en"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Telefon</b><span style="color:red;"> </span></p>
                  <input type="text" name="telefon1"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Telefon (2)</b><span style="color:red;"> </span></p>
                  <input type="text" name="telefon2"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Fax</b><span style="color:red;"> </span></p>
                  <input type="text" name="fax"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Veb sayt linki</b><span style="color:red;"> </span></p>
                  <input type="text" name="web_site"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Elektron poçt</b><span style="color:red;"> </span></p>
                  <input type="email" name="email"  class="form-control"    >
                </div>
                <div class="col-md-4">
                  <p><b>Ünvan</b><span style="color:red;"> </span></p>
                  <input type="text" name="address"  class="form-control"    >
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
</script>
@endsection
