@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('committees.update', $committee->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">

                  <div class="col-md-6">
                    <p><b>Kateqoriya adı<span style="color:red">*</span></b></p>
                    <input type="text" name="menu_name" value="{{$committee->menu_name}}" class="form-control" required>
                  </div>
                  <div class="col-md-6">
                    <p><b>Kateqoriya adı (en)<span style="color:red"></span></b></p>
                    <input type="text" name="menu_name_en" value="{{$committee->menu_name_en}}" class="form-control" >
                  </div>
                  <div class="col-md-6">
                    <p><b>Səhifə başlığı<span style="color:red">*</span></b></p>
                    <input type="text" name="title" value="{{$committee->title}}" class="form-control" required>
                  </div>
                  <div class="col-md-6">
                    <p><b>Səhifə başlığı(en)<span style="color:red"></span></b></p>
                    <input type="text" name="title_en" value="{{$committee->title_en}}" class="form-control" >
                  </div>

                  <div class="col-md-6">
                    <p><b>Məlumat<span style="color:red">*</span></b></p>
                    <textarea name="description" rows="8" cols="80" required id="article-ckeditor2">{{$committee->description}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Məlumat (en)<span style="color:red"></span></b></p>
                    <textarea name="description_en" rows="8" cols="80"  id="article-ckeditor3">{{$committee->description_en}}</textarea>
                  </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var CSRFToken = $('meta[name="csrf-token"]').attr('content');

var options = {
  filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
  filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token='+CSRFToken,
  filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
  filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='+CSRFToken
};
CKEDITOR.replace( 'article-ckeditor2', options );
CKEDITOR.replace( 'article-ckeditor3', options );

$("form").submit( function(e) {

   var messageLength2 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
   // var messageLength3 = CKEDITOR.instances['article-ckeditor3'].getData().replace(/<[^>]*>/gi, '').length;
   if( messageLength2 < 1) {
       alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
       e.preventDefault();
   }

});

     </script>
@endsection
