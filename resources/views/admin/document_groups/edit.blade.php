@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('document_groups.update', $group->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="group_name"  class="form-control"  required value="{{$group->group_name}}">
                  </div>

                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  (en)</b><span style="color:red;"></span></p>
                    <input type="text" name="group_name_en"  class="form-control" value="{{$group->group_name_en}}"  >
                  </div>
                  <div class="col-md-4">
                    <p><b>Sıra nömrəsi </b><span style="color:red;">*</span></p>
                    <input type="number" name="group_order"  class="form-control"  required value="{{$group->group_order}}">
                  </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">



     </script>
@endsection
