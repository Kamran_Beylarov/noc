@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('sponsors.update', $sponsor->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-6">
                    <p><b>Sponsor adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="sponsor_name"  class="form-control"  required value="{{$sponsor->sponsor_name}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Sıra nömrəsi </b><span style="color:red;">* </span></p>
                    <input type="number" name="sponsor_order"  class="form-control"  required value="{{$sponsor->sponsor_order}}">
                  </div>

                  <div class="col-md-6">
                    <p><b>Haqqında məlumat </b><span style="color:red;">* </span></p>
                    <textarea name="sponsor_detalis" rows="8" cols="80" class="form-control" id="article-ckeditor">{{$sponsor->sponsor_detalis}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Haqqında məlumat (en) </b><span style="color:red;"> </span></p>
                    <textarea name="sponsor_detalis_en" rows="8" cols="80" class="form-control" id="article-ckeditor1">{{$sponsor->sponsor_detalis_en}}</textarea>
                  </div>


                  <div class="col-md-4">
                    <p><b>Veb sayt linki</b><span style="color:red;"> </span></p>
                    <input type="text" name="web_site"  class="form-control" value="{{$sponsor->web_site}}"   >
                  </div>
                  <div class="col-md-4">
                    <p><b>Facebook linki</b><span style="color:red;"> </span></p>
                    <input type="fb" name="fb"  class="form-control" value="{{$sponsor->fb}}"   >
                  </div>
                  <div class="col-md-4">
                    <p><b>İnstagram linki</b><span style="color:red;"> </span></p>
                    <input type="text" name="ins"  class="form-control"  value="{{$sponsor->ins}}"  >
                  </div>
                              <div class="col-md-6" >
                                <p><b>Logo </b><br><span style="color:red;"></span></p>
                                <input type="file" name="logo"  class="form-control" id="logo" style="display:none;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="logo"><img src="/uploads/{{$sponsor->logo}}" class="img-responsive" style="cursor:pointer"/></label>

                                </div>
                              </div>
                              <div class="col-md-6" >
                                <p><b>Logo (ağ)</b><br><span style="color:red;"></span></p>
                                <input type="file" name="logo_white"  class="form-control" id="logo_white" style="display:none;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="logo_white"><img src="/uploads/{{$sponsor->logo_white}}" class="img-responsive" style="cursor:pointer"/></label>

                                </div>
                              </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );

  $(".form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     // var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength < 1  ) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }

 });

$('#logo, #logo_white').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});

     </script>
@endsection
