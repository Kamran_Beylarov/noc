@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('executive.update', $icraye->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-6">
                    <p><b>Səhifə başlığı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="title"  class="form-control"  required value="{{$icraye->title}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Səhifə başlığı  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="title_en"  class="form-control"  value="{{$icraye->title_en}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Kontent<span style="color:red">*</span></b></p>
                    <textarea name="description" rows="8" cols="80" required id="article-ckeditor">{{$icraye->description}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Kontent (en)<span style="color:red"></span></b></p>
                    <textarea name="description_en" rows="8" cols="80" required id="article-ckeditor1">{{$icraye->description_en}}</textarea>
                  </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );

  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength < 1) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }
 });
</script>
@endsection
