@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('documents.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="col-md-4">
                  <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                <select class="form-control" name="group_id" required>
                  <option value="">Seçin</option>
                  @foreach($groups as $group)
                  <option value="{{$group->id}}">{{$group->group_name}}</option>
                  @endforeach
                </select>
                </div>
                <div class="col-md-4">
                  <p><b>Dil</b><span style="color:red;">* </span></p>
                  <select class="form-control" name="lang" required>
                    <option value="">Seçin</option>
                    <option value="az">AZ</option>
                    <option value="en">EN</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <p><b>Sənəd adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="document_name"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>Link  </b><span style="color:red;"> </span></p>
                  <input type="text" name="url"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Sənəd (PDF)</b><span style="color:red;"> </span></p>
                  <input type="file" name="document_file"  class="form-control"    >
                </div>



                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>

@endsection
