@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('documents.update', $document->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="group_id" required>
                    <option value="">Seçin</option>
                    @foreach($groups as $group)
                    <option value="{{$group->id}}" @if($group->id == $document->group_id) selected @endif>{{$group->group_name}}</option>
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-4">
                    <p><b>Dil</b><span style="color:red;">* </span></p>
                    <select class="form-control" name="lang" required>
                      <option value="">Seçin</option>
                      <option value="az" @if('az' == $document->lang) selected @endif>AZ</option>
                      <option value="en" @if('en' == $document->lang) selected @endif>EN</option>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <p><b>Sənəd adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="document_name"  class="form-control"  required value="{{$document->document_name}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Link  </b><span style="color:red;"> </span></p>
                    <input type="text" name="url"  class="form-control"  value="{{$document->url}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Sənəd (PDF)</b><span style="color:red;"> </span></p>
                    <input type="file" name="document_file"  class="form-control"    >
                    <a href="/uploads/{{$document->document_file}}" target="_blank">{{$document->document_file}}</a>
                  </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script type="text/javascript">



     </script>
@endsection
