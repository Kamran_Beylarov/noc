@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('game_statistics.update', $statistic->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                <div class="col-md-6">
                  <p><b>Oyunlar  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="game_id" required>
                    <option value="">Seçin</option>
                    @foreach($games as $games)
                    <option value="{{$games->id}}" @if($statistic->game_id == $games->id) selected @endif >{{$games->game_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                  <p><b>İdman Növləri  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="sport_id" required>
                    <option value="">Seçin</option>
                    @foreach($sports as $sport)
                    <option value="{{$sport->id}}" @if($statistic->sport_id == $sport->id) selected @endif >{{$sport->sport_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-4">
                  <p><b>Kişi sayı</b><span style="color:red;"> *</span></p>
                  <input type="number" name="male"  class="form-control" required value="{{$statistic->male}}" >
                </div>
                <div class="col-md-4">
                  <p><b>Qadın sayı</b><span style="color:red;"> *</span></p>
                  <input type="number" name="female"  class="form-control" required value="{{$statistic->female}}" >
                </div>
            

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>

@endsection
