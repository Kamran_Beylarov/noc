@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('tournaments.update', $tournament->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">



                  <div class="col-md-6">
                    <p><b>Turnir adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="tournament_name"  class="form-control"  required value="{{$tournament->tournament_name}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Turnir adı  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="tournament_name_en"  class="form-control"  value="{{$tournament->tournament_name}}">
                  </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>


@endsection
