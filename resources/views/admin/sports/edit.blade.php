@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('sports.update', $sport->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-6">
                    <p><b>İdman növü </b><span style="color:red;">* </span></p>
                    <input type="text" name="sport_name"  class="form-control"  required value="{{$sport->sport_name}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>İdman növü (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="sport_name_en"  class="form-control" value="{{$sport->sport_name_en}}" >
                  </div>
                <div class="col-md-12" >
                  <p><b>Icon </b><br><span style="color:red;">(min:92x92)</span></p>
                  <input type="file" name="sport_icon"  class="form-control" id="cover" style="display:n one;">
                  <div class="col-md-3" style="padding-top:40px;">
                    <label for="cover"><img src="/uploads/{{$sport->sport_icon}}" class="img-responsive" style="cursor:pointer"/></label>
                    <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                    <input type="file" name="img_src[]" multiple class="form-control"> -->
                  </div>
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

     <script type="text/javascript">

    $('#cover, #intro').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
