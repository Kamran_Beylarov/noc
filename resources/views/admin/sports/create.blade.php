@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('sports.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-6">
                  <p><b>İdman növü   </b><span style="color:red;">* </span></p>
                  <input type="text" name="sport_name"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>İdman növü   (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="sport_name_en"  class="form-control"  >
                </div>
                <div class="col-md-12">
                  <p><b>Icon (min:92x92)</b><span style="color:red;">* </span></p>
                  <input type="file" name="sport_icon"  class="form-control"  id="logo" required>
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
</script>
@endsection
