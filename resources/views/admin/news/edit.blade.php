@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('news.update', $news->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">

                                  <div class="col-md-3">
                                    <p><b>Tip  </b><span style="color:red;">* </span></p>
                                    <select class="form-control" name="news_type" required id="news">
                                      <option value="">Seçin</option>
                                      <option value="0" @if($news->news_type == 0) selected @endif>Xəbər</option>
                                      <option value="1" @if($news->news_type == 1) selected @endif>Tədbir</option>
                                      <option value="2" @if($news->news_type == 2) selected @endif>Proqram</option>
                                    </select>
                                  </div>
                                  <div class="col-md-3">
                                    <p><b>Önəmlilik  </b><span style="color:red;"> </span></p>
                                    <select class="form-control" name="news_important" >
                                      <option value="">Seçin</option>
                                      <option value="1" @if($news->news_type == 1) selected @endif>Rəsmi</option>
                                      <option value="2" @if($news->news_type == 2) selected @endif>Təcili</option>
                                    </select>
                                  </div>
                                  <div class="col-md-3">
                                    <p><b>Tarix <span style="color:red">*</span></b></p>
                                    <input type="text" name="news_date" value="{{$news->news_date}}" class="form-control" required id="date-fr">
                                  </div>
                                  <div class="col-md-3">
                                    <p><b>Dil  </b><span style="color:red;">* </span></p>
                                    <select class="form-control" name="lang" required>
                                      <option value="">Seçin</option>
                                      <option value="az" @if($news->lang == 'az') selected @endif>Az</option>
                                      <option value="en" @if($news->lang == 'en') selected @endif>En</option>
                                    </select>
                                  </div>
                                  <div class="col-md-12">
                                    <p><b>Başlıq</b><span style="color:red;">* </span></p>
                                    <input type="text" name="news_name"  class="form-control"  required value="{{$news->news_name}}" >
                                  </div>

                                  <div class="col-md-12">
                                    <p><b>Kontent<span style="color:red">*</span></b></p>
                                    <textarea name="news_description" rows="8" cols="80" required id="article-ckeditor">{{$news->news_description}}</textarea>
                                  </div>
                                  <div class="col-md-12 news_options" @if($news->news_type != 0) style="display:none;" @endif>
                                    <div class="col-md-4">
                                      <p><b>Slider də görünsün</b><span style="color:red;"></span></p>
                                      <div class="form-check form-switch">
                                        <input class="form-control" type="checkbox"  id="check" name="intro_status" value="1" @if($news->intro_status == 1) checked @endif />
                                        <label  for="check"></label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <p><b>Slider üçün şəkil (Fayl ölçüsü max 1024kb olmalıdır (min:1280x514))</b><span style="color:red;"> </span></p>
                                      <input type="file" name="intro_img"  class="form-control"    id="intro">
                                      <div class="col-md-6" style="padding-top:40px;">
                                        <label for="intro"><img src="/news/{{$news->intro_img}}" class="img-responsive" style="cursor:pointer"/></label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <p><b>Aid olduğu oyun</b><span style="color:red;"> </span></p>
                                      <select class="form-control" name="game_id" id="games">
                                        <option value="">Seçin</option>
                                        @foreach($games as $game)
                                        <option value="{{$game->id}}" @if($news->game_id == $game->id) selected @endif>{{$game->game_name}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                              <div class="col-md-12" >
                                <p><b>Thumbnail </b><br><span style="color:red;">(Fayl ölçüsü max 1024kb olmalıdır (min:720x400))</span></p>
                                <input type="file" name="news_thumbnail"  class="form-control" id="cover" style="display:n one;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="cover"><img src="/news/{{$news->news_thumbnail}}" class="img-responsive" style="cursor:pointer"/></label>
                                  <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                                  <input type="file" name="img_src[]" multiple class="form-control"> -->
                                </div>
                              </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );

  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
     var news_val = $('#news').val();
     var games_val = $('#games').val()
     if( messageLength < 1) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }
     if(news_val != 0){
       if($('#check').is(':checked')){
         alert('Yalnız xəbərlər sliderdə görünə bilər');
         e.preventDefault();
       }
       if($('#games').val() != ''){
         alert('Yalnız xəbərlərin aid olduğu oyunlar ola bilər');
         e.preventDefault();
         console.log(games_val);
       }
     }

 });

$('#date-fr').bootstrapMaterialDatePicker({
  format : 'YYYY-MM-DD',
  time: false,
 });
$('#logo, #intro').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#news').on('change', function(){
var val =  $(this).val();
  if(val == 0){
    $('.news_options').show();
  }else{
    $('.news_options').hide();
  }
})
     </script>
@endsection
