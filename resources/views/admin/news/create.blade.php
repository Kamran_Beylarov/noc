@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('news.store') }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="col-md-3">
                  <p><b>Tip  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="news_type" required id="news">
                    <option value="">Seçin</option>
                    <option value="0">Xəbər</option>
                    <option value="1">Tədbir</option>
                    <option value="2">Proqram</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <p><b>Önəmlilik  </b><span style="color:red;"> </span></p>
                  <select class="form-control" name="news_important" >
                    <option value="">Seçin</option>
                    <option value="1">Rəsmi</option>
                    <option value="2">Təcili</option>
                  </select>
                </div>
                <div class="col-md-3">
                  <p><b>Tarix <span style="color:red">*</span></b></p>
                  <input type="text" name="news_date" value="" class="form-control" required id="date-fr">
                </div>
                <div class="col-md-3">
                  <p><b>Dil  </b><span style="color:red;">* </span></p>
                  <select class="form-control" name="lang" required>
                    <option value="">Seçin</option>
                    <option value="az">Az</option>
                    <option value="en">En</option>
                  </select>
                </div>
                <div class="col-md-4">
                  <p><b>Başlıq</b><span style="color:red;">* </span></p>
                  <input type="text" name="news_name"  class="form-control news_titile"  required>
                </div>

                <div class="col-md-4">
                  <p><b>Thumbnail (Fayl ölçüsü max 1024kb olmalıdır (min:720x400))</b><span style="color:red;">* </span></p>
                  <input type="file" name="news_thumbnail"  class="form-control news_titile"  required  id="logo">
                </div>
                <div class="col-md-4">
                  <p><b>Qallereya (Fayl ölçüsü max 1024kb olmalıdır (min:560x480))</b><span style="color:red;"></span></p>
                  <input type="file" name="img_src[]"  class="form-control" id="imgs" multiple>
                </div>
                <div class="col-md-12">
                  <p><b>Kontent<span style="color:red">*</span></b></p>
                  <textarea name="news_description" rows="8" cols="80" required id="article-ckeditor"></textarea>
                </div>

                <div class="col-md-12 news_options" style="display:none">
                  <div class="col-md-4">
                    <p><b>Slider də görünsün</b><span style="color:red;"></span></p>
                    <div class="form-check form-switch">
                      <input class="form-control" type="checkbox"  id="check" name="intro_status" value="1" />
                      <label  for="check"></label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <p><b>Slider üçün şəkil (Fayl ölçüsü max 1024kb olmalıdır (min:1280x514))</b><span style="color:red;"> </span></p>
                    <input type="file" name="intro_img"  class="form-control"    id="cover">
                  </div>
                  <div class="col-md-4">
                    <p><b>Aid olduğu oyun</b><span style="color:red;"> </span></p>
                    <select class="form-control" name="game_id" id="games">
                      <option value="">Seçin</option>
                      @foreach($games as $game)
                      <option value="{{$game->id}}">{{$game->game_name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>



                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor');
  // var val = $('.news_titile').val()
  //    var totalSoFar = 0;
  //       for (var i = 0; i < val.length; i++)
  //         if (val[i] === " ") { 
  //           totalSoFar = +1; 
  //        }
  //       console.log(totalSoFar);
  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var news_val = $('#news').val();
     if( messageLength < 1) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }
     if(news_val != 0){
       if($('#check').is(':checked')){
         alert('Yalnız xəbərlər sliderdə görünə bilər');
         e.preventDefault();
       }
       if($('#games').val() != ''){
         alert('Yalnız xəbərlərin aid olduğu oyunlar ola bilər');
         e.preventDefault();
       }
     }
  
 });

$('#date-fr').bootstrapMaterialDatePicker({
  format : 'YYYY-MM-DD',
  time: false,
 });
$('#logo, #cover').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#imgs').bind('change', function(event) {
  var length = $(this).context.files.length
   for (var i = 0; i < length; i++) {
     if ($(this).context.files[i].size/ 1024 > 1024) {
       $(this).replaceWith($(this).val('').clone(true));
       alert('Fayl 1024kb dan böyükdür');
     }
   }
});
$('#news').on('change', function(){
var val =  $(this).val();
  if(val == 0){
    $('.news_options').show();
  }else{
    $('.news_options').hide();
  }
})
</script>
@endsection
