@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('news.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Başlıq</th>
                          <th>Tədbir / Xəbər / Proqram</th>
                          <th>Qallereya</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($news as $new)
                      <tr>

                          <td>{{ $new->news_name }}</td>
                          <td>
                            @if($new->news_type == 0)
                              Xəbər
                            @elseif($new->news_type == 1)
                              Tədbir
                            @else
                              Proqram
                            @endif
                          </td>
                            <td><a href="{{ route('news.show', $new->id) }}" class="btn-primary btn">Qallereya</a></td>
                          <td>
                            <button value="{{$new->id}}" class="btn btn-danger remove" data-type="@if($new->news_type == 0)Xəbər @elseif($new->news_type == 1) Tədbir @else Proqram @endif">Sil</button>
                              <form id="remove{{$new->id}}" action="{{ route('news.destroy', $new->id) }}" method="post" style="display: none;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('news.edit', $new->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>

              {{$news->links()}}



            </div>
        </div>
    </div>
</div>
<script>

    $('.remove').on('click', function(){
        var val = $(this).val();
        var  type = $(this).data('type');
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
         },
      buttonsStyling: false
    })
    
    swalWithBootstrapButtons.fire({
      title: 'Əminsiniz?',
      text:type + "silinəcək",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Bəli, Sil!',
      cancelButtonText: 'Ləğv et!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        $('#remove'+val+'').submit();
       
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Ləğv edildi',
        )
      }
    })
    });
    </script>
@endsection
