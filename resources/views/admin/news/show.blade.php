@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('news.index') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                  <i class="material-icons">keyboard_backspace</i>
                </button></a>
            </div>
            <div class="body table-responsive">


                      @foreach($news->news_gallery as $port)
                            <div class="col-md-3">
                              <img src="/news/{{$port->img_src}}" alt="" style="width:100%;margin-bottom:10px;">


                                <div class="" style="margin-bottom:10px;">
                                  <button class="btn btn-success open_form" data-id="{{$port->id}}">Redaktə et</button>
                                  <a href="/admin/news_imgs/{{$port->id}}/del" class="btn-danger btn">Sil</a>
                                </div>
                                <form action="/admin/news_update_img" method="post"  enctype="multipart/form-data" class="forms" style="display:none;" id="{{$port->id}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="img_id" value="{{$port->id}}">
                                    <input type="file" name="img_file" style="width:100%;">
                                    <button type="submit" class="btn btn-primary" style="margin-top:15px;">Əlavə et</button>

                                </form>
                            </div>

                      @endforeach
                        <form  action="/admin/news_create_img" method="post" enctype="multipart/form-data" class="col-md-3">
                          @csrf
                          <input type="hidden" name="news_id" value="{{$id}}">
                          <label for="" style="color:red">Fayl ölçüsü max 1024kb olmalıdır</label>
                          <input type="file" name="img_src" value="" required id="img">
                          <button type="submit" name="button" class="btn btn-success" style="margin-top:10px;">Əlavə et</button>
                        </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('.open_form').click(function(event) {
    $('.forms').hide();

    var id = $(this).data('id');
    $('#'+id).slideDown();

  });

  $('#img').bind('change', function(event) {
    if ($(this).context.files[0].size/ 1024 > 1024) {
      $(this).replaceWith($(this).val('').clone(true));
      alert('Fayl 1024kb dan böyükdür');
    }
  });

</script>
@endsection
