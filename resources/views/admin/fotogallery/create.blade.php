@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('gallery.store') }}" method="POST" enctype="multipart/form-data">
                @csrf


                <div class="col-md-6">
                  <p><b>Qallereya  Başlığı </b><span style="color:red;">* </span></p>
                  <input type="text" name="gallery_name"  class="form-control"  required>
                </div>
                <div class="col-md-6">
                  <p><b>Qallereya  Başlığı (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="gallery_name_en"  class="form-control"  >
                </div>
                <div class="col-md-6">
                  <p><b>Qallereya Ön şəkli (Fayl ölçüsü max 1024kb olmalıdır (min:280x240))</b><span style="color:red;">* </span></p>
                  <input type="file" name="gallery_thubnail"  class="form-control"  required  id="logo">
                </div>
                <div class="col-md-6">
                  <p><b>Qallereya fotoları
                     (Fayl ölçüsü max 1024kb olmalıdır (min:1180x610))</b><span style="color:red;">* </span></p>
                  <input type="file" name="img_src[]"  class="form-control"  required  id="imgs" multiple>
                </div>




                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#imgs').bind('change', function(event) {
  var length = $(this).context.files.length
   for (var i = 0; i < length; i++) {
     if ($(this).context.files[i].size/ 1024 > 1024) {
       $(this).replaceWith($(this).val('').clone(true));
       alert('Fayl 1024kb dan böyükdür');
     }
   }
});
</script>
@endsection
