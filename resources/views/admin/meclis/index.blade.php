@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('bash_meclis.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Səhifə başlığı</th>
                          <th>Ümumi Tərkib</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($meclis as $mecli)
                      <tr>

                          <td>{{ $mecli->page_name }}</td>

                            <td><a href="{{ route('bash_meclis.show', $mecli->id) }}" class="btn-primary btn">Bax</a></td>
                          <td>
                              <form action="{{ route('bash_meclis.destroy', $mecli->id) }}" method="post" style="display: initial;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('bash_meclis.edit', $mecli->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>





            </div>
        </div>
    </div>
</div>
@endsection
