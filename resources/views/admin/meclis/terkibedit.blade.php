@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="/admin/meclis/terkib_update" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$terkib->id}}">
                  <div class="col-md-12">
                    <div class="input_groups ">
                      <div class="col-md-3">
                        <p><b>Başlıq</b><span style="color:red;"></span></p>
                        <input type="text" name="terkib"  class="form-control" required value="{{$terkib->terkib}}">
                      </div>
                      <div class="col-md-3">
                        <p><b>Başlıq (en)</b><span style="color:red;"></span></p>
                        <input type="text" name="terkib_en"  class="form-control" value="{{$terkib->terkib_en}}">
                      </div>
                      <div class="col-md-2">
                        <p><b>Say </b><span style="color:red;"></span></p>
                        <input type="number" name="say"  class="form-control" required value="{{$terkib->say}}">
                      </div>
                      <div class="col-md-2">
                        <p><b>Yer </b><span style="color:red;"></span></p>
                        <input type="number" name="yer"  class="form-control" required value="{{$terkib->yer}}">
                      </div>
                    </div>
                  </div>

                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>


@endsection
