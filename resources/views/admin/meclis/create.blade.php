@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('bash_meclis.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="col-md-6">
                  <p><b>Səhifə Başlığı <span style="color:red">*</span></b></p>
                  <input type="text" name="page_name" value="" class="form-control" required >
                </div>
                <div class="col-md-6">
                  <p><b>Səhifə Başlığı (en)</b><span style="color:red;"></span></p>
                  <input type="text" name="page_name_en"  class="form-control">
                </div>
                <div class="col-md-6">
                  <p><b>Kontent<span style="color:red">*</span></b></p>
                  <textarea name="page_description" rows="8" cols="80" required id="article-ckeditor"></textarea>
                </div>
                <div class="col-md-6">
                  <p><b>Kontent (en)<span style="color:red"></span></b></p>
                  <textarea name="page_description_en" rows="8" cols="80" required id="article-ckeditor1"></textarea>
                </div>
                <div class="col-md-12">
                  <p><b>Thumbnail (Fayl ölçüsü max 1024kb olmalıdır (min:800x420))</b><span style="color:red;">* </span></p>
                  <input type="file" name="page_img"  class="form-control"  required  id="logo">
                </div>
                <div class="col-md-12">
                    <p class="text-center"><b>Federasiyalar, Təşkilatlar və Üzvlər</b><span style="color:red;"></span></p>
                  <div class="input_groups ">
                    <div class="col-md-3">
                      <p><b>Başlıq</b><span style="color:red;">*</span></p>
                      <input type="text" name="terkib[]"  class="form-control" required>
                    </div>
                    <div class="col-md-3">
                      <p><b>Başlıq (en)</b><span style="color:red;"></span></p>
                      <input type="text" name="terkib_en[]"  class="form-control" >
                    </div>
                    <div class="col-md-2">
                      <p><b>Say </b><span style="color:red;">*</span></p>
                      <input type="number" name="say[]"  class="form-control" required>
                    </div>
                    <div class="col-md-2">
                      <p><b>Yer </b><span style="color:red;">*</span></p>
                      <input type="number" name="yer[]"  class="form-control" required>
                    </div>
                    <div class="col-md-2">
                      <p style="padding-bottom:22px;"></p>
                      <button type="button" class="btn btn-danger" id="remove">Sil</button>
                      <button type="button" class="btn btn-primary" id="add_element">Əlavə et</button>
                    </div>
                  </div>
                </div>




                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );

  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength < 1 ) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }

 });

 $('#logo').bind('change', function(event) {
   if ($(this).context.files[0].size/ 1024 > 1024) {
     $(this).replaceWith($(this).val('').clone(true));
     alert('Fayl 1024kb dan böyükdür');
   }
 });
 $(document).on('click','#add_element', function(){
   $('.input_groups:last').clone().insertAfter("div.input_groups:last");
 });
 $(document).on('click','#remove', function(){
   $(this).parents('div.input_groups').remove();
 });
</script>
@endsection
