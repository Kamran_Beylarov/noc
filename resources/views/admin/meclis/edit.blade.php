@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('bash_meclis.update', $meclis->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-6">
                    <p><b>Səhifə Başlığı <span style="color:red">*</span></b></p>
                    <input type="text" name="page_name" value="{{$meclis->page_name}}" class="form-control" required >
                  </div>
                  <div class="col-md-6">
                    <p><b>Səhifə Başlığı (en)</b><span style="color:red;"></span></p>
                    <input type="text" name="page_name_en"  class="form-control" value="{{$meclis->page_name_en}}">
                  </div>
                  <div class="col-md-6">
                    <p><b>Kontent<span style="color:red">*</span></b></p>
                    <textarea name="page_description" rows="8" cols="80" required id="article-ckeditor">{{$meclis->page_description}}</textarea>
                  </div>
                  <div class="col-md-6">
                    <p><b>Kontent (en)<span style="color:red"></span></b></p>
                    <textarea name="page_description_en" rows="8" cols="80" required id="article-ckeditor1">{{$meclis->page_description_en}}</textarea>
                  </div>
                  <div class="col-md-12">
                    <p><b>Thumbnail (Fayl ölçüsü max 1024kb olmalıdır (min:800x420))</b><span style="color:red;">* </span></p>
                    <input type="file" name="page_img"  class="form-control" id="cover" style="display:n one;">
                    <div class="col-md-3" style="padding-top:40px;">
                      <label for="cover"><img src="/uploads/{{$meclis->page_img}}" class="img-responsive" style="cursor:pointer"/></label>
                  </div>



                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">
  CKEDITOR.replace( 'article-ckeditor' );
  CKEDITOR.replace( 'article-ckeditor1' );

  $("form").submit( function(e) {
     var messageLength = CKEDITOR.instances['article-ckeditor'].getData().replace(/<[^>]*>/gi, '').length;
     var messageLength1 = CKEDITOR.instances['article-ckeditor1'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength < 1) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }

 });

$('#cover').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});

     </script>
@endsection
