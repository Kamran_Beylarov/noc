@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="/admin/meclis/{{$id}}/create"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Federasiyalar, Təşkilatlar və Üzvlər</th>
                          <th>Say</th>
                          <th>Yer</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($terkib as $mecli)
                      <tr>

                          <td>{{ $mecli->terkib }}</td>
                          <td>{{ $mecli->say }}</td>
                          <td>{{ $mecli->yer }}</td>

                          <td>
                            <a href="/admin/meclis/{{$mecli->id}}/del" class="btn-danger btn">Sil</a>
                              <a href="/admin/meclis/{{$mecli->id}}/terkib_edit" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>





            </div>
        </div>
    </div>
</div>
@endsection
