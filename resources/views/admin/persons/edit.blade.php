@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('persons.update', $person->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">


                                  <div class="col-md-6">
                                    <p><b>Şəxs adı soyadı ata adı (oğlu/qızı)</b><span style="color:red;">* </span></p>
                                    <input type="text" name="name_surname"  class="form-control"  required value="{{$person->name_surname}}">
                                  </div>

                                  <div class="col-md-6">
                                    <p><b>Şəxs adı soyadı ata adı (oğlu/qızı) (en)</b><span style="color:red;"> </span></p>
                                    <input type="text" name="name_surname_en"  class="form-control"  value="{{$person->name_surname_en}}">
                                  </div>


                                  <div class="col-md-3">
                                    <p><b>Vəzifəsi</b><span style="color:red;">* </span></p>
                                    <input type="text" name="person_post"  class="form-control"  required value="{{$person->person_post}}">
                                  </div>
                                  <div class="col-md-3">
                                    <p><b>Vəzifəsi (en)</b><span style="color:red;"> </span></p>
                                    <input type="text" name="person_post_en"  class="form-control"  value="{{$person->person_post_en}}">
                                  </div>

                                  <div class="col-md-3">
                                    <p><b>Sıra nömrəsi</b><span style="color:red;">* </span></p>
                                    <input type="number" name="person_order"  class="form-control"  required value="{{$person->person_order}}">
                                  </div>
                                  <div class="col-md-3">
                                    <p><span style="color:red;">* </span></p>
                                    <p></p>
                                    <input type="radio" id="other" name="page_status" value="genel" @if($person->page_status == 'genel') checked @endif >
                                    <label for="other">Heç biri</label><br>
                                    <input type="radio" id="pre" name="page_status" value="president" @if($person->page_status == 'president') checked @endif>
                                    <label for="pre">Prezident </label><br>
                                    <input type="radio" id="reh" name="page_status" value="rehber" @if($person->page_status == 'rehber') checked @endif>
                                    <label for="reh">İcra aparatının rəhbəri</label>
                                  </div>

                                  <div class="col-md-6">
                                    <p><b>Haqqında məlumat<span style="color:red">*</span></b></p>
                                    <textarea name="person_info" rows="8" cols="80" required id="article-ckeditor2">{{$person->person_info}}</textarea>
                                  </div>
                                  <div class="col-md-6">
                                    <p><b>Haqqında məlumat (en)<span style="color:red"></span></b></p>
                                    <textarea name="person_info_en" rows="8" cols="80"  id="article-ckeditor3">{{$person->person_info_en}}</textarea>
                                  </div>

                              <div class="col-md-6" >
                                <p><b>Avatar </b><br><span style="color:red;"></span></p>
                                <input type="file" name="avatar"  class="form-control" id="avat" style="display:n one;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="avat"><img src="/uploads/{{$person->avatar}}" class="img-responsive" style="cursor:pointer"/></label>
                                  <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                                  <input type="file" name="img_src[]" multiple class="form-control"> -->
                                </div>
                              </div>
                              <div class="col-md-6" >
                                <p><b>Səhifə şəkli </b><br><span style="color:red;"></span></p>
                                <input type="file" name="person_img"  class="form-control" id="cover" style="display:n one;">
                                <div class="col-md-3" style="padding-top:40px;">
                                  <label for="cover"><img src="/uploads/{{$person->person_img}}" class="img-responsive" style="cursor:pointer"/></label>
                                  <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                                  <input type="file" name="img_src[]" multiple class="form-control"> -->
                                </div>
                              </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">

CKEDITOR.replace( 'article-ckeditor2' );
CKEDITOR.replace( 'article-ckeditor3' );

$("form").submit( function(e) {

   var messageLength2 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
   // var messageLength3 = CKEDITOR.instances['article-ckeditor3'].getData().replace(/<[^>]*>/gi, '').length;
   if( messageLength2 < 1 ) {
       alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
       e.preventDefault();
   }

});

$('#date-fr').bootstrapMaterialDatePicker({
  format : 'YYYY-DD-MM',
  time: false,
 });

    $('#cover, #avat').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
