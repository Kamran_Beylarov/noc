@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('persons.store') }}" method="POST" enctype="multipart/form-data">
                @csrf



                <div class="col-md-6">
                  <p><b>Şəxs adı soyadı ata adı (oğlu/qızı)</b><span style="color:red;">* </span></p>
                  <input type="text" name="name_surname"  class="form-control"  required>
                </div>

                <div class="col-md-6">
                  <p><b>Şəxs adı soyadı ata adı (oğlu/qızı) (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="name_surname_en"  class="form-control"  >
                </div>
                <div class="col-md-3">
                  <p><b>Avatar</b><span style="color:red;">* </span></p>
                  <input type="file" name="avatar"  class="form-control"  required  id="logo">
                </div>
                <div class="col-md-3">
                  <p><b>Səhifə şəkli</b><span style="color:red;">* </span></p>
                  <input type="file" name="person_img"  class="form-control"  required  id="cov">
                </div>
                <div class="col-md-6">
                  <p></p>
                  <input type="radio" id="other" name="page_status" value="genel" checked>
                  <label for="other">Heç biri</label><br>
                  <input type="radio" id="pre" name="page_status" value="president">
                  <label for="pre">Prezident</label><br>
                  <input type="radio" id="reh" name="page_status" value="rehber">
                  <label for="reh">İcra aparatının rəhbəri</label>
                </div>

                <div class="col-md-4">
                  <p><b>Vəzifəsi</b><span style="color:red;">* </span></p>
                  <input type="text" name="person_post"  class="form-control"  required>
                </div>
                <div class="col-md-4">
                  <p><b>Vəzifəsi (en)</b><span style="color:red;"> </span></p>
                  <input type="text" name="person_post_en"  class="form-control" >
                </div>

                <div class="col-md-4">
                  <p><b>Sıra nömrəsi</b><span style="color:red;">* </span></p>
                  <input type="number" name="person_order"  class="form-control"  required>
                </div>

                <div class="col-md-6">
                  <p><b>Haqqında məlumat<span style="color:red">*</span></b></p>
                  <textarea name="person_info" rows="8" cols="80" required id="article-ckeditor2"></textarea>
                </div>
                <div class="col-md-6">
                  <p><b>Haqqında məlumat (en)<span style="color:red"></span></b></p>
                  <textarea name="person_info_en" rows="8" cols="80"  id="article-ckeditor3"></textarea>
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script type="text/javascript">

  CKEDITOR.replace( 'article-ckeditor2' );
  CKEDITOR.replace( 'article-ckeditor3' );

  $("form").submit( function(e) {

     var messageLength2 = CKEDITOR.instances['article-ckeditor2'].getData().replace(/<[^>]*>/gi, '').length;
     // var messageLength3 = CKEDITOR.instances['article-ckeditor3'].getData().replace(/<[^>]*>/gi, '').length;
     if( messageLength2 < 1) {
         alert( 'İşarələnmiş bütün xanalar doldurulmalıdır' );
         e.preventDefault();
     }

 });


$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
$('#cov').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
</script>
@endsection
