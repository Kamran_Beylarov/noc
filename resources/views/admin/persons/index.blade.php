@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('success') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('persons.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Şəxs adı</th>
                          <th>Sıra nömrəsi </th>
                          <th>Olimpia Dünyası</th>
                          <th>İcrayyə Komitəsi</th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($persons as $persons)
                      <tr>

                          <td>{{ $persons->name_surname }}</td>
                          <td>{{ $persons->person_order }}</td>
                          <td>
                            @if($persons->aparat == 1)
                               <button type="button" class="btn btn-success index_status id{{$persons->id}}" value="{{$persons->id}}" >Var</button>
                               @else
                               <button type="button" class="btn btn-danger index_status id{{$persons->id}}" value="{{$persons->id}}" >Yoxdur</button>
                               @endif
                          </td>
                          <td>
                            @if($persons->komite == 1)
                               <button type="button" class="btn btn-success icra_komite cid{{$persons->id}}" value="{{$persons->id}}" >Var</button>
                               @else
                               <button type="button" class="btn btn-danger icra_komite cid{{$persons->id}}" value="{{$persons->id}}" >Yoxdur</button>
                               @endif
                          </td>

                          <td>
                              <form action="{{ route('persons.destroy', $persons->id) }}" method="post" style="display: initial;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('persons.edit', $persons->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>





            </div>
        </div>
    </div>
</div>
<script>
$('.index_status').on('click',function(e){
  var id = $(this).val();
  $.ajax({
    type:"GET",
    cache:false,
    url:"/admin/index_status/",
    data:{id:id},
    success: function (html) {
      console.log(html);
      if (html.aparat == 1) {
        $('.id'+html.id).empty().html('Var').removeClass('btn-danger').addClass('btn-success');
      }else{
        $('.id'+html.id).empty().html('Yoxdur').removeClass('btn-success').addClass('btn-danger');
      }
    }
  });
});
$('.icra_komite').on('click',function(e){
  var id = $(this).val();
  $.ajax({
    type:"GET",
    cache:false,
    url:"/admin/icra_komite/",
    data:{id:id},
    success: function (html) {
      console.log(html);
      if (html.komite == 1) {
        $('.cid'+html.id).empty().html('Var').removeClass('btn-danger').addClass('btn-success');
      }else{
        $('.cid'+html.id).empty().html('Yoxdur').removeClass('btn-success').addClass('btn-danger');
      }
    }
  });
});
</script>
@endsection
