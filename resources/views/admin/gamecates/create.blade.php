@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('game_categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="col-md-4">
                  <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                  <input type="text" name="cate_name"  class="form-control"  required>
                </div>
                <div class="col-md-4">
                  <p><b>Kateqoriya adı  (EN)</b><span style="color:red;"> </span></p>
                  <input type="text" name="cate_name_en"  class="form-control"  >
                </div> <div class="col-md-4">
                  <p><b>Sıra nömrəsi</b><span style="color:red;"> *</span></p>
                  <input type="number" name="cate_order"  class="form-control"  required>
                </div>
                <div class="col-md-12">
                  <p><b>Thumbnail (Fayl ölçüsü max 1024kb olmalıdır (min:260x360))</b><span style="color:red;"> </span></p>
                  <input type="file" name="cate_img"  class="form-control"  id="logo">
                </div>
                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                      Əlavə Et
                    </button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#logo').bind('change', function(event) {
  if ($(this).context.files[0].size/ 1024 > 1024) {
    $(this).replaceWith($(this).val('').clone(true));
    alert('Fayl 1024kb dan böyükdür');
  }
});
</script>
@endsection
