@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">

            <div class="header">
            </div>
            <div class="body table-responsive">
              <form class="form-horizontal" action="{{ route('game_categories.update', $game_cate->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <input type="hidden" name="_method" value="PATCH">
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  </b><span style="color:red;">* </span></p>
                    <input type="text" name="cate_name"  class="form-control"  required value="{{$game_cate->cate_name}}">
                  </div>
                  <div class="col-md-4">
                    <p><b>Kateqoriya adı  (EN)</b><span style="color:red;"> </span></p>
                    <input type="text" name="cate_name_en"  class="form-control" value="{{$game_cate->cate_name_en}}" >
                  </div><div class="col-md-4">
                    <p><b>Sıra nömrəsi</b><span style="color:red;"> *</span></p>
                    <input type="number" name="cate_order"  class="form-control" value="{{$game_cate->cate_order}}" required>
                  </div>
                <div class="col-md-12" >
                  <p><b>Thumbnail </b><br><span style="color:red;">(Fayl ölçüsü max 1024kb olmalıdır (min:260x360))</span></p>
                  <input type="file" name="cate_img"  class="form-control" id="cover" style="display:n one;">
                  <div class="col-md-3" style="padding-top:40px;">
                    <label for="cover"><img src="/uploads/{{$game_cate->cate_img}}" class="img-responsive" style="cursor:pointer"/></label>
                    <!-- <p><b>İşə aid şəkillər (Eyni anda max 10 şəkil əlavə edilə bilər)</b></p>
                    <input type="file" name="img_src[]" multiple class="form-control"> -->
                  </div>
                </div>


                <div class="col-md-10">
                  <button type="submit" class="btn btn-success">
                    Redaktə Et
                    </button>
                </div>
              </form>


            </div>
        </div>
    </div>
</div>

     <script type="text/javascript">

    $('#cover, #intro').bind('change', function(event) {
      if ($(this).context.files[0].size/ 1024 > 1024) {
        $(this).replaceWith($(this).val('').clone(true));
        alert('Fayl 1024kb dan böyükdür');
      }
    });
     </script>
@endsection
