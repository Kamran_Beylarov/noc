@extends('layouts.static')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            @if(Session::has('mesaj'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                {{ Session::get('mesaj') }}
            </div>
            @endif
            <div class="header">
                <h2>
                <a href="{{ route('game_categories.create') }}"><button type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float">
                    <i class="material-icons">add</i>
                </button></a>
            </div>
            <div class="body table-responsive">


              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Kateqoriya adı </th>
                          <th>Sıra nömrəsi </th>
                          <th><span class="text-danger">Sil</span> / <span class="text-success">Redaktə et</span></th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($game_cates as $game_cate)
                      <tr>

                          <td>{{ $game_cate->cate_name }}</td>
                          <td>{{ $game_cate->cate_order }}</td>
                          <td>
                            <button value="{{$game_cate->id}}" class="btn btn-danger remove">Sil</button>
                              <form id="remove{{$game_cate->id}}" style="display:none;" action="{{ route('game_categories.destroy', $game_cate->id) }}" method="post" style="display: initial;">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="_method" value="DELETE">
                                  <input type="submit" value="Sil" class="btn btn-danger">
                              </form>
                              <a href="{{ route('game_categories.edit', $game_cate->id) }}" class="btn-success btn">Redaktə et</a>
                          </td>
                      </tr>
                      @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<script>

$('.remove').on('click', function(){
    var val = $(this).val();
    const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
     },
  buttonsStyling: false
})

swalWithBootstrapButtons.fire({
  title: 'Əminsiniz?',
  text: "Oyun kateqoriyası silinərsə kateqoriyaya aid bütün oyunlar, oyunların daxilindəki nəticələr, oyunlara aid idmançı nəticələri və xəbərlər silinəcək",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Bəli, Sil!',
  cancelButtonText: 'Ləğv et!',
  reverseButtons: true
}).then((result) => {
  if (result.isConfirmed) {
    $('#remove'+val+'').submit();
   
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Ləğv edildi',
    )
  }
})
});
</script>
@endsection
