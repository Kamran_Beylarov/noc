<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="robots" content="noindex">
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Azərbaycan Respublikası Milli Olimpiya Komitəsi</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{url('admin/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/node-waves/waves.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Animation Css -->
    <link href="{{url('admin/plugins/animate-css/animate.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{url('admin/css/style.css')}}" rel="stylesheet">
    <link href="{{url('admin/css/themes/all-themes.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/jquery-spinner/css/bootstrap-spinner.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link href="{{url('admin/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet">
    <link href="{{url('admin/css/materialdata.css')}}" rel="stylesheet">


    <script src="{{url('admin/plugins/jquery/jquery.min.js')}}"></script>
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="{{url('admin/plugins/bootstrap/js/bootstrap.js')}}"></script>
      <script src="{{url('admin/js/materialdata.js')}}"></script>

</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Yüklənir...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->

    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">NOC</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->

                    <!-- #END# Notifications -->
                    <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons">input</i></a></li>
                          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Naviqasiya</li>
                    <li>
                        <a href="{{url('admin/gallery')}}">
                            <span>Foto qallereya</span>
                        </a>
                    </li>
                    {{-- <li>
                        <a href="{{url('admin/videogallery')}}">
                            <span>Video qallereya</span>
                        </a>
                    </li> --}}
                    <li>
                        <a href="{{url('admin/publications')}}">
                            <span>Nəşrlər</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/news')}}">
                            <span>Tədbirlər ,  Proqramlar və Xəbərlər</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/federations')}}">
                            <span>Federasiyalar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/document_groups')}}">
                            <span>Sənəd Kateqoriyaları</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/documents')}}">
                            <span>Sənədlər</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/sponsors')}}">
                            <span>Sponsorlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/partners')}}">
                            <span>Tərəfdaşlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/executive')}}">
                            <span>İcrayyə komitəsi</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/persons')}}">
                            <span>Önəmli şəxslər</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/history')}}">
                            <span>Tarix</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/committees')}}">
                            <span>Komissiyalar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/bash_meclis')}}">
                            <span>Baş Məclis</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Baş Məclis Üzvləri</span>
                        </a>
                        <ul>
                          <li>
                              <a href="{{url('admin/institutions')}}">
                                  <span>Kateqoriyalar</span>
                              </a>
                          </li>
                          <li>
                              <a href="{{url('admin/members')}}">
                                  <span>Üzvlər</span>
                              </a>
                          </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{url('admin/sports')}}">
                            <span>İdman Növləri</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/game_categories')}}">
                            <span>Oyun Kateqoriyaları</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/games')}}">
                            <span>Oyunlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/game_statistics')}}">
                            <span>Oyun statistikaları</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/athlete')}}">
                            <span>İdmançılar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/tournaments')}}">
                            <span>Qeyri Olimpiya Turnirləri</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('admin/athlete_statistics')}}">
                            <span>İdmançı nəticələri</span>
                        </a>
                    </li>

                  </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2021 <a href="/"> Azərbaycan Respublikası Milli Olimpiya Komitəsi</a>.
                </div>

            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

    </section>

    <section class="content">
          @yield('content')
    </section>

    <!-- Jquery Core Js -->

    <!-- Bootstrap Core Js -->

    {{-- <script src="{{url('admin/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>--}}
    <script src="{{url('admin/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{url('admin/plugins/node-waves/waves.js')}}"></script>
    <script src="{{url('admin/plugins/jquery-countto/jquery.countTo.js')}}"></script>
    <script src="{{url('admin/plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{url('admin/plugins/morrisjs/morris.js')}}"></script>
    <script src="{{url('admin/plugins/chartjs/Chart.bundle.js')}}"></script>
    <script src="{{url('admin/plugins/flot-charts/jquery.flot.js')}}"></script>
    <script src="{{url('admin/plugins/flot-charts/jquery.flot.resize.js')}}"></script>
    <script src="{{url('admin/plugins/flot-charts/jquery.flot.pie.js')}}"></script>
    <script src="{{url('admin/plugins/flot-charts/jquery.flot.categories.js')}}"></script>
    <script src="{{url('admin/plugins/flot-charts/jquery.flot.time.js')}}"></script>
    <script src="{{url('admin/plugins/jquery-sparkline/jquery.sparkline.js')}}"></script>

    <script src="{{url('admin/js/admin.js')}}"></script>
    <script src="{{url('admin/js/pages/index.js')}}"></script>
    <script src="{{url('admin/js/demo.js')}}"></script>

    <!-- // <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

</body>
<style media="screen">
.swal-overlay--show-modal {
  display: block !important;
}
</style>
<script type="text/javascript">
@if(Session::has('alert'))
  swal("Diqqət!", "{{ Session::get('alert') }}", "warning");
@endif
</script>
</html>
