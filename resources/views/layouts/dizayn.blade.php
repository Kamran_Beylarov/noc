<!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="tags for website">
    <meta name="description" content="">
    <meta name="author" content="www.endorphin.az">

    {!! MetaTag::openGraph() !!}
    {!! MetaTag::twitterCard() !!}
    <meta name="twitter:card" content="summary_large_image">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('front/fav/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('front/fav/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('front/fav/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x7" href="{{ url('front/fav/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('front/fav/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('front/fav/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('front/fav/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('front/fav/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('front/fav/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('front/fav/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('front/fav/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('front/fav/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('front/fav/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('front/fav/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('front/fav/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <title>Azərbaycan Respublikası Milli Olimpiya Komitəsi </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
    <link href="{{ url('front/css/main.css?v=5') }}" rel="stylesheet">
    {{-- <script id="cookieyes" type="text/javascript" src="https://cdn-cookieyes.com/client_data/4bd25238fc235b01fb86ba42.js">
    </script> --}}
    <!-- End cookieyes banner -->

</head>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src ="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="{{ url('front/js/main.js?v=4') }}"></script></body>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

<link rel="stylesheet" href="{{ url('front/css/style.css') }}">
<link rel="stylesheet" href="{{ url('front/css/custom.css?v=10.1') }}">

<script type="text/javascript" src="{{ url('front/js/custom.js?v=1.1') }}"></script>
<style media="screen">
    .swal-overlay--show-modal {
        display: block !important;
    }

</style>
<script type="text/javascript">
    @if (Session::has('error'))
        swal("Warning!", "{{ Session::get('error') }}", "warning");
    @endif
    @if (Session::has('success'))
        swal("{{ Session::get('success') }}!", " ", "success");
    @endif
    $(document).on('click', '.cky-btn-accept', function() {


//         var iFrameIds = ['otchannel01', 'otchannel02', 'otchannel03', 'otchannel04', 'otchannel05',
//             'otchannel06'
//         ];

//         iFrameIds.forEach(function(id) {

//             var iframe = document.getElementById(id);

//             if (iframe) {

//                 var url = iframe.getAttribute("src");



//                 iframe.setAttribute("src", url + '&consent=1');


//             }

//         });
//     });
//     $(document).ready(function() {
//         var iFrameIds = ['otchannel01', 'otchannel02', 'otchannel03', 'otchannel04', 'otchannel05',
//             'otchannel06'
//         ];
//         console.log(Cookies.get('cky-active-check'));
//         iFrameIds.forEach(function(id) {

//             var iframe = document.getElementById(id);

//             if (iframe) {

//                 var url = iframe.getAttribute("src");

//                 iframe.setAttribute("src", url + '&consent=1');


//             }

//     });

// });
</script>

<body>
    <header>
        <section id="header-top">
            <div class="container">
                <div class="wrapper">
                    <div class="languages">
                        <a href="/locale/az" class="lang_link @if ($locale=='az' ) active @endif">AZE</a>
                        <a href="/locale/en" class="lang_link @if ($locale=='en' ) active @endif">ENG</a>
                    </div>
                    <div class="social-media">
                        <a href="https://twitter.com/nocazerbaijan" class="sm_link" target="_blank"><span
                                class="icon-social-icons-black-06"></span></a>
                        <a href="https://www.facebook.com/nocazerbaijan/" class="sm_link" target="_blank"><span
                                class="icon-social-icons-black-03"></span></a>
                        <a href="https://www.linkedin.com/company/nocazerbaijan/" class="sm_link" target="_blank"><span
                                class="icon-social-icons-black-04"></span></a>
                        <a href="https://www.youtube.com/channel/UCAD0sVhftaAUdFktUWfi7cQ" class="sm_link"
                            target="_blank"><span class="icon-social-icons-black-02"></span></a>
                        <a href="https://www.instagram.com/noc.azerbaijan/" class="sm_link" target="_blank"><span
                                class="icon-social-icons-black-01"></span></a>
                        <a href="https://www.tiktok.com/@nocazerbaijan?" class="sm_link" target="_blank"><span
                                class="icon-social-icons-black-05"></span></a>
                    </div>
                </div>
            </div>
        </section>
        <section id="header-menu">
            <div class="container">
                <div class="wrapper">
                    <a href="/" class="logo">
                        <img src="{{ url('front/assets/logo.svg') }}" alt="">
                        <span>
                            {!! trans('lang.noc') !!}
                        </span>
                    </a>
                    <button class="ham-menu">
                        <img src="{{ url('front/assets/ham.svg') }}" alt="">
                    </button>
                    <div class="menu_holder">
                        <div class="menu_wrapper">
                            <nav>
                               {{-- <a href="/tokyo2020" class="nav_link tokyo_alert">TOKYO 2020</a> --}}
                                <a href="/about" class="nav_link">{!! trans('lang.about_us') !!} </a>
                                {{-- <a href="/activity" class="nav_link">{!! trans('lang.activities') !!}</a> --}}
                                <a href="/articles" class="nav_link">{!! trans('lang.news') !!}</a>
                                <a href="/game-categories" class="nav_link">{!! trans('lang.games') !!}</a>
                                <a href="/athletes" class="nav_link">{!! trans('lang.athletes') !!}</a>
                                <a href="/galleries" class="nav_link">{!! trans('lang.gallery') !!}</a>
                                <a href="/partners" class="nav_link">{!! trans('lang.partners') !!}</a>
                            </nav>
                            <div class="mob_wrapper">
                                <div class="language v-mob">
                                    <a href="/locale/az" class="l_link @if ($locale=='az' ) active @endif">AZE </a>
                                    <a href="/locale/en" class="l_link @if ($locale=='en' ) active @endif">ENG</a>
                                </div>
                                <div class="search-form">
                                    <button type="button" class="btn_search"><i class="fa fa-search"></i></button>
                                    <form action="/news-search" method="get">
                                        <input type="text" name="search_news"
                                            placeholder="{{ trans('lang.search') }}">
                                        <button type="submit" class="btn_submit"><img
                                                src="{{ url('front/assets/arrow-navy.svg') }}" alt=""></button>
                                    </form>
                                </div>
                            </div>
                            <div class="sm_links v-mob">
                                <a href="https://twitter.com/nocazerbaijan" class="sm_link" target="_blank"><span
                                        class="icon-social-icons-black-06"></span></a>
                                <a href="https://www.facebook.com/nocazerbaijan/" class="sm_link" target="_blank"><span
                                        class="icon-social-icons-black-03"></span></a>
                                <a href="https://www.linkedin.com/company/nocazerbaijan/" class="sm_link"
                                    target="_blank"><span class="icon-social-icons-black-04"></span></a>
                                <a href="https://www.youtube.com/channel/UCAD0sVhftaAUdFktUWfi7cQ" class="sm_link"
                                    target="_blank"><span class="icon-social-icons-black-02"></span></a>
                                <a href="https://www.instagram.com/noc.azerbaijan/" class="sm_link"
                                    target="_blank"><span class="icon-social-icons-black-01"></span></a>
                                <a href="https://www.tiktok.com/@nocazerbaijan?" class="sm_link" target="_blank"><span
                                        class="icon-social-icons-black-05"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>

    <main class="content">
        @yield('content')
    </main>
    <footer>
        <div class="container">
            <div class="footer_content">
                <div class="f_col">
                    <div class="f_about">
                        <h6 class="f_title">{{ trans('lang.footer_title') }}</h6>
                        <p class="f_text">{{ trans('lang.footer_text') }}</p>
                    </div>
                    <div class="f_sm">
                        <h6 class="f_title">{{ trans('lang.follow_us') }}</h6>
                        <div class="sm_links">
                            <a href="https://twitter.com/nocazerbaijan" class="link" target="_blank"><span
                                    class="icon-social-icons-black-06"></span></a>
                            <a href="https://www.facebook.com/nocazerbaijan/" class="link" target="_blank"><span
                                    class="icon-social-icons-black-03"></span></a>
                            <a href="https://www.linkedin.com/company/nocazerbaijan/" class="link" target="_blank"><span
                                    class="icon-social-icons-black-04"></span></a>
                            <a href="https://www.youtube.com/channel/UCAD0sVhftaAUdFktUWfi7cQ" class="link"
                                target="_blank"><span class="icon-social-icons-black-02"></span></a>
                            <a href="https://www.instagram.com/noc.azerbaijan/" class="link" target="_blank"><span
                                    class="icon-social-icons-black-01"></span></a>
                            <a href="https://www.tiktok.com/@nocazerbaijan?" class="link" target="_blank"><span
                                    class="icon-social-icons-black-05"></span></a>
                        </div>
                    </div>
                </div>
                <div class="f_col">
                    <div class="f_wrapper">
                        <div class="f_partners">
                            <h6 class="f_title">{{ trans('lang.proud_partners') }}</h6>
                            <div class="wrapper">
                                @foreach ($sponsors_layout as $sponsors_lay)
                                    <a href="{{ $sponsors_lay->web_site }}" class="partner_link" target="_blank"><img
                                            src="/uploads/{{ $sponsors_lay->logo_white }}" alt=""></a>

                                @endforeach
                            </div>
                        </div>
                        {{-- <form action="/subscribe" class="f_form" method="post">
                       <p style="color: #fff;"> {{Session::get('success')}}</p>
                      
                        @csrf
                            <h6 class="f_title">{{ trans('lang.straight_to_your_inbox') }}</h6>
                            <div class="input_wrapper">
                                <input type="email" placeholder="{{ trans('lang.please_enter_you_e-mail') }}" required name="email">
                                <i class="fa fa-envelope"></i>
                            </div>
                        </form> --}}
                    </div>
                </div>
            </div>
            <div class="footer_menu">
                <div class="f_copyright">
                    <p>{{ trans('lang.footer_title') }} © 2021</p>
                </div>
                <nav>
                    <a href="/about" class="nav_link">{!! trans('lang.about_us') !!}</a>
                    {{-- <a href="/activity" class="nav_link">{!! trans('lang.activities') !!}</a> --}}
                    <a href="/articles" class="nav_link">{!! trans('lang.news') !!}</a>
                    <a href="/games" class="nav_link">{!! trans('lang.games') !!}</a>
                    <a href="/athletes" class="nav_link">{!! trans('lang.athletes') !!}</a>
                    <a href="/galleries" class="nav_link">{!! trans('lang.gallery') !!}</a>
                    <a href="/partners" class="nav_link">{!! trans('lang.partners') !!}</a>
                </nav>
            </div>
        </div>
    </footer>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-5R1HQ1KLZB"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
 
  gtag('config', 'G-5R1HQ1KLZB');
</script>

</html>
