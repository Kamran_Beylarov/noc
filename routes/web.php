<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'IndexController@index');


Route::get('/articles', 'NewsController@news');
Route::get('/news/{id}', 'NewsController@news_detalis');
Route::get('/news-archive', 'NewsController@news_archive');
Route::get('/news-search', 'NewsController@news_search');


Route::get('/activity', 'NewsController@activity');
Route::get('/activities', 'NewsController@activities');
Route::get('/activity/{id}', 'NewsController@activity_detalis');
Route::get('/programs', 'NewsController@programs');
Route::get('/programs/{id}', 'NewsController@program_detalis');
Route::get('/galleries', 'GalleryController@galleries');
Route::get('/gallery/{id}', 'GalleryController@gallery_detalis');
Route::get('/videos/{id}', 'GalleryController@video_detalis');
Route::get('/game-categories', 'GameController@game_categories');
Route::get('/game-categories/{id}', 'GameController@games');
Route::get('/game/{id}', 'GameController@games_detalis');
Route::get('/partners', 'PartnersController@partners');
Route::get('/partner/{id}', 'PartnersController@partner_detalis');
Route::get('/about', 'AboutController@about');
Route::get('/athletes', 'AthleteController@index');
Route::get('/athlete/{id}', 'AthleteController@detailis');
Route::get('/athletes/filters', 'AthleteController@athlete_filter');
Route::get('/sports', 'AthleteController@sports');

Route::get('/locale/{lang}', 'IndexController@setLocale');
Route::post('/subscribe', 'ContactController@subscribe');

// Route::get('/test_view', 'IndexController@test');
// Route::get('/tokyo2020', function(){
//     return view('front.tokyo2020');
// });

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::resource('gallery' ,'admin\GalleryController');
    Route::get('/imgs/{id}/del' ,'admin\GalleryController@del');
    Route::post('/update_img' ,'admin\GalleryController@update_img');
    Route::post('/create_img' ,'admin\GalleryController@create_img');

    Route::resource('videogallery' ,'admin\VideoGalleryController');
    Route::resource('publications' ,'admin\PublicationsController');

    Route::resource('news' ,'admin\NewsController');
    Route::get('/news_imgs/{id}/del' ,'admin\NewsController@del');
    Route::post('/news_update_img' ,'admin\NewsController@update_img');
    Route::post('/news_create_img' ,'admin\NewsController@create_img');

    Route::resource('/federations' ,'admin\FederationsController');
    Route::resource('/documents' ,'admin\DocumentsController');
    Route::resource('/document_groups' ,'admin\DocumentGroupsController');
    Route::resource('/sponsors' ,'admin\SponsorsController');
    Route::resource('/partners' ,'admin\PartnersController');

    Route::resource('/persons' ,'admin\PersonsController');
    Route::get('/index_status','admin\PersonsController@index_status');
    Route::get('/icra_komite','admin\PersonsController@icra_komite');

    Route::resource('/history' ,'admin\HisrtoryController');
    Route::resource('/committees' ,'admin\CommitteeController');

    Route::resource('/bash_meclis' ,'admin\BasMeclisController');
    Route::get('/meclis/{id}/del' ,'admin\BasMeclisController@del');
    Route::get('/meclis/{id}/terkib_edit' ,'admin\BasMeclisController@terkib_edit');
    Route::get('/meclis/{id}/create' ,'admin\BasMeclisController@creat_ter');
    Route::post('/meclis/terkib_update' ,'admin\BasMeclisController@terkib_update');
    Route::post('/meclis/terkib_create' ,'admin\BasMeclisController@terkib_create');

    Route::resource('/institutions' ,'admin\InstitutionsController');
    Route::resource('/members' ,'admin\MembersController');

    Route::resource('/game_categories' ,'admin\GameCategoriesController');
    Route::resource('/sports' ,'admin\SportsController');
    Route::resource('/games' ,'admin\GamesController');
    Route::get('/game_status' ,'admin\GamesController@game_status');

    Route::resource('/executive' ,'admin\IcrayyeController');
    Route::resource('/game_statistics' ,'admin\GameStatisticsController');
    Route::resource('/athlete' ,'admin\AthleteController');
    Route::get('/athlete_kommisia' ,'admin\AthleteController@kommisia');
    Route::get('/significance_status' ,'admin\AthleteController@significance_status');

    Route::resource('/tournaments' ,'admin\TurnirController');
    Route::resource('/athlete_statistics' ,'admin\AthleteStatisticsController');
    Route::get('/tournaments_type' ,'admin\AthleteStatisticsController@tournaments_type');
});
