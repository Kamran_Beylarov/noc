$(document).ready(function () {
  $('.filter_input').on('change', function () {
    $('#filter').submit();
  });
  $('.filter_a_z button').click(function () {
    $('.alphabet_main').toggle();
  });

  // $('.tab_link').on('click', function(){
  //   $('.structure_content').hide();
  //   var id= $(this).data('target');
  //   console.log(id);
  //   $('#'+id).show();
  // });
 owl = $('#news_gallery').owlCarousel({
   
    margin: 10,
    nav: false,
    dots:false,
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });
  $('.next').on('click',function(){
    owl.trigger('next.owl.carousel')
  });
  $('.prev').on('click',function(){
    owl.trigger('prev.owl.carousel')
  });


  $('[data-fancybox="gallery"]').fancybox({
    buttons: [
    "zoom",
    "fullScreen",
    "thumbs",
    "close"
    ],
});
});